﻿using NServiceBus;
using System;

namespace DMG.Bus.Message
{
    public class Mail: IEvent
    {
        public string from { get; set; }

        public string to { get; set; }
        public string subject { get; set; }
        public string content { get; set; }
        public string htmlContent { get; set; }
    }
}
