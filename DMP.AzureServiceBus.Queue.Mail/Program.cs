﻿using Microsoft.Azure.ServiceBus;
using System;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using Microsoft.Extensions.Configuration;
namespace DMP.AzureServiceBus.Queue.Mail
{
    class Program
    {
        const string ServiceBusConnectionString = "Endpoint=sb://digitalmedicalpassport.servicebus.windows.net/;SharedAccessKeyName=dmp;SharedAccessKey=Q3q+z9qYqdw+9prEY5DNTxsM1/TkUa86pWA4D3gkp0Y=";
        const string QueueName = "testQueue";
        static IQueueClient queueClient;
        static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            queueClient = new QueueClient(ServiceBusConnectionString, QueueName);

            Console.WriteLine("======================================================");
            Console.WriteLine("Press ENTER key to exit after receiving all the messages.");
            Console.WriteLine("======================================================");

            // Register the queue message handler and receive messages in a loop
            RegisterOnMessageHandlerAndReceiveMessages();

            Console.Read();

            await queueClient.CloseAsync();
        }

        static void RegisterOnMessageHandlerAndReceiveMessages()
        {
            var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                MaxConcurrentCalls = 1,
                AutoComplete = false
            };
            queueClient.RegisterMessageHandler(ProcessMessageAsync, messageHandlerOptions);
        }

        private static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
            var context = exceptionReceivedEventArgs.ExceptionReceivedContext;
            Console.WriteLine("Exception context for troubleshooting:");
            Console.WriteLine($"- Endpoint: {context.Endpoint}");
            Console.WriteLine($"- Entity Path: {context.EntityPath}");
            Console.WriteLine($"- Executing Action: {context.Action}");
            return Task.CompletedTask;
        }

        static async Task ProcessMessageAsync(Message message, CancellationToken token)
        {
            //var student = new student();
            var mail = JsonConvert.DeserializeObject<MailBindingModel>(Encoding.UTF8.GetString(message.Body));
           // Console.WriteLine($"Received message: SequenceNumber:{message.SystemProperties.SequenceNumber} Body: {student.FirstName } , { student.id} ");

            var key = "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";
            var client = new SendGridClient(key);

            var from = new EmailAddress(mail.from, "Digitial Medical Passport Application");
            var subject = "Something has changed to your medical Profile!";
            var to = new EmailAddress(mail.to, "Example User");
            var plainTextContent = mail.content;
            var htmlContent = "<strong>Hello, Something has changed to your medical profile. Login to see the changes</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
           // Console.WriteLine("Email Sent");
            await queueClient.CompleteAsync(message.SystemProperties.LockToken);
        }
    }
    public class MailBindingModel
    {
        public string from { get; set; }

        public string to { get; set; }
        public string subject { get; set; }
        public string content { get; set; }
        public string htmlContent { get; set; }
    }
}