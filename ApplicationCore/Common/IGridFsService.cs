﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MongoDB.Driver.GridFS;

namespace ApplicationCore.Common
{
    public interface IGridFsService
    {
        void Delete(string id);
        Task<GridFSFileInfo> Getfile(string name);
        void UploadFile(string filename, IFormFile file);
    }
}