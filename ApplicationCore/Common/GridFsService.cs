﻿using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ApplicationCore.Common
{
    public class GridFsService<T> : IGridFsService where T : class
    {
        public readonly DbContext<T> db;
        IGridFSBucket bucket;

        public GridFsService()
        {
            db = new DbContext<T>();
            bucket = new GridFSBucket(db.database);
        }
        //Get file by name
        public async virtual Task<GridFSFileInfo> Getfile(string name)
        {
            //bucket.FindAsync();

            FilterDefinition<GridFSFileInfo> filter = null;
            if (name != null)
            {
                filter = Builders<GridFSFileInfo>.Filter.Eq(f => f.Filename, name);
            }

            var options = new GridFSFindOptions
            {
                Limit = 1,

            };

            using (var cursor = await bucket.FindAsync(filter, options))
            {
                try
                {
                    var fileinfo = (await cursor.ToListAsync()).FirstOrDefault();
                    return fileinfo;
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.ToString());
                }

                return null;
            }
        }

        //Delete file by id

        public virtual async void Delete(string id)
        {
            if (id != null)
            {
                try
                {
                    await bucket.DeleteAsync(ObjectId.Parse(id));
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.ToString());
                }
            }

        }
        //uploading a file 
        public async void UploadFile(string filename, IFormFile file)
        {


            var options = new GridFSUploadOptions
            {
                ChunkSizeBytes = 1048576,
                Metadata = new BsonDocument
                {
                    {"contentType",file.ContentType },
                    { "resolution","1080P" },
                    {"copyrighted",true }
                }
            };
            try
            {
                if (filename != null)
                {
                    var id = await bucket.UploadFromStreamAsync(filename, file.OpenReadStream(), options);
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
            }


        }

        //Downloading file
        //public async Task<GridFSDownloadStream> DownloadFile(string name)
        //{
        //    var stream = await bucket.OpenDownloadStreamByNameAsync(name);
        //    var contentType = stream.FileInfo.ContentType ?? stream.FileInfo.Metadata["contentType"].AsString;

        //    return (stream, contentType);


        //}
    }
}
