﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Text;

namespace ApplicationCore.Common
{

    public class DbContext<T> where T : class
    {


        protected readonly IMongoClient client;
        public readonly IMongoDatabase database;
        public readonly IMongoCollection<T> collection;


        public DbContext()
        {
            //var connectionString = "mongodb://127.0.0.1:27017";
            var  connectionString = @"mongodb://dmp:HBqAc9FdD2PIuPv9l7gA0pRfzI0V98fXjUMCwZ0PXPwD8sOWENaBBIbjA1eLhmOCHbBeXm9ENkU3nNtVBlQ62w==@dmp.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
            try
            {
                var settings = MongoClientSettings.FromUrl(new MongoUrl(connectionString));
               // settings.ClusterConfigurator = builder => builder.Subscribe(new Log4netMongoEvent());
                settings.SslSettings = new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
                client = new MongoClient(settings);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
            }
            database = client.GetDatabase("demo");
            collection = database.GetCollection<T>(typeof(T).Name.ToLower());
        }


    }
}

