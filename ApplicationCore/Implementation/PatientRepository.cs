﻿using ApplicationCore.Common;
using ApplicationCore.Entities;
using ApplicationCore.Helper;
using ApplicationCore.Interface;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.Implementation
{
    public class PatientRepository : IDisposable, IPatientRepository
    {
        // DBContext<PatientProfile> db;
        // IDBService<PatientProfile> _dbService;
        public GridFsService<Patient> _dbFileService;
        public DbContext<Patient> _dbService;
       // private IPropertyMappingService _propertyMappingService;
        public PatientRepository()
        {
            _dbService = new DbContext<Patient>();
            //  _dbService = new DBService<PatientProfile>();
            _dbFileService = new GridFsService<Patient>();

           // _propertyMappingService = propertyMappingService;
        }





        public async  Task<PagedList<Patient>> GetAll(PatientResourceParameter resourceParamete)
        {
            var collectionBeforePaging = _dbService.collection.AsQueryable()
              .OrderBy(o => o.UserId)
               .ThenBy(t => t._id).AsQueryable();
            //var collectionBeforePaging =
            //   _dbService.collection.AsQueryable().ApplySort(resourceParamete.OrderBy,
            //  _propertyMappingService.GetPropertyMapping<PatientDto, Patient>());
           

            if (!string.IsNullOrEmpty(resourceParamete.FirstName))
            {
                var firstNameFiltering = resourceParamete.FirstName.Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging.Where(a => a.FirstName.ToLowerInvariant() == firstNameFiltering);
            }

            if (!string.IsNullOrEmpty(resourceParamete.OrderByFirstName))
            {
                var order = resourceParamete.OrderByFirstName.Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging.OrderByDescending(o => o.FirstName);
            }

            if (!string.IsNullOrEmpty(resourceParamete.SearchQuery))
            {
                var searchQuery = resourceParamete.SearchQuery.Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging.Where(a => a.FirstName.ToLowerInvariant().Contains(searchQuery)
                  || a.LastName.ToLowerInvariant().Contains(searchQuery) || a.UserId.ToLowerInvariant().Contains(searchQuery));
            }

            return PagedList<Patient>.Create(collectionBeforePaging, resourceParamete.PageNumber, resourceParamete.PageSize);

            // var filter = Builders<Patient>.Filter.Empty;
            //  return  await _dbService.collection.;
            //var result= await _dbService.collection.AsQueryable()
            // .OrderBy(r => r.UserId)
            // .ThenByDescending(r => r._id)
            // .Skip(resourceParamete.PageNumber * (resourceParamete.PageNumber-1))
            // .Take(resourceParamete.PageSize)
            // .ToListAsync();
            //  return result;

            // return await _dbService.collection.Find(filter).ToListAsync();

        }

        public async Task<Patient> GetById(string id)
        {
            
            return await _dbService.collection.AsQueryable().Where(p => p.UserId == id).FirstOrDefaultAsync();

           
        }
        /// <summary>
        /// Find doctors list in patient profile. This list is added only when authorised the doctor
        /// </summary>
        /// <param name="id"> Patient id</param>
        /// <returns></returns>
        public async Task<IList<DoctorRef>> getDoctors(string id)
        {

            return await _dbService.collection.AsQueryable()
              .Where(p => p.UserId == id).Select(s => s.Doctors).FirstOrDefaultAsync();

        }
        /// <summary>
        /// Insert doctor details in the patient profile after patient authorisation
        /// </summary>
        /// <param name="doctor">DoctorRef model </param>
        /// <param name="id">Patient id to find the specifc corrent patient</param>
        public async Task InsertDoctorToPatientProfile(DoctorRef doctor,string id)
        {
            var result = _dbService.collection.AsQueryable().Where(c => c._id == id && c.Doctors.Any(a => a.DoctorId == doctor.DoctorId)).Count();
                
            if(result > 0)
            {
                return;
            }
            else
            {
                var filter = Builders<Patient>.Filter.Where(c => c._id == id || c.UserId == id);
                var update = Builders<Patient>.Update.Push(p => p.Doctors, doctor);
                await _dbService.collection.UpdateOneAsync(filter, update);
            }
            
           
        }
        //public async Task<IEnumerable<Patient>> GetMultipleById(IEnumerable<string> ids)
        //{
        //   // var filter = Builders<Patient>.Filter.Where(p => ids.GetEnumerator());
        //    return _dbService.collection.AsQueryable().SelectMany(p => p.UserId );
        //}
        public async Task<bool> Update(string id, Patient model)
        {
            var filter = Builders<Patient>.Filter.Eq(r => r.UserId, id);
            var update = Builders<Patient>.Update.Set(s => s.Title, model.Title)
                                                .Set(s => s.FirstName, model.FirstName)
                                                .Set(s => s.LastName, model.LastName)
                                                .Set(s => s.Gender, model.Gender)
                                                .Set(s => s.Dob, model.Dob)
                                                .Set(s => s.Alcohol, model.Alcohol)
                                                .Set(s=> s.Occupation,model.Occupation)
                                                .Set(s => s.LivingStatus, model.LivingStatus)
                                                .Set(s => s.SocialHistory, model.SocialHistory)
                                                .Set(s => s.Addresses, model.Addresses)
                                                .Set(s => s.PhoneNumbers, model.PhoneNumbers)
                                                .Set(s => s.SexualOrientation, model.SexualOrientation)
                                                .Set(s => s.Email, model.Email)
                                                .Set(s => s.Alergys, model.Alergys);
            // await _dbService.collection.UpdateOneAsync(filter, update);
            var result = await _dbService.collection.UpdateOneAsync(filter, update);
            return result.IsAcknowledged;

        }
        public async Task<bool> UpdateImageUri(string id,string url)
        {
            var filter = Builders<Patient>.Filter.Eq(r => r.UserId, id);
            var update = Builders<Patient>.Update.Set(s => s.ImageUri, url);
            var result = await _dbService.collection.UpdateOneAsync(filter, update);
            return result.IsAcknowledged;
        }
        public async Task<bool> Delete(string id)
        {
           // var result= await  _dbService.collection.AsQueryable().Where(c=> c._id == id).Where(c=> c.Doctors.Any(a=>a.DoctorId == ))
            var filter = Builders<Patient>.Filter.Eq(r => r.UserId, id);

           var result= await _dbService.collection.DeleteOneAsync(filter);
            return result.IsAcknowledged;
        }
        /// <summary>
        /// Check Patient exist or not
        /// </summary>
        /// <param name="Id">Patient Id</param>
        /// <returns> bool </returns>
       public async Task<bool> IsExist(string Id)
        {
          return await _dbService.collection.Find(c => c._id == Id).AnyAsync();
        }
        /// <summary>
        /// Check Doctor Exist or not in the Client Collection
        /// </summary>
        /// <param name="clinetId">Patient Id</param>
        /// <param name="doctorId"> Doctor Id</param>
        /// <returns> Return Task<bool> </returns>
        public async Task<bool> IsDoctorExist(string clinetId,string doctorId)
        {
            return await _dbService.collection.Find(c => c._id == clinetId && c.Doctors.Any(a => a.DoctorId == doctorId)).AnyAsync();
        }


        public async Task Insert(Patient document)
        {

            document._id = document.UserId;
            document.TimeStamp = DateTime.Now;
            await _dbService.collection.InsertOneAsync(document);
        }

        public async void Replace(string id, Patient doc)
        {
            var filter = Builders<Patient>.Filter.Eq(r => r.UserId, id);
            await _dbService.collection.ReplaceOneAsync(filter, doc);
        }

       
        public async Task<int> CountPatient()
        {
            var result =await _dbService.collection.AsQueryable().CountAsync();
            return result;
        }

        public async Task<bool> InsertPrescription(Prescription prescription, string userId)
        {
            
            var filter = Builders<Patient>.Filter.Where(c => c._id == userId || c.UserId == userId);
            
            var update = Builders<Patient>.Update.Push(p => p.Prescriptions, prescription);
            var result= await _dbService.collection.UpdateOneAsync(filter, update);
            return result.IsAcknowledged;
        }

        public async Task<IEnumerable<object>> getByMonth(int year)
        {
            var res = await _dbService.collection.AsQueryable()
                .OrderBy(x => x.TimeStamp).ToListAsync();
            // var year = DateTime.Now.Year;
            var group = res.Where(c=>c.TimeStamp.Year == year).GroupBy(g => g.TimeStamp.Month)
               .Select(x => new { Month = x.Key, total = x.Count() }).ToList();

            //var group= res.GroupBy(g => new { g.TimeStamp.Year, g.TimeStamp.Month})
            //     .Select(x => new { Month = $"{x.Key.Year} {x.Key.Month}", total = x.Count() }).ToList();

            return group;

        }
        public async Task<IEnumerable<object>> groupByGender()
        {
            var result =await _dbService.collection.AsQueryable()
                .GroupBy(s => s.Gender).Select(s => new { Gender = s.Key, Count = s.Count() }).ToListAsync();
            return result;
        }
        public async Task<IEnumerable<object>> groupBySexualOrientation()
        {
            var result = await _dbService.collection.AsQueryable().GroupBy(s => s.SexualOrientation).Select(s => new { SexualOrientation = s.Key, Count = s.Count() }).ToListAsync();
            return result;
        }
        public async Task<IEnumerable<object>> groupByCity(string Country)
        {
            var result = await _dbService.collection.AsQueryable()
                .Where(c=>c.Addresses.Any(a=>a.Country == Country))
                .GroupBy(s => s.Addresses.Select(c=>c.City)).Select(s => new { City = s.Key, Count = s.Count() }).ToListAsync();
            return result;
        }

        public async Task<IEnumerable<object>> groupByCountry()
        {
            var result = await _dbService.collection.AsQueryable().GroupBy(s => s.Addresses.Select(c => c.Country)).Select(s => new { Country = s.Key, Count = s.Count() }).ToListAsync();
            return result;
        }
        public void Dispose()
        {
            if (_dbService != null)
            {
                // _dbService.Dispose();
                _dbService = null;
            }
        }
    }
}
