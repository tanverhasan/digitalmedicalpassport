﻿using ApplicationCore.Common;
using ApplicationCore.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using MongoDB.Driver.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Interface;
using ApplicationCore.Helper;
using System.Linq;
using MongoDB.Bson;

namespace ApplicationCore.Implementation
{
    public class DoctorRepository : IDoctorRepository
    {
        GridFsService<Doctor> _dbFileService;
        public DbContext<Doctor> _dbService;
        public DoctorRepository()
        {
            _dbService = new DbContext<Doctor>();
            //  _dbService = new DBService<PatientProfile>();
            // _dbService_Paitient= new DbContext<Patients>
            _dbFileService = new GridFsService<Doctor>();
        }





        public async Task<PagedList<Doctor>> GetAll(DoctorResourceParameter resourceParameter)
        {

            //var filter = Builders<Doctor>.Filter.Empty;
            //return await _dbService.collection.AsQueryable()
            //    .OrderBy(r => r.UserId)
            //    .ThenByDescending(r => r._id).ToListAsync();

            var collectionBeforePaging = _dbService.collection.AsQueryable()
                .OrderBy(o => o.UserId)
                .ThenBy(t => t._id).AsQueryable();
            if (!string.IsNullOrEmpty(resourceParameter.FirstName))
            {
                var firstNameFiltering = resourceParameter.FirstName.Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging.Where(a => a.FirstName.ToLowerInvariant() == firstNameFiltering);
            }

            if (!string.IsNullOrEmpty(resourceParameter.OrderByFirstName))
            {
                var order = resourceParameter.OrderByFirstName.Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging.OrderByDescending(o => o.FirstName);
            }

            if (!string.IsNullOrEmpty(resourceParameter.SearchQuery))
            {
                var searchQuery = resourceParameter.SearchQuery.Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging.Where(a => a.FirstName.ToLowerInvariant().Contains(searchQuery)
                  || a.LastName.ToLowerInvariant().Contains(searchQuery)
                  || a.UserId.ToLowerInvariant().Contains(searchQuery)
                  || a.Email.ToLowerInvariant().Contains(searchQuery)
                  || a.AreaOfWork.ToLowerInvariant().Contains(searchQuery)
                  || a.Addresses.Any(c => c.AddressType.ToLowerInvariant().Contains("work")
                  && c.City.ToLowerInvariant().Contains(searchQuery)));

            }
            return PagedList<Doctor>.Create(collectionBeforePaging, resourceParameter.PageNumber, resourceParameter.PageSize);
            // return await _dbService.collection.Find(filter).ToListAsync();

        }

        public async Task<Doctor> GetById(string id)
        {
            // var filter = Builders<PatientProfile>.Filter.Eq(f => f.UserId, id);
            var result = await _dbService.collection.AsQueryable().Where(p => p.UserId == id).FirstOrDefaultAsync();
            return result;
            // return await _dbService.collection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<bool> Delete(string id)
        {
            var filter = Builders<Doctor>.Filter.Eq(r => r.UserId, id);

            var result = await _dbService.collection.DeleteOneAsync(filter);
            return result.IsAcknowledged;
        }



        public async Task Insert(Doctor document)
        {
            if (!IsExist(document.UserId).Result)
            {
                document._id = document.UserId;
                document.TimeStamp = DateTime.Now;
                await _dbService.collection.InsertOneAsync(document);
            }
            return;

        }

        public async Task<bool> IsExist(string id)
        {
            return await _dbService.collection.Find(c => c._id == id).AnyAsync();
        }
        public async Task<bool> IsDoctorExist(string clientId, string doctorId)
        {
            return await _dbService.collection.Find(c => c._id == clientId && c.Doctors.Any(a => a.DoctorId == doctorId)).AnyAsync();
        }
        public async Task<bool> IsPatientExist(string clientId, string patientId)
        {
            return await _dbService.collection.Find(c => c._id == clientId && c.Patients.Any(a => a.patientId == patientId)).AnyAsync();
        }

        public async Task<bool> addPatient(PatientRef document, string id)
        {
            var result = _dbService.collection.AsQueryable().Where(c => c.UserId == id && c.Patients.Any(a => a.patientId == document.patientId)).Count();
            if(result > 0)
            {
                return false;
            }
            else
            {
                var filter = Builders<Doctor>.Filter.Where(r => r._id == id);
                var data = Builders<Doctor>.Update.Push(f => f.Patients, document);
                var update = await _dbService.collection.UpdateOneAsync(filter, data);
                return update.IsAcknowledged;
            }
           



        }


        public async Task Replace(string id, Doctor doc)
        {
            var filter = Builders<Doctor>.Filter.Eq(r => r.UserId, id);
            await _dbService.collection.ReplaceOneAsync(filter, doc);
        }

        public async Task<IList<PatientRef>> getPatients(string id)
        {

            return await _dbService.collection.AsQueryable()
              .Where(p => p.UserId == id).Select(s => s.Patients).FirstOrDefaultAsync();

        }

        public async Task<int> getTotalPatients(string doctorId)
        {
            var total = await _dbService.collection.AsQueryable().Where(c => c.UserId == doctorId).Select(s => s.Patients).CountAsync();
            return total;
        }

        public async Task<int> getTotalDoctors(string doctorId)
        {
            var total = await _dbService.collection.AsQueryable()
                .Where(c => c.UserId == doctorId).Select(s => s.Doctors).CountAsync();
            return total;
        }

        public async Task AddDoctorToDoctorProfile(DoctorDoctorRef doctorDoctorRef, string id)
        {
            //var filter = Builders<Doctor>.Filter.Exists(c => c._id == id && c.Doctors.Any(a => a.DoctorId == doctorDoctorRef.DoctorId));
            var result = _dbService.collection.AsQueryable().Where(c => c._id == id && c.Doctors.Any(a => a.DoctorId == doctorDoctorRef.DoctorId)).Count();
                
            if (result > 0)
            {
                //await _dbService.collection.FindOneAndUpdateAsync(c => c._id == id && c.Doctors.Any(
                //   a => a.DoctorId == doctorDoctorRef.DoctorId), Builders<Doctor>.Update.Push(p => p.Doctors, doctorDoctorRef));
                return;
            }
            else
            {
                var filter = Builders<Doctor>.Filter.Where(c => c._id == id || c.UserId == id);
                var data = Builders<Doctor>.Update.Push(p => p.Doctors, doctorDoctorRef);
                await _dbService.collection.UpdateOneAsync(filter, data);
            }


        }
        public async Task<bool> AddAuthorisation(string doctorId, string patientId, bool status)
        {


            var result = _dbService.collection.AsQueryable().Where(c => c.UserId == doctorId && c.Patients.Any(a => a.patientId == patientId)).FirstOrDefault();

            if (result != null)
            {
                foreach (var item in result.Patients)
                {
                    if (item.patientId == patientId)
                    {

                        item.status = status;
                    }
                }

                var filter = Builders<Doctor>.Filter.Eq(r => r.UserId, doctorId);
                var update = Builders<Doctor>.Update.Set(s => s.Patients, result.Patients);
                var up = await _dbService.collection.UpdateOneAsync(filter, update);
                return up.IsAcknowledged;
            }
            return false;


        }

        public async Task RemoveAuthorisation(string doctorId, string patientId)
        {
            await _dbService.collection.FindOneAndUpdateAsync(c => c._id == doctorId && c.Patients.Any(a => a.patientId == patientId),
                Builders<Doctor>.Update.Set(c => c.Patients.ElementAt(-1).status, false));
        }

        public async Task<bool> AddPrescription(Prescription prescription, string userId)
        {
            var filter = Builders<Doctor>.Filter.Where(c => c.UserId == userId || c._id == userId);
            var update = Builders<Doctor>.Update.Push(p => p.Prescriptions, prescription);
            var result = await _dbService.collection.UpdateOneAsync(filter, update);
            return result.IsAcknowledged;
        }
        public async Task<bool> Update(string id, Doctor model)
        {
            var filter = Builders<Doctor>.Filter.Eq(r => r.UserId, id);
            var update = Builders<Doctor>.Update.Set(s => s.Title, model.Title)
                                                .Set(s => s.FirstName, model.FirstName)
                                                .Set(s => s.LastName, model.LastName)
                                                .Set(s => s.Gender, model.Gender)
                                                .Set(s => s.Dob, model.Dob)
                                                .Set(s => s.AreaOfWork, model.AreaOfWork)
                                                .Set(s => s.Alcohol, model.Alcohol)
                                                .Set(s => s.LivingStatus, model.LivingStatus)
                                                .Set(s => s.SocialHistory, model.SocialHistory)
                                                .Set(s => s.Summary, model.Summary)
                                                .Set(s => s.Addresses, model.Addresses)
                                                .Set(s => s.PhoneNumbers, model.PhoneNumbers)
                                                .Set(s => s.SexualOrientation, model.SexualOrientation)
                                                .Set(s => s.Email, model.Email)
                                                .Set(s => s.Qualifications, model.Qualifications)
                                                .Set(s => s.Alergys, model.Alergys);
            // await _dbService.collection.UpdateOneAsync(filter, update);
            var result = await _dbService.collection.UpdateOneAsync(filter, update);
            return result.IsAcknowledged;

        }
        public async Task<bool> UpdateImageUri(string id, string url)
        {
            var filter = Builders<Doctor>.Filter.Eq(r => r.UserId, id);
            var update = Builders<Doctor>.Update.Set(s => s.ImageUri, url);
            var result = await _dbService.collection.UpdateOneAsync(filter, update);
            return result.IsAcknowledged;
        }
        //public void UploadFile(string fileName, HttpPostedFileBase source)
        //{
        //    _dbFileService.UploadFile(fileName, source);
        //}

        public async Task<int> CountDoctor()
        {
            var result = await _dbService.collection.AsQueryable().CountAsync();
            return result;
        }

        public async Task<IEnumerable<object>> groupByGender()
        {
            var result = await _dbService.collection.AsQueryable().GroupBy(s => s.Gender).Select(s => new { Gender = s.Key, Count = s.Count() }).ToListAsync();
            return result;
        }
        public async Task<IEnumerable<object>> groupBySexualOrientation()
        {
            var result = await _dbService.collection.AsQueryable().GroupBy(s => s.SexualOrientation).Select(s => new { SexualOrientation = s.Key, Count = s.Count() }).ToListAsync();
            return result;
        }
        public async Task<IEnumerable<object>> groupByCity(string Country)
        {
            var result = await _dbService.collection.AsQueryable()
                .Where(c => c.Addresses.Any(a => a.Country == Country))
                .GroupBy(s => s.Addresses.Select(c => c.City)).Select(s => new { City = s.Key, Count = s.Count() }).ToListAsync();
            return result;
        }

        public async Task<IEnumerable<object>> groupByCountry()
        {
            var result = await _dbService.collection.AsQueryable().GroupBy(s => s.Addresses.Select(c => c.Country)).Select(s => new { Country = s.Key, Count = s.Count() }).ToListAsync();
            return result;
        }

        public async Task<IEnumerable<object>> getByMonth(int year)
        {
            var res = await _dbService.collection.AsQueryable()
                .OrderBy(x => x.TimeStamp).ToListAsync();
            // var year = DateTime.Now.Year;
            var group = res.Where(c => c.TimeStamp.Year == year).GroupBy(g => g.TimeStamp.Month)
               .Select(x => new { Month = x.Key, total = x.Count() }).ToList();

            //var group= res.GroupBy(g => new { g.TimeStamp.Year, g.TimeStamp.Month})
            //     .Select(x => new { Month = $"{x.Key.Year} {x.Key.Month}", total = x.Count() }).ToList();

            return group;

        }
        public void Dispose()
        {
            if (_dbService != null)
            {
                // _dbService.Dispose();
                _dbService = null;
            }
        }
    }
}

