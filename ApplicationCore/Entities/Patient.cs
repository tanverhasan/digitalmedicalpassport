﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ApplicationCore.Entities
{
    [DataContract]
    public class Patient
    {
        public string _id { get; set; }

        //[DataMember]
        //public string MongoId
        //{
        //    get { return _id.ToString(); }
        //    set { _id = ObjectId.Parse(value); }
        //}
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string UserId { get; set; }
        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public DateTime Dob { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Weight { get; set; }
        [DataMember]
        public string Height { get; set; }
        [DataMember]
        public string ImageUri { get; set; }
        [DataMember]
        public IList<PhoneNumber> PhoneNumbers { get; set; }
        [DataMember]
        public IList<Address> Addresses { get; set; }

        [DataMember]
        public string Occupation { get; set; }

        [DataMember]
        public string SexualOrientation { get; set; }

        [DataMember]
        public string LivingStatus { get; set; }

        [DataMember]
        public string Alcohol { get; set; }

        [DataMember]
        public string SocialHistory { get; set; }


        [DataMember]
        public DateTime TimeStamp { get; set; }
       

        [DataMember]
        public IList<Alergy> Alergys { get; set; }

        [DataMember]
        public IList<Prescription> Prescriptions { get; set; }
        [DataMember]
        public IList<DoctorRef> Doctors { get; set; }


        [DataMember]
        [BsonExtraElements]
        public BsonDocument CatchAll { get; set; }
    }
    [DataContract]
    public class Alergy
    {
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Description { get; set; }
    }

    public class DoctorRef
    {
       // public string Id { get; set; }
        public string DoctorId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Status { get; set; }
    }
}
