﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ApplicationCore.Entities
{
    [DataContract]
    public class Qualification
    {
        //[DataMember]
        //public string Tile { get; set; }
        [DataMember]
        public string University { get; set; }
        [DataMember]
        public string Subject { get; set; }
        [DataMember]
        public string Degree { get; set; }
    }
}
