﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ApplicationCore.Entities
{
    [DataContract]
    public class Doctor
    {
        [DataMember]
        public string _id { get; set; }


        //// public string MongoId { get; set; }
        // //{
        // //    get { return _id.ToString(); }
        // //    set { _id = ObjectId.Parse(value); }
        // //}
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string UserId { get; set; }
        [DataMember]
        public string FirstName { get; set; } 
        [DataMember]
        public String LastName { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public DateTime Dob { get; set; }
        [DataMember]
        public string ImageUri { get; set; }
        [DataMember]
        public string AreaOfWork { get; set; }
        [DataMember]
        public string Summary { get; set; }
        [DataMember]
        public string SexualOrientation { get; set; }

        [DataMember]
        public string LivingStatus { get; set; }

        [DataMember]
        public string Alcohol { get; set; }

        [DataMember]
        public string SocialHistory { get; set; }

        [DataMember]
        public IList<Address> Addresses { get; set; }
        [DataMember]
        public DateTime TimeStamp { get; set; }
        
        [DataMember]
        public IList<PhoneNumber> PhoneNumbers { get; set; }
        [DataMember]
        public IList<Prescription> Prescriptions { get; set; }
        [DataMember]
        public IList<Alergy> Alergys { get; set; }
        [DataMember]
        public IList<Qualification> Qualifications { get; set; }
        [DataMember]
        public IList<PatientRef> Patients { get; set; }
        [DataMember]
        public IList<DoctorDoctorRef> Doctors { get; set; }
        [DataMember]
        [BsonExtraElements]
        public BsonDocument CatchAll { get; set; }
    }

    public class DoctorDoctorRef
    {
        public string DoctorId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Status { get; set; }
    }
    public class PatientRef
    {
        public ObjectId Id { get; set; }
        public string patientId { get; set; }
        public string patientFirstNmae { get; set; }
        public string patientLastName { get; set; }
        public string Role { get; set; }
        public bool status { get; set; }
    }
}
