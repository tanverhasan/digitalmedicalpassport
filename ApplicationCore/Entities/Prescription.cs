﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Prescription
    {
        public ObjectId _id { get; set; }
        [DataMember]
        public string MongoId
        {
            get { return _id.ToString(); }
            set { _id = ObjectId.Parse(value); }
        }
        [DataMember]
        
        public DateTime CurrentDate { get; set; }

        [DataMember]
        public IList<Medicine> Medicines { get; set; }

        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string DoctorRef { get; set; }
    }
}
