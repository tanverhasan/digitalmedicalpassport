﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ApplicationCore.Entities
{
    [DataContract]
   public class Address
    {
        [DataMember]
        public string AddressType { get; set; }
        [DataMember]
        public string Street1 { get; set; }
        [DataMember]
        public string Street2 { get; set; }
        
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string District { get; set; }
        [DataMember]
        public string Postal { get; set; }

        [DataMember]
        public string Country { get; set; }
    }
}
