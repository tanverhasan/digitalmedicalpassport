﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ApplicationCore.Entities
{
    [DataContract]
   public class PhoneNumber
    {
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Number { get; set; }
    }
}
