﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Helper;

namespace ApplicationCore.Interface
{
    public interface IDoctorRepository
    {
        Task<bool> Delete(string id);
        void Dispose();
        Task<bool> IsExist(string id);
        Task<bool> IsDoctorExist(string clientId, string doctorId);
        Task<bool> IsPatientExist(string clientId, string patientId);
        Task<PagedList<Doctor>> GetAll(DoctorResourceParameter resourceParameter);
        Task<Doctor> GetById(string id);
        Task Insert(Doctor document);
        Task Replace(string id, Doctor doc);
        Task<bool> Update(string id, Doctor model);
        Task<bool> UpdateImageUri(string id, string url);
        Task<bool> addPatient(PatientRef document, string id);
        Task<IList<PatientRef>> getPatients(string id);
        Task AddDoctorToDoctorProfile(DoctorDoctorRef doctorDoctorRef, string id);
        Task<bool> AddAuthorisation(string doctorId, string patientId, bool status);
        Task RemoveAuthorisation(string doctorId, string patientId);
        Task<bool> AddPrescription(Prescription prescription, string userId);
        Task<int> getTotalPatients(string doctorId);
        Task<int> getTotalDoctors(string doctorId);
        Task<IEnumerable<object>> groupByGender();
        Task<IEnumerable<object>> groupBySexualOrientation();
        Task<IEnumerable<object>> groupByCity(string Country);
        Task<IEnumerable<object>> groupByCountry();
        Task<int> CountDoctor();
        Task<IEnumerable<object>> getByMonth(int year);
    }
}