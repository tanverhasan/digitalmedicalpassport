﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Helper;

namespace ApplicationCore.Interface
{
    public interface IPatientRepository
    {
        Task<bool> Delete(string id);
        void Dispose();
        Task<bool> IsExist(string Id);
        Task<bool> IsDoctorExist(string clinetId, string doctorId);
        Task<PagedList<Patient>> GetAll(PatientResourceParameter resourceParameter);
        Task<Patient> GetById(string id);
        Task Insert(Patient document);
        Task<bool> Update(string id, Patient model);
        Task<bool> UpdateImageUri(string id, string url);
        void Replace(string id, Patient doc);
       // void Update(string id, string filed);
        Task<bool> InsertPrescription(Prescription prescription, string userId);
        Task<IList<DoctorRef>> getDoctors(string id);
        Task InsertDoctorToPatientProfile(DoctorRef doctor, string id);
        // Task<List<List<Patient>>> countByCity(string country);
        Task<int> CountPatient();
        Task<IEnumerable<object>> groupByGender();
        Task<IEnumerable<object>> groupBySexualOrientation();
        Task<IEnumerable<object>> groupByCity(string Country);
        Task<IEnumerable<object>> groupByCountry();
        Task<IEnumerable<object>> getByMonth(int year);
    }
}