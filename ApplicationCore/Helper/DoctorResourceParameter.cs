﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Helper
{
    public class DoctorResourceParameter
    {
        public int maxPageSize = 20;
        public int PageNumber { get; set; } = 1;
        private int pageSize = 10;
        public int PageSize
        {
            get
            {
                return pageSize;
            }
            set
            {
                pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public string FirstName { get; set; }
        
        public string SearchQuery { get; set; }
        public string OrderByFirstName { get; set; }
    }
}
