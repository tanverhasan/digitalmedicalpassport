﻿using ApplicationCore.Entities;
using ApplicationCore.Helper;
using ApplicationCore.Implementation;
using ApplicationCore.Interface;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace DigitalMedicalPassport.Test
{
    public class PatientRepositoryTest
    {
        private readonly IPatientRepository patientRepository;

        public PatientRepositoryTest()
        {
            this.patientRepository = new PatientRepository();
        }

        [Fact]
        public async Task Test_GetAllPatient()
        {
            var resource = new PatientResourceParameter();
            var query = await this.patientRepository.GetAll(resource);
            var result = query.Count();
            Assert.Equal(4, result);
        }

        [Fact]
        public async Task Test_GetPatientById()
        {
            string id = "59eb597323c2e524ff758788";
            var query = await this.patientRepository.GetById(id);
            Assert.Contains(id, query.UserId);
            Assert.Contains("test", query.FirstName);
            Assert.IsType<Doctor>(query);
        }
        [Fact]
        public async Task Test_PatientFilterByFirstName()
        {
            var parameter = new PatientResourceParameter();
            parameter.FirstName = "Tanver";

            var query = await this.patientRepository.GetAll(parameter);
            var result = query.Count();
            foreach (var item in query)
            {
                var firstName = item.FirstName;
                Assert.Equal(parameter.FirstName, firstName);
            }

        }
        [Fact]
        public async Task Test_PatientSearchByLastName()
        {
            var parameter = new PatientResourceParameter();
            parameter.SearchQuery = "Hasan";
            var query = await this.patientRepository.GetAll(parameter);

            foreach (var item in query)
            {
                var lastName = item.LastName;
                Assert.Equal(parameter.SearchQuery, lastName);
            }
        }

        [Fact]
        public async Task Test_PatientSearchByFirstName()
        {
            var parameter = new PatientResourceParameter();
            parameter.SearchQuery = "Tanver";
            var query = await this.patientRepository.GetAll(parameter);

            foreach (var item in query)
            {
                var firstName = item.FirstName;
                Assert.Equal(parameter.SearchQuery, firstName);
            }
        }

        [Fact]
        public async Task Test_PatientSearchByEmail()
        {
            var parameter = new PatientResourceParameter();
            parameter.SearchQuery = "t.h.noman@outlook.com";
            var query = await this.patientRepository.GetAll(parameter);

            foreach (var item in query)
            {
                var Email = item.Email;
                Assert.Equal(parameter.SearchQuery, Email);
            }
        }
        [Fact]
        public async Task Test_PatientSearchByCity()
        {
            var parameter = new PatientResourceParameter();
            parameter.SearchQuery = "dhaka";
            var query = await this.patientRepository.GetAll(parameter);

            foreach (var items in query)
            {
                foreach (var item in items.Addresses)
                {
                    var city = item.City;
                    Assert.Equal(parameter.SearchQuery, city);
                }
            }
        }

        [Fact]
        public async Task Test_DeleteMethod()
        {
            var id = "5a7979689bf9bc6ee2538c53";
            var query = await this.patientRepository.Delete(id);
            Assert.True(query);
        }

        [Fact] 
        public async Task Test_InsertMethod()
        {
            var profile = new Patient
            {
                UserId = "5a7979689bf9bc6ee2538c25",
                FirstName = "test1",
                LastName = "test2",
                Email = "test@outlook.com",
                Dob = DateTime.Now,
                SexualOrientation = "Heterosexual",
            };
            var result = this.patientRepository.Insert(profile);
            Assert.False(result.IsCompleted);

            var query = await this.patientRepository.GetById(profile.UserId);
            Assert.Equal(profile.UserId, query.UserId);
            Assert.Equal(profile.FirstName, query.FirstName);

        }
        [Fact]
        public async Task Test_IsExistMethod()
        {
            var id = "5a7979689bf9bc6ee2538c53";
            var result = await this.patientRepository.IsExist(id);

            Assert.True(result);
        }

        [Fact]
        public async Task Test_IsDoctorExistMethod()
        {
            var userId = "59eb597323c2e524ff758788";
            var doctorId = "59eb4cf766d3a61aa2216ee9";
            var result = await this.patientRepository.IsDoctorExist(userId, doctorId);

            Assert.True(result);
        }
        [Fact]
        public async Task Test_getDoctorsMethod()
        {
            var id = "59eb597323c2e524ff758788";

            var result = await this.patientRepository.getDoctors(id);
            var count = result.Count();
            Assert.Equal(3, count);
        }

        [Fact]
        public async Task Test_InsertDoctorToPatientProfileMethod()
        {
            var doctor = new DoctorRef
            {
                DoctorId = "59eb4cf766d3a61aa2216ee9",
                FirstName = "test ",
                LastName = "test",
                Status = true
            };

            var userId = "59eb597323c2e524ff758788";
            var result = this.patientRepository.InsertDoctorToPatientProfile(doctor, userId);
            Assert.True(result.IsCompletedSuccessfully);
        }

        [Fact]

        public async Task Test_InsertPrescriptionMethod()
        {
            var prescription = new Prescription
            {
                CurrentDate = DateTime.Now,
                Medicines = new List<Medicine>
                {
                    new Medicine { Name="A", Description="three times a day"}
                },
                Comments = "Nothing happend"
            };
            var userId = "59eb597323c2e524ff758788";
            var result = await this.patientRepository.InsertPrescription(prescription, userId);
            Assert.True(result);
        }

        [Fact]
        public async Task groupByGender()
        {
            var response = await this.patientRepository.groupByGender();

            Console.WriteLine(response);
        }

        [Fact]
        public async Task groupByCity()
        {
            var response = await this.patientRepository.groupByCity("Bangladesh");

            Console.WriteLine(response);
        }

        [Fact]
        public async Task getByMonth()
        {
           var response= await this.patientRepository.getByMonth(2018);
            Console.WriteLine(response);
        }

    }
}
