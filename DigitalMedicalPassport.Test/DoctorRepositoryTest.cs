using ApplicationCore.Entities;
using ApplicationCore.Helper;
using ApplicationCore.Implementation;
using ApplicationCore.Interface;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace DigitalMedicalPassport.Test
{
    public class DoctorRepositoryTest
    {
       public readonly IDoctorRepository doctorRepository;

        public DoctorRepositoryTest()
        {
            this.doctorRepository = new DoctorRepository();
        }
        
        [Fact]
        public async Task Test_GetAllDoctor()
        {
            var parameter = new DoctorResourceParameter();
            var result = await this.doctorRepository.GetAll(parameter);
            var count = result.Count();
            Assert.Equal(1, count);
            

        }


        [Fact]
        public async Task TestGetById()
        {


            string id = "59eb4cf766d3a61aa2216ee9";
            var result = await this.doctorRepository.GetById(id);

            Assert.Equal(id, result.UserId);
            Assert.Equal("Tanver", result.FirstName);
        }

        [Fact]
        public async Task Test_DoctoreFilterByFirstName()
        {
            var parameter = new DoctorResourceParameter();
            parameter.FirstName = "Tanver";

            var query = await this.doctorRepository.GetAll(parameter);
            var result = query.Count();
            foreach (var item in query)
            {
                var firstName = item.FirstName;
                Assert.Equal(parameter.FirstName, firstName);
            }

        }
        [Fact]
        public async Task Test_DoctorSearchByLastName()
        {
            var parameter = new DoctorResourceParameter();
            parameter.SearchQuery = "Hasan";
            var query = await this.doctorRepository.GetAll(parameter);

            foreach (var item in query)
            {
                var lastName = item.LastName;
                Assert.Equal(parameter.SearchQuery, lastName);
            }
        }

        [Fact]
        public async Task Test_DoctorSearchByFirstName()
        {
            var parameter = new DoctorResourceParameter();
            parameter.SearchQuery = "Tanver";
            var query = await this.doctorRepository.GetAll(parameter);

            foreach (var item in query)
            {
                var firstName = item.FirstName;
                Assert.Equal(parameter.SearchQuery, firstName);
            }
        }

        [Fact]
        public async Task Test_DoctorSearchByEmail()
        {
            var parameter = new DoctorResourceParameter();
            parameter.SearchQuery = "t.h.noman@outlook.com";
            var query = await this.doctorRepository.GetAll(parameter);

            foreach (var item in query)
            {
                var Email = item.Email;
                Assert.Equal(parameter.SearchQuery, Email);
            }
        }
        [Fact]
        public async Task Test_DoctorSearchByCity()
        {
            var parameter = new DoctorResourceParameter();
            parameter.SearchQuery = "dhaka";
            var query = await this.doctorRepository.GetAll(parameter);

            foreach (var items in query)
            {
                foreach (var item in items.Addresses)
                {
                    var city = item.City;
                    Assert.Equal(parameter.SearchQuery, city);
                }
            }
        }

        [Fact]
        public async Task Test_DeleteMethod()
        {
            var id = "5a7979689bf9bc6ee2538c53";
            var query = await this.doctorRepository.Delete(id);
            Assert.True(query);
        }

        [Fact]
        public async Task Test_InsertMethod()
        {
            var profile = new Doctor
            {
                UserId = "5a7979689bf9bc6ee2538c45",
                FirstName = "test1",
                LastName = "test2",
                Email = "test@outlook.com",
                Dob = DateTime.Now,
                SexualOrientation = "Heterosexual",
            };
            var result = this.doctorRepository.Insert(profile);
            Assert.False(result.IsCompleted);

            var query = await this.doctorRepository.GetById(profile.UserId);
            Assert.Equal(profile.UserId, query.UserId);
            Assert.Equal(profile.FirstName, query.FirstName);

        }
        [Fact]
        public async Task Test_IsExistMethod()
        {
            var id = "5a7979689bf9bc6ee2538c45";
            var result = await this.doctorRepository.IsExist(id);

            Assert.True(result);
        }

        [Fact]
        public async Task Test_IsDoctorExistMethod()
        {
            var userId = "59eb4cf766d3a61aa2216ee9";
            var doctorId = "59eb4cf766d3a61aa2216ee9";
            var result = await this.doctorRepository.IsDoctorExist(userId, doctorId);

            Assert.True(result);
        }
        [Fact]
        public async Task Test_getDoctorsMethod()
        {
            var id = "59eb4cf766d3a61aa2216ee9";

            var result = await this.doctorRepository.getPatients(id);
            var count = result.Count();
            Assert.Equal(2, count);
        }

        [Fact]
        public async Task Test_InsertDoctorToDoctorProfileMethod()
        {
            var doctor = new DoctorDoctorRef
            {
                DoctorId = "59eb4cf766d3a61aa2216ee7",
                FirstName = "test ",
                LastName = "test",
                Status = true
            };

            var userId = "59eb4cf766d3a61aa2216ee9";
            var result = this.doctorRepository.AddDoctorToDoctorProfile(doctor, userId);
            Assert.True(result.IsCompletedSuccessfully);
        }

        //[Fact]
        //public async Task testAddAuthorisation()
        //{
        //    this.doctorRepository.AddAuthorisation
        //}
    }
}
