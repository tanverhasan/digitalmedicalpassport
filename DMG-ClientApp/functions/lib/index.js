"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const cors = require("cors");
const Guid = require("guid");
const sha256 = require("sha256");
const jwt = require("jsonwebtoken");
const corsHandler = cors({ origin: true });
admin.initializeApp({
    credential: admin.credential.cert({
        projectId: "digitalmedicalpassport",
        clientEmail: "firebase-adminsdk-ip2ip@digitalmedicalpassport.iam.gserviceaccount.com",
        privateKey: "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCYtBGSOmKMVmSE\nKJkjJl/8RI33NTbGJfLuwb0K8r/t1U2uCrFZRHJyOxMCtyVv0h7Z7DLThRpydgEk\nYlZdkNQhYeLR4ROVDqX3kN+6YIT8/xx9Ed2K1uG6v78UvuFGCNjKlhnwTBgDDYz4\nTezd2I31W7VcIUQsUQlDUePvmGPBpjwdTC7jHV4E3kMVeL85x0qkSllgNHOMcrUY\nkReT/eEixQ4vFDzvnUHWOdJUBsyZMARUJtaY+HqlWIgmJtbueMfM9d+5rUn4++R4\nKH9fcPXtC1QE1OGXdjEZie/XBIAYDJVubV+DQOWISnwQubVwtymZIX3gE4Kg+84O\n+K/AHEgjAgMBAAECggEAAXIFjzkcQQXO/Rfr8yg5f7LKa+JGEB7AgOq2i7RRMBgf\nxxufeB/XZtC7qlU/dnfVBkJfwh7KvvXyUXzYzRlCLhq2Fz8mfKJfMcuzl8OyyTjv\nn8NxMbo8+BPj5CILvH+i8FqhXjk/f5qQ5gd0IYQbkTdQAnzndJ/uaC5jQPD+qLuS\nrIcMBHaWFNrqyowHbvWHX/PL6S6Ku+BoG9krEX+bkv0aoXkQ5cmqz5FvmAmL1B7N\nEDmVyWMwBES58Lte1P6c2gpqGWniGM6GglNwuNmWOD+x7/WAxWSQYsYPBmloup4i\nIevF0XAhS/mq7v+016u+Bq7DMg38t7Qt2bzlnSUEyQKBgQDHM9UX7ZF19f+j1/kj\nL2GVIwVOlBsdtNpnBxl+d8eAfb1i2WKJOhyJ1W9Tf54yKiEA7MnlHSZKeJBOeJzX\nkdPZNC5rIDQ8xaO3LNHG7UI3hVIdj1LbhpoCD6IzcR2jcwUKe7LoSwna/L0pietN\nqr+UIOJzicNO/tqnjNU51QShdQKBgQDEPi4YZjwIwLxgu9poUqHSm1Xhib7a9y0D\nivTP/y7rVacfD2NqLKwUEkwCsUXbFljx7EEDg/KfjFrqh3GxE7D2lK4WKps7ItsB\nD6iMlP7YDyty2m7gatLrxRRAQw6yQ9S/nIRf4olGM/DQ8dbcrnpkL20sY9s7U6AH\nCpCtEBw4NwKBgQCWELMYiozJW5/Q81OpMPmHW1GVJ7fhwivB77RB9MQjRRZWEEPN\n8ywqndJ3Ca3IRrqEMt3JrxL4iSjVJMGrGB2LBGdvplwT6CP0S1xhSr1WjCYXRLB7\n1kPRB6v+qtlUF8MVaaiWnGj5QurSDydnZBlc4wUVWxP6P73C+TJsldAePQKBgCJP\nRLvbU++r0sOO1Fac69HoepFeBdI+mM4fw0GCOe/HH9rcU+NCQCuaL1PhLndpZs+V\nIKC+tvKSFWsflw8AhFX4FJ+nf1BNfcCnvi4l9pBLxqrj6bKuuoWiBTJfBvi/xzhg\n4vvonVt6xUWiopAeqSNO72VtZ+ztZ/MTM8wuHuDtAoGAYIM/VlmoSpiMA3Gz54z2\nrF8iVnR2FpSqzVrcjeaDITk6BmvwReC8b1v0AcUlEsi+yPIHwkf29ogbtQErNwgR\nWa18lQELt/ARMQBuv1XN7jE/IFQVDjlJzaPa5RhNOoAB886tbXcRylU5EAqyu4vp\nhxA99ytdj7UM5YuIxo2GDdk=\n-----END PRIVATE KEY-----\n"
    }),
    databaseURL: "https://digitalmedicalpassport.firebaseio.com"
});
exports.customToken = functions.https.onRequest((req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    corsHandler(req, res, () => {
        if (req.body.id === undefined) {
            res.status(400).send("No id defined");
        }
        else {
            const uid = req.body.id;
            console.log(uid);
            // console.log( "Body : "+req.body.id);
            admin
                .auth()
                .createCustomToken(uid)
                .then(token => {
                console.log(token);
                res.status(200).send(token);
            })
                .catch(err => {
                console.log(err);
            });
        }
    });
});
exports.skypeSignedToken = functions.https.onRequest((req, res) => {
    // res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
    //  corsHandler(res, res, () => {
    const payload = {};
    const content = JSON.stringify(payload);
    const api_key = "1b129ef8-2d46-b59b-9346-16d1896b8d20";
    const api_secrete = "5c71f501-1d05-447e-f13c-4ca97934807e";
    const jwtBearerToken = jwt.sign({
        jti: Guid.raw(),
        iss: api_key,
        iat: Math.floor(Date.now() / 1000),
        sub: sha256(content),
        exp: Math.floor(Date.now() / 1000) + 10
    }, api_secrete);
    console.log("Token " + jwtBearerToken);
    // fetch("https://interviews.skype.com/api/interviews", {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //     Authorization: "Bearer " + jwtBearerToken
    //   },
    //   body: content
    // }).then(r => {
    //     console.log("Response" + r);
    //     res.status(200).send(r.json());
    //   });
    res.status(200).send(jwtBearerToken);
    // });
});
exports.hello = functions.https.onRequest((req, res) => {
    // res.header("Access-Control-Allow-Origin", "*");
    // corsHandler(res, res, () => {
    res.status(200).send("Hello world ");
    // });
});
// const api_key = "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";
// sgMail.setApiKey(api_key);
// // Start writing Firebase Functions
// // https://firebase.google.com/functions/write-firebase-functions
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
// exports.firestoreEmail = functions.firestore
//   .document("users/{userId}/followers/{followerId}")
//   .onWrite(event => {
//     const userId = event.params.userId;
//     const db = admin.firestore();
//     return db
//       .collection("users")
//       .doc(userId)
//       .get()
//       .then(doc => {
//         const user = doc.data();
//         const msg = {
//           to: user.email,
//           from: "t.h.noman@outlook.com"
//         };
//         return sgMail.send(msg);
//       })
//       .then(() => console.log("email sent"))
//       .catch(err => console.log(err));
//   });
// exports.httpEmail = functions.https.onRequest((req, res) => {
//   console.log(req.body);
//   const message={
//     to: req.body.to,
//     from : req.body.from,
//     subject: req.body.subject,
//     content: req.body.content
//   }
//   sgMail
//     .send(message)
//     .then(() => {
//       res.status(200).send("Email Sent");
//     })
//     .catch(err => console.log(err));
// });
//# sourceMappingURL=index.js.map