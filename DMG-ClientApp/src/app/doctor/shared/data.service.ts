import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { AngularFirestore } from "angularfire2/firestore";
import { IRequest, User } from "../../service/user";
import {
  IDoctor,
  IUserReq,
  IDoctorPatient,
  IPatient,
  IPrescription
} from "./doctor";
import { environment } from "../../../environments/environment";
import { HttpParams } from "@angular/common/http";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { catchError, map, retry } from "rxjs/operators";
import { ErrorObservable } from "rxjs/observable/ErrorObservable";
import { Observable } from "rxjs/Observable";
import { IBooking } from "../../core/shared/booking";
import { IBookingSlot } from "./booking";

@Injectable()
export class DataService {
  private messageSource = new BehaviorSubject<string>(null);
  currentMessage = this.messageSource.asObservable();

  private httpStatusCode = new BehaviorSubject<any>(null);
  statusCode = this.httpStatusCode.asObservable();
  user: User;
  userId = localStorage.getItem("userId");
  constructor(private http: HttpClient, private afs: AngularFirestore) {}

  changeMessage(message: string) {
    this.messageSource.next(message);
  }

  getProfile(id) {
    return this.http
      .get(`${environment.api.baseUrl}doctor/${id}`)
      .pipe(
        retry(3),
        map(res => res as IDoctor),
        catchError(c => this.handleError(c))
      );
  }

  saveProfile(doctor: IDoctor) {
    return this.http
      .post(`${environment.api.baseUrl}doctor`, doctor)
      .pipe(map(res => res), catchError(c => this.handleError(c)));
  }
  updateProfile(id, model: any) {
    return this.http
      .put(`${environment.api.baseUrl}doctor/${id}`, model)
      .pipe(catchError(c => this.handleError(c)));
  }
  saveImage(id, uri) {
    const body = {
      ImageUri: uri
    };
    return this.http
      .put(`${environment.api.baseUrl}Doctor/image/${id}`, body)
      .pipe(catchError(c => this.handleError(c)));
  }
  getAllPatient() {
    return this.http
      .get(`${environment.api.baseUrl}patient`)
      .pipe(
        retry(3),
        map(res => res as IPatient[]),
        catchError(c => this.handleError(c))
      );
  }

  getAllDoctor() {
    return this.http
      .get(`${environment.api.baseUrl}doctor`)
      .pipe(
        retry(3),
        map(res => res as IDoctor[]),
        catchError(c => this.handleError(c))
      );
  }

  getDoctorBySearch(param: string) {
    return this.http
      .get(
        `${
          environment.api.baseUrl
        }doctor?PageNumber=1&PageSize=20&SearchQuery=${param}`
      )
      .pipe(
        retry(3),
        map(res => res as IDoctor[]),
        catchError(c => this.handleError(c))
      );
  }
  getPatientBySearch(param: string) {
    return this.http
      .get(
        `${
          environment.api.baseUrl
        }patient?PageNumber=1&PageSize=20&SearchQuery=${param}`
      )
      .pipe(
        retry(3),
        map(res => res as IPatient[]),
        catchError(c => this.handleError(c))
      );
  }

  getAuthorisationList() {
    const ref = this.afs.doc(`users/${this.userId}`);
    // this.afs.collection(`users`,ref=> ref.where(request.))

    return ref.valueChanges();
  }
  addAccessAuthorisation(data: IUserReq) {
    return this.afs.doc(`users/${this.userId}`).update(data);
  }

  getPatients() {
    return this.http
      .get(`${environment.api.baseUrl}DoctorPatients/${this.userId}`)
      .pipe(
        retry(3),
        map(res => res as IDoctorPatient[]),
        catchError(c => this.handleError(c))
      );
    // .map(res => {
    //   return res as IDoctorPatient[];
    // });
  }
  addDoctorToUserProfile(userId, doctorId, doctorFirstName, doctorLastName) {
    const body = {
      userId: userId,
      doctorId: doctorId,
      firstName: doctorFirstName,
      lastName: doctorLastName,
      status: true
    };
    console.log(body);
    const userRole = localStorage.getItem("roles");
    if (userRole === "doctor") {
      return this.http
        .post(`${environment.api.baseUrl}DoctorDoctor`, body)
        .pipe(retry(3), map(res => res), catchError(c => this.handleError(c)));
    }
    if (userRole === "user") {
      return this.http
        .post(`${environment.api.baseUrl}PatientDoctor`, body)
        .pipe(retry(3), map(res => res), catchError(c => this.handleError(c)));
    }
  }

  approveDoctor(id, patientId) {
    const body = {
      patientId: patientId,
      status: true
    };
    return this.http
      .put(`${environment.api.baseUrl}DoctorPatients/${id}`, body)
      .pipe(retry(3), map(res => res), catchError(c => this.handleError(c)));
  }

  getPatientProfile(id: any, role: string) {
    if (role === "doctor") {
      return this.http
        .get(`${environment.api.baseUrl}Doctor/${id}`)
        .pipe(
          retry(3),
          map(res => res as IPatient),
          catchError(c => this.handleError(c))
        );
      // .map(res => res as IPatient);
    }
    if (role === "user") {
      return this.http
        .get(`${environment.api.baseUrl}Patient/${id}`)
        .pipe(
          map(res => res as IPatient),
          catchError(c => this.handleError(c))
        );
      // .map(res => res as IPatient);
    }
  }

  sentRequest(userId: string, requestId: string, firstName, lastName) {
    const userdocRef = this.afs.collection("users").doc(userId);

    const data = {
      requests: [
        {
          firstName: firstName,
          lastName: lastName,
          requestId: requestId,
          authorization: false
        }
      ]
    };
    //  this.user.requests.push(data);
    // this.addToDoctorList(requestId, userId, false);
    return userdocRef.update(data);
  }

  addToDoctorList(
    doctorId: string,
    patientId: string,
    patientFirstNmae: string,
    patientLastName: string,
    role: string,
    status: boolean
  ) {
    // const baseurl = 'http://localhost:5001/api/DoctorPatients';
    const data = {
      doctorId: doctorId,
      patientId: patientId,
      patientFirstNmae: patientFirstNmae,
      patientLastName: patientLastName,
      role: role,
      status: status
    };
    console.log(data);
    return this.http
      .post(`${environment.api.baseUrl}DoctorPatients`, data)
      .pipe(retry(3), catchError(c => this.handleError(c)));
    // .map(res => {
    //   return res;
    // });
  }

  GetMedicineList() {
    return this.afs
      .collection("medicines", res => res.orderBy("name", "desc"))
      .valueChanges();
  }

  saveTest(id, data: any) {
    const docId = this.afs.createId();
    return this.afs
      .collection("users")
      .doc(id)
      .collection("tests")
      .doc(docId)
      .set(data);
  }

  savePrescription(id: string, role: string, data: any) {
    const body: IPrescription = Object.assign({}, data);
    body.userId = id;
    body.currentDate = new Date().toLocaleString();
    console.log("body" + body);
    console.log(role);
    if (role === "user") {
      return this.http
        .post(`${environment.api.baseUrl}PatientPrescriptions`, body)
        .pipe(catchError(c => this.handleError(c)));
    } else {
      return this.http
        .post(`${environment.api.baseUrl}DoctorPrescription`, body)
        .pipe(catchError(c => this.handleError(c)));
    }
  }
  getProfileFromAf(id) {
    return this.afs
      .collection(`users`)
      .doc(id)
      .valueChanges();
  }
  getBooking(id: string) {
    return this.afs
      .collection("users")
      .doc(id)
      .collection<IBooking>("bookings", ref =>
        ref.orderBy("bookingDate", "desc")
      )
      .valueChanges();
  }

  createSlot(data: any, id) {
    const slotId = this.afs.createId();
    const bookingSlot: IBookingSlot = Object.assign({}, data);
    bookingSlot.slotId = slotId;
    return this.afs
      .collection("users")
      .doc(id)
      .collection("slots")
      .doc(slotId)
      .set(bookingSlot);
  }

  getSlots(id) {
    return this.afs
      .collection("users")
      .doc(id)
      .collection("slots")
      .valueChanges();
  }

  getSlotsBooking(id) {
    return this.afs
      .collection("users")
      .doc(id)
      .collection("slots", ref => ref.orderBy("date", "desc"))
      .valueChanges();
  }

  saveImageUri(id, url) {
    return this.afs
      .collection(`users`)
      .doc(id)
      .update({ imageUrl: url });
  }

  getTotalPatientsForEachDoctor(doctorId) {
    return this.http
      .get(`${environment.api.baseUrl}DoctorPatients/${doctorId}`)
      .pipe(
        retry(3),
        map(res => res as IDoctorPatient[]),
        catchError(c => this.handleError(c))
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
      this.httpStatusCode.next(error.status);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      "Something bad happened; please try again later."
    );
  }
}
