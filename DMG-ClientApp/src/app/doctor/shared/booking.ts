export interface IBookingSlot {
  slotId: string;
  date: Date;
  slot: ISlot[];
}

export interface ISlot {
  time: string;
  booked: boolean;
  userId: string;
  firstName: string;
  lastName: string;
  email: string;
  reason:string;
}
