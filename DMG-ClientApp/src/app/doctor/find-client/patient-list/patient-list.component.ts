import { Component, OnInit, ViewChild, ViewContainerRef } from "@angular/core";
import { DataService } from "../../shared/data.service";
import { IPatient } from "../../../patient/shared/patient";
import {
  MatTableDataSource,
  MatSort,
  MatPaginator,
  MatSnackBar
} from "@angular/material";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { IDoctor } from "../../shared/doctor";

@Component({
  selector: "app-patient-list",
  templateUrl: "./patient-list.component.html",
  styleUrls: ["./patient-list.component.scss"]
})
export class PatientListComponent implements OnInit {
  userProfile: IDoctor;
  patientProfile: IDoctor;
  dataSource: MatTableDataSource<{}>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  patietns: IPatient[];
  displayedColumns = ["firstName", "lastName", "email", "dob", "details"];
  searchParam: any;
  constructor(private data: DataService, public snackBar: MatSnackBar) {}

  ngOnInit() {
    this.getData();
  }
  getData() {
    this.data.getAllPatient().subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  search() {
    this.data.getPatientBySearch(this.searchParam).subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
  reload() {
    this.getData();
  }
  sendRequest(
    clientId: string,
    clientFirstName: string,
    clientLastName: string
  ) {
    const userId = localStorage.getItem("userId");
    this.data.getProfile(userId).subscribe(res => {
      this.userProfile = res as IDoctor;
      this.data.sentRequest(
        clientId,
        userId,
        this.userProfile.firstName,
        this.userProfile.lastName
      );
    });
    // this.data.sentRequest(id, ).then(() => {
    //   this.showSuccess();
    // });

    this.data
      .addToDoctorList(
        userId,
        clientId,
        clientFirstName,
        clientLastName,
        "user",
        false
      )
      .subscribe(res => {
        this.openSnackBar();
      });

    // Update firebase user doc
    // this.data
    //   .sentRequest(userId, clientId, clientFirstName, clientLastName)
    //   .then(() => {
    //     console.log('Added to access request list');
    //   });
  }

  openSnackBar() {
    this.snackBar.open("Request Sent", "Successfully", {
      duration: 3000
    });
  }
}
