import { Component, OnInit, ViewChild, ViewContainerRef } from "@angular/core";
import { DataService } from "../../shared/data.service";
import {
  MatTableDataSource,
  MatSort,
  MatPaginator,
  MatSnackBar
} from "@angular/material";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { IDoctor } from "../../shared/doctor";

@Component({
  selector: "app-doctor-list",
  templateUrl: "./doctor-list.component.html",
  styleUrls: ["./doctor-list.component.scss"]
})
export class DoctorListComponent implements OnInit {
  userProfile: IDoctor;
  dataSource: MatTableDataSource<{}>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns = ["firstName", "lastName", "email", "dob", "details"];

  searchParam: any;
  constructor(private data: DataService, private snackBar: MatSnackBar) {}

  ngOnInit() {
    this.getData();
  }
  getData() {
    this.data.getAllDoctor().subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  search() {
    this.data.getDoctorBySearch(this.searchParam).subscribe(res => {
      console.log(res);
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
  reload() {
    this.getData();
  }
  sendRequest(
    clientId: string,
    clientFirstName: string,
    clientLastName: string
  ) {
    const userId = localStorage.getItem("userId");
    this.data.getProfile(userId).subscribe(res => {
      this.userProfile = res as IDoctor;
      this.data.sentRequest(
        clientId,
        userId,
        this.userProfile.firstName,
        this.userProfile.lastName
      );
    });

    this.data
      .addToDoctorList(
        userId,
        clientId,
        clientFirstName,
        clientLastName,
        "doctor",
        false
      )
      .subscribe(res => {
        console.log(res);
        this.openSnackBar();
      });
  }

  openSnackBar() {
    this.snackBar.open("Request Sent", "Successfully", {
      duration: 3000
    });
  }
}
