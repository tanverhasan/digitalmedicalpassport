import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../shared/data.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-find-client',
  templateUrl: './find-client.component.html',
  styleUrls: ['./find-client.component.scss']
})
export class FindClientComponent implements OnInit {
  // dataSource: MatTableDataSource<{}>;
  // @ViewChild(MatSort) sort: MatSort;
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // patietns: IPatient[];
  // displayedColumns = ["firstName", "lastName", "email", "dob", "details"];
  // searchParam: any;

  constructor(private http: DataService) { }

  ngOnInit() {
  //  this.getData();
  }
  // getData(){
  //   this.http.getAllClients().subscribe(res=>{
  //     console.log(res);
  //     this.dataSource = new MatTableDataSource(res);
  //     this.dataSource.sort = this.sort;
  //     this.dataSource.paginator = this.paginator;
  //   });
  // }
  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim();
  //   filterValue = filterValue.toLowerCase();
  //   this.dataSource.filter = filterValue;
  // }
  // sendRequest(id: string) {

  //   this.http.sentRequest(id, localStorage.getItem("userId"));
  //   console.log(id);
  // }
  // reload() {
  //   this.getData();
  // }
}
