import { Component, OnInit } from "@angular/core";
import { DataService } from "../shared/data.service";
import { IBookingSlot } from "../shared/booking";
import { MatDialog } from "@angular/material";
import { SchudualDayComponent } from "../schudual-day/schudual-day.component";

@Component({
  selector: "app-schudual-view",
  templateUrl: "./schudual-view.component.html",
  styleUrls: ["./schudual-view.component.scss"]
})
export class SchudualViewComponent implements OnInit {
  bookingSlots: IBookingSlot[];
  constructor(private data: DataService, private dialog: MatDialog) {}

  ngOnInit() {
    this.data.getSlots(localStorage.getItem("userId")).subscribe(res => {
      this.bookingSlots = res as IBookingSlot[];
      console.log(this.bookingSlots);
    });
  }

  openDialog() {
    this.dialog.open(SchudualDayComponent, {
      width: "400px"
    });
  }
}
