import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { DataService } from "../shared/data.service";
import { IDoctor } from "../shared/doctor";
import { MatDialog } from "@angular/material";
import { FileUploadComponent } from "../file-upload/file-upload.component";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent implements OnInit {
  age: number;
  url: any;
  profile: IDoctor;
  loader = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: DataService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.loadData();
    // this.http
    //   .getProfileFromAf(localStorage.getItem("userId"))
    //   .subscribe((res: any) => {
    //     console.log(res);
    //     this.url = res.imageUrl;
    //   });
    this.http.statusCode.subscribe(res => {
      if (res === 404) {
        this.loader = false;
      }
    });
  }
  loadData() {
    this.http.getProfile(localStorage.getItem("userId")).subscribe(res => {
      this.profile = res as IDoctor;
      console.log(this.profile);
      this.loader = false;
    });
  }

  addPatientProfile() {
    this.router.navigate(["/doctor/new"]);
  }

  editPatientProfile(id) {
    this.router.navigate([`/doctor/${id}/edit`]);
  }

  imageUpload() {
    const dialogRef = this.dialog.open(FileUploadComponent);

    dialogRef.afterClosed().subscribe(res => {
      console.log(res);
      this.profile.imageUri = res;
      // this.loadData();
    });
  }

  getAge(dateString) {
    const today = new Date();
    const birthDate = new Date(dateString);
    this.age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      this.age--;
    }
    return this.age;
  }
}
