import { Component, OnInit, Inject } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Time } from "@angular/common";
import { ISlot, IBookingSlot } from "../shared/booking";
import { DateAdapter, MatSnackBar } from "@angular/material";
import { DataService } from "../shared/data.service";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "app-schudual-day",
  templateUrl: "./schudual-day.component.html",
  styleUrls: ["./schudual-day.component.scss"]
})
export class SchudualDayComponent implements OnInit {
  // boolingSlot: any;
  sDate: Date;
  sStartTime: string;
  sEndTime: string;
  sTimeInterval: number;
  result: any[] = [];
  constructor(
    private data: DataService,
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<SchudualDayComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any
  ) {}

  ngOnInit() {
    // this.sDate = new FormControl();
  }

  calculate() {
    console.log(this.sStartTime + this.sEndTime + "date" + this.sDate);
    const mydate = this.sDate;
    this.calculateSlot(this.sStartTime, this.sEndTime, this.sTimeInterval);
    const bookingSlot = {
      date: this.sDate.toLocaleString(),
      slot: this.result
    };
    console.log(bookingSlot);
    this.data
      .createSlot(bookingSlot, localStorage.getItem("userId"))
      .then(() => {
        this.snackBar.open("Slots Created", "Successfully", {
          duration: 5000
        });
      });
  }

  calculateSlot(start: string, end: string, interval) {
    console.log("start: " + start + "end : " + end);
    const startTime = start.split(":");
    const endTime = end.split(":");

    // tslint:disable-next-line:radix
    const startInt = parseInt(startTime[0]) * 60 + parseInt(startTime[1]);
    // tslint:disable-next-line:radix
    const endInt = parseInt(endTime[0]) * 60 + parseInt(endTime[1]);
    for (let time = startInt; time < endInt; time += interval) {
      const data = {
        time: this.timestring(time),
        booked: false
      };
      this.result.push(data);
    }
  }

  timestring(time) {
    let hours = Math.floor(time / 60);
    let min = time % 60;
    if (hours < 10) {
      const sethours = "0" + hours;
      // tslint:disable-next-line:radix
      hours = parseInt(sethours);
    }

    if (min < 10) {
      const setmin = "0" + min;
      // tslint:disable-next-line:radix
      min = parseInt(setmin);
    }

    return hours + ":" + min;
  }

  cancel() {
    this.dialogRef.close();
  }
}
