import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { DataService } from "../shared/data.service";
import { ISlot, IBookingSlot } from "../shared/booking";
import { IBooking } from "../../core/shared/booking";
import { IDoctorPatient } from "../shared/doctor";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  Patients: IDoctorPatient[];
  count: number;
  displayCount = 0;
  totalPatient: IDoctorPatient[];

  Chartdata: { x: string[]; y: number[] }[];
  booking: IBookingSlot[];

  bookingData: IDisplay[] = [];

  constructor(private data: DataService) {}

  ngOnInit() {
    this.data
      .getTotalPatientsForEachDoctor(localStorage.getItem("userId"))
      .subscribe(res => {
        this.Patients = res as IDoctorPatient[];
        // this.Patients.length;
        // console.log(this.count);
        for (let index = 0; index <= this.Patients.length; index++) {
          this.displayCount = index;
        }
      });

  }
}
export interface IDisplay {
  date: Date;
  total: number;
}
