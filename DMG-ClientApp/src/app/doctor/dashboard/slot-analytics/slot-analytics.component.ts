import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { IBookingSlot } from "../../shared/booking";
import { DataService } from "../../shared/data.service";
declare var Plotly: any;
@Component({
  selector: "app-slot-analytics",
  templateUrl: "./slot-analytics.component.html",
  styleUrls: ["./slot-analytics.component.scss"]
})
export class SlotAnalyticsComponent implements OnInit {
  lineGraphData: { x: Date[]; y: number[]; type: string }[] = [];
  x: Date[] = [];
  y: number[] = [];
  booking: IBookingSlot[];
  @ViewChild("chart") el: ElementRef;
  showSpinner = true;

  constructor(private data: DataService) {}

  ngOnInit() {
    const Divelement = this.el.nativeElement;

    this.data.getSlotsBooking(localStorage.getItem("userId")).subscribe(res => {
      this.booking = res as IBookingSlot[];
      // tslint:disable-next-line:no-shadowed-variable
      this.booking.forEach(element => {
        const total = element.slot.filter(f => f.booked === true).length;

        this.x.push(element.date);
        this.y.push(total);
      });

      this.lineGraphData = [
        {
          x: this.x,
          y: this.y,
          type: "bar"
        }
      ];

      const style = {
        title: "US Export of Plastic Scrap",
        xaxis: {
          tickangle: -45
        },
        yaxis: {
          title: "Number of patients"
        },
        margin: { t: 0 }
      };

      Plotly.plot(Divelement, this.lineGraphData, style);
      this.showSpinner = false;
    });
  }
}
