import { Component, OnInit } from "@angular/core";
import { DataService } from "../shared/data.service";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, Validators, FormGroup, FormArray } from "@angular/forms";
import { MatSnackBar } from "@angular/material";
import { Observable } from "rxjs/Observable";
import { IDoctor } from "../shared/doctor";
import { startWith, map } from "rxjs/operators";

@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.component.html",
  styleUrls: ["./edit-profile.component.scss"]
})
export class EditProfileComponent implements OnInit {
  doctorForm: FormGroup;
  firstFormGroup;
  AddressFormGroup;
  secondFormGroup;
  thirdFormGroup;
  isLinear;
  get addresses(): FormArray {
    return <FormArray>this.doctorForm.get("addresses");
  }
  get phoneNumbers(): FormArray {
    return <FormArray>this.doctorForm.get("phoneNumbers");
  }
  get alergys(): FormArray {
    return <FormArray>this.doctorForm.get("alergys");
  }
  get qualifications(): FormArray {
    return <FormArray>this.doctorForm.get("qualifications");
  }
  Gender = ["Male", "Female"];
  options = ["Mr", "Mrs", "Miss", "Dr", "Ms"];
  genderOptions = ["Heterosexual", "Homosexual", "Lesbian", "Bisexual"];
  filteredOptions: Observable<string[]>;
  genderFilteredOptions: Observable<string[]>;

  constructor(
    private data: DataService,
    private route: Router,
    private router: ActivatedRoute,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.doctorForm = this.fb.group({
      title: ["", [Validators.required]],
      firstName: ["", [Validators.required, Validators.minLength(3)]],
      lastName: ["", [Validators.required, Validators.minLength(3)]],
      gender: ["", Validators.required],
      dob: ["", [Validators.required]],
      imageUri: [""],
      areaOfWork: [""],
      summary: [""],
      email: ["", [Validators.email]],
      occupation: ["", [Validators.required]],
      sexualOrientation: ["", Validators.required],
      alcohol: "",
      phoneNumbers: this.fb.array([this.buildPhoneNumber()]),
      qualifications: this.fb.array([this.buildQualification()]),
      addresses: this.fb.array([this.buildAddress()]),
      alergys: this.fb.array([this.buildAlergy()]),
      livingStatus: ["", Validators.required],
      socialHistory: [""]
    });

    this.router.paramMap
      .switchMap(m => this.data.getProfile(m.get("id")))
      .subscribe(res => {
        const d = new Date(res.dob);
        const setDate = d.toISOString().substring(0, 10);

        this.doctorForm.patchValue({
          title: res.title,
          firstName: res.firstName,
          lastName: res.lastName,
          gender: res.gender,
          dob: setDate,
          areaOfWork: res.areaOfWork,
          summary: res.summary,
          email: res.email,
          occupation: res.occupation,
          sexualOrientation: res.sexualOrientation,
          alcohol: res.alcohol,
          livingStatus: res.livingStatus,
          socialHistory: res.socialHistory
        });

        this.populateAddress(res);
        this.populatePhoneNumber(res);
        this.populateQualification(res);
        this.populateAlergy(res);
      });

    this.filteredOptions = this.doctorForm
      .get("title")
      .valueChanges.pipe(startWith(""), map(val => this.filter(val)));
    this.genderFilteredOptions = this.doctorForm
      .get("sexualOrientation")
      .valueChanges.pipe(startWith(""), map(val => this.filterGender(val)));
  }

  private populateAlergy(res: IDoctor) {
    for (let index = 0; index < res.alergys.length; index++) {
      this.alergys.controls.push(this.buildAlergy());
      this.alergys.at(index).patchValue({
        type: res.alergys[index].type,
        description: res.alergys[index].description
      });
      this.alergys.controls.pop();
    }
  }

  private populateQualification(res: IDoctor) {
    for (let index = 0; index < res.qualifications.length; index++) {
      this.qualifications.controls.push(this.buildQualification());
      this.qualifications.at(index).patchValue({
        degree: res.qualifications[index].degree,
        university: res.qualifications[index].university,
        subject: res.qualifications[index].subject
      });
      this.qualifications.controls.pop();
    }
  }

  private populatePhoneNumber(res: IDoctor) {
    for (let index = 0; index < res.phoneNumbers.length; index++) {
      this.phoneNumbers.controls.push(this.buildPhoneNumber());
      this.phoneNumbers.at(index).patchValue({
        type: res.phoneNumbers[index].type,
        number: res.phoneNumbers[index].number
      });
      this.phoneNumbers.controls.pop();
    }
  }

  private populateAddress(res: IDoctor) {
    for (let index = 0; index < res.addresses.length; index++) {
      this.addresses.controls.push(this.buildAddress());
      this.addresses.at(index).patchValue({
        street1: res.addresses[index].street1,
        street2: res.addresses[index].street2,
        postal: res.addresses[index].postal,
        city: res.addresses[index].city
      });
      this.addresses.controls.pop();
    }
  }

  filter(val: string): string[] {
    return this.options.filter(
      option => option.toLowerCase().indexOf(val.toLowerCase()) === 0
    );
  }
  filterGender(val: string): string[] {
    return this.genderOptions.filter(
      option => option.toLowerCase().indexOf(val.toLowerCase()) === 0
    );
  }
  addAddress(): void {
    this.addresses.push(this.buildAddress());
  }
  removeAddress(index) {
    this.addresses.controls.splice(index);
  }
  addPhoneNumber(): void {
    this.phoneNumbers.push(this.buildPhoneNumber());
  }
  removePhoneNumber(index) {
    this.phoneNumbers.controls.splice(index);
  }
  addAlergy(): void {
    this.alergys.push(this.buildAlergy());
  }
  removeAlergy(index) {
    this.alergys.controls.splice(index);
  }
  addQualification(): void {
    this.qualifications.push(this.buildQualification());
  }
  removeQualification(id) {
    this.qualifications.controls.splice(id);
  }
  buildAddress(): FormGroup {
    return this.fb.group({
      addressType: "home",
      street1: "",
      street2: "",
      city: "",
      state: "",
      postal: ""
    });
  }
  buildPhoneNumber(): FormGroup {
    return this.fb.group({
      type: ["", Validators.required],
      number: ["", Validators.required]
    });
  }

  buildQualification() {
    return this.fb.group({
      university: ["", Validators.required],
      subject: ["", Validators.required],
      degree: ["", Validators.required]
    });
  }
  buildAlergy(): FormGroup {
    return this.fb.group({
      type: "",
      description: ""
    });
  }

  save() {
    console.log("clicked");
    const id = localStorage.getItem("userId");
    const data: IDoctor = Object.assign({}, this.doctorForm.value);
    data.userId = id;
    console.log(data);
    this.data.updateProfile(id, data).subscribe(res => {
      console.log(res);
      this.openSnackBar();
      this.route.navigate(["/doctor/profile"]);
    });
  }

  openSnackBar() {
    this.snackBar.open("Profie Created", "Successfully", {
      duration: 3000
    });
  }
}
