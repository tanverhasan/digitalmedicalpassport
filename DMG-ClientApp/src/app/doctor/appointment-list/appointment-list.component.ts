import { Component, OnInit, ViewChild } from "@angular/core";
import { DataService } from "../shared/data.service";
import { Observable } from "rxjs/Observable";
import { IBooking } from "../../core/shared/booking";
import { MatTableDataSource, MatPaginator, MatSort } from "@angular/material";

@Component({
  selector: "app-appointment-list",
  templateUrl: "./appointment-list.component.html",
  styleUrls: ["./appointment-list.component.scss"]
})
export class AppointmentListComponent implements OnInit {
  booking: Observable<IBooking[]>;
  dataSource: MatTableDataSource<{}>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns = [
    "id",
    "firstName",
    "lastName",
    "email",
    "bookingdate",
    "bookingtime",
    "detail"
  ];
  constructor(private data: DataService) {}

  ngOnInit() {
    this.data.getBooking(localStorage.getItem("userId")).subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
}
