import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { RouterModule } from "@angular/router";
import { MaterialModule } from "../design/material.module";
import { UiLoadingModule } from "../ui/ui-loading.module";
import { AddProfileComponent } from "./add-profile/add-profile.component";
import { EditProfileComponent } from "./edit-profile/edit-profile.component";
import { ProfileComponent } from "./profile/profile.component";
import { DataService } from "./shared/data.service";
import { FindClientComponent } from "./find-client/find-client.component";
import { DoctorListComponent } from "./find-client/doctor-list/doctor-list.component";
import { PatientListComponent } from "./find-client/patient-list/patient-list.component";
import { AccessRequestListComponent } from "./access-request-list/access-request-list.component";
import { AuthorisedPatientsComponent } from "./authorised-patients/authorised-patients.component";
import { PatientDetailsComponent } from "./authorised-patients/patient-details/patient-details.component";
import { SharedModule } from "../shared/shared.module";
import { AddPrescriptionComponent } from "./authorised-patients/add-prescription/add-prescription.component";
import { AddTestComponent } from "./authorised-patients/add-test/add-test.component";
import { FileUploadComponent } from "./file-upload/file-upload.component";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { AppointmentListComponent } from "./appointment-list/appointment-list.component";
import { SchudualDayComponent } from "./schudual-day/schudual-day.component";
import { SchudualViewComponent } from "./schudual-view/schudual-view.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { SlotAnalyticsComponent } from "./dashboard/slot-analytics/slot-analytics.component";
import { PatientAuthGuard } from "../auth/patient-auth.guard";
import { AuthDoctorGuard } from "../auth/auth-doctor.guard";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule,
    UiLoadingModule,
    SharedModule,
    Ng2SmartTableModule,
    RouterModule.forChild([
      {
        path: "",
        canActivate: [AuthDoctorGuard],
        canActivateChild: [AuthDoctorGuard],
        children: [
          { path: "dashboard", component: DashboardComponent },
          { path: "details", component: ProfileComponent },
          { path: "findClients", component: FindClientComponent },
          { path: "access-requests", component: AccessRequestListComponent },
          { path: "schedual", component: SchudualDayComponent },
          { path: "booking", component: AppointmentListComponent },
          { path: "view-schedual", component: SchudualViewComponent },
          { path: "new", component: AddProfileComponent },
          { path: "patients", component: AuthorisedPatientsComponent },
          { path: "patients/:id", component: PatientDetailsComponent },
          { path: "patients/:id/pres", component: AddPrescriptionComponent },
          { path: "patients/:id/test", component: AddTestComponent },
          { path: ":id/edit", component: EditProfileComponent }
        ]
      }
    ])
  ],
  declarations: [
    ProfileComponent,
    AddProfileComponent,
    EditProfileComponent,
    FindClientComponent,
    DoctorListComponent,
    PatientListComponent,
    AccessRequestListComponent,
    AuthorisedPatientsComponent,
    PatientDetailsComponent,
    AddPrescriptionComponent,
    AddTestComponent,
    FileUploadComponent,
    AppointmentListComponent,
    SchudualDayComponent,
    SchudualViewComponent,
    DashboardComponent,
    SlotAnalyticsComponent
  ],
  providers: [DataService],
  entryComponents: [FileUploadComponent, SchudualDayComponent]
})
export class DoctorModule {}
