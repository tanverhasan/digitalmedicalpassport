import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  AbstractControl
} from "@angular/forms";
import { DataService } from "../shared/data.service";
import { IDoctor } from "../shared/doctor";
import { Observable } from "rxjs/Observable";
import { startWith } from "rxjs/operators/startWith";
import { map } from "rxjs/operators/map";
import { filter } from "rxjs/operators/filter";
import { MatSnackBar } from "@angular/material";
import { Router } from "@angular/router";

@Component({
  selector: "app-add-profile",
  templateUrl: "./add-profile.component.html",
  styleUrls: ["./add-profile.component.scss"]
})
export class AddProfileComponent implements OnInit {
  doctorForm: FormGroup;
  firstFormGroup;
  AddressFormGroup;
  secondFormGroup;
  thirdFormGroup;
  isLinear;
  get addresses(): FormArray {
    return <FormArray>this.doctorForm.get("addresses");
  }
  get phoneNumbers(): FormArray {
    return <FormArray>this.doctorForm.get("phoneNumbers");
  }
  get alergys(): FormArray {
    return <FormArray>this.doctorForm.get("alergys");
  }
  get qualifications(): FormArray {
    return <FormArray>this.doctorForm.get("qualifications");
  }
  Gender = ["Male", "Female"];
  options = ["Mr", "Mrs", "Miss", "Dr", "Ms"];
  genderOptions = ["Heterosexual", "Homosexual", "Lesbian", "Bisexual"];
  filteredOptions: Observable<string[]>;
  genderFilteredOptions: Observable<string[]>;

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private http: DataService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.doctorForm = this.fb.group({
      title: ["", [Validators.required]],
      firstName: ["", [Validators.required, Validators.minLength(3)]],
      lastName: ["", [Validators.required, Validators.minLength(3)]],
      gender: ["", Validators.required],
      dob: ["", [Validators.required]],
      imageUri: [""],
      areaOfWork: [""],
      summary: [""],
      email: ["", [Validators.email]],
      occupation: ["", [Validators.required]],
      sexualOrientation: ["", Validators.required],
      alcohol: "",
      phoneNumbers: this.fb.array([this.buildPhoneNumber()]),
      qualifications: this.fb.array([this.buildQualification()]),
      notification: "email",
      sendCatalog: true,
      addresses: this.fb.array([this.buildAddress()]),
      alergys: this.fb.array([this.buildAlergy()]),
      livingStatus: ["", Validators.required],
      socialHistory: [""]
    });
    this.doctorForm.patchValue({
      email: localStorage.getItem("email")
    });
    this.filteredOptions = this.doctorForm
      .get("title")
      .valueChanges.pipe(startWith(""), map(val => this.filter(val)));
    this.genderFilteredOptions = this.doctorForm
      .get("sexualOrientation")
      .valueChanges.pipe(startWith(""), map(val => this.filterGender(val)));
  }
  filter(val: string): string[] {
    return this.options.filter(
      option => option.toLowerCase().indexOf(val.toLowerCase()) === 0
    );
  }
  filterGender(val: string): string[] {
    return this.genderOptions.filter(
      option => option.toLowerCase().indexOf(val.toLowerCase()) === 0
    );
  }
  addAddress(): void {
    this.addresses.push(this.buildAddress());
  }
  removeAddress(index) {
    this.addresses.controls.splice(index);
  }
  addPhoneNumber(): void {
    this.phoneNumbers.push(this.buildPhoneNumber());
  }
  removePhoneNumber(index) {
    this.phoneNumbers.controls.splice(index);
  }
  addAlergy(): void {
    this.alergys.push(this.buildAlergy());
  }
  removeAlergy(index) {
    this.alergys.controls.splice(index);
  }
  addQualification(): void {
    this.qualifications.push(this.buildQualification());
  }
  removeQualification(id) {
    this.qualifications.controls.splice(id);
  }
  buildAddress(): FormGroup {
    return this.fb.group({
      addressType: "home",
      street1: "",
      street2: "",
      city: "",
      district: "",
      postal: "",
      country: ""
    });
  }
  buildPhoneNumber(): FormGroup {
    return this.fb.group({
      type: ["", Validators.required],
      number: ["", Validators.required]
    });
  }

  buildQualification() {
    return this.fb.group({
      university: ["", Validators.required],
      subject: ["", Validators.required],
      degree: ["", Validators.required]
    });
  }
  buildAlergy(): FormGroup {
    return this.fb.group({
      type: "",
      description: ""
    });
  }

  save() {
    console.log("clicked");
    const data: IDoctor = Object.assign({}, this.doctorForm.value);
    data.userId = localStorage.getItem("userId");
    console.log(data);
    this.http.saveProfile(data).subscribe(res => {
      console.log(res);
      this.openSnackBar();
      this.router.navigate(["/doctor/profile"]);
    });
  }

  openSnackBar() {
    this.snackBar.open("Profie Created", "Successfully", {
      duration: 3000
    });
  }
}
