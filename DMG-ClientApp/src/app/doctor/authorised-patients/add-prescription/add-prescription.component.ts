import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, FormArray } from "@angular/forms";
import { Validators } from "@angular/forms";
import { DataService } from "../../shared/data.service";

import { IMedicine } from "../../../core/shared/medicine";
import { Observable } from "rxjs/Observable";
import { startWith } from "rxjs/operators/startWith";
import { map } from "rxjs/operators/map";
import { ActivatedRoute, Router } from "@angular/router";
import { distinctUntilChanged, debounceTime } from "rxjs/operators";
import { MatSnackBar } from "@angular/material";
import { IPrescription } from "../../shared/doctor";

@Component({
  selector: "app-add-prescription",
  templateUrl: "./add-prescription.component.html",
  styleUrls: ["./add-prescription.component.scss"]
})
export class AddPrescriptionComponent implements OnInit {
  filteredOptions: Observable<any[]>;
  medicinelist: IMedicine[];
  role: string;
  id: string;

  filteredOption: any;
  prescriptionForm: FormGroup;

  get medicines() {
    return this.prescriptionForm.get("medicines") as FormArray;
  }

  constructor(
    private fb: FormBuilder,
    private data: DataService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.data.currentMessage.subscribe(s => {
      this.role = s;
    });
    console.log(this.role);
    this.id = this.route.snapshot.paramMap.get("id");
  }

  ngOnInit() {
    this.prescriptionForm = this.fb.group({
      medicines: this.fb.array([this.buildMedicine()]),
      comments: [""]
    });

    this.data.GetMedicineList().subscribe(res => {
      this.medicinelist = res as IMedicine[];
      console.log(this.medicinelist);
      this.filteredOptions = this.prescriptionForm
        .get(["medicines", 0, "name"])
        .valueChanges.pipe(
          startWith(""),
          debounceTime(500),
          distinctUntilChanged(),
          map(val => (val ? this.filter(val) : this.medicinelist.slice()))
        );
    });

    // this.filteredOptions = this.medicines.valueChanges.pipe(
    //   startWith(""),
    //   map(state => (state ? this.filter(state) : this.states.slice()))
    // );
    // this.filteredOptions = this.medicines.valueChanges.pipe(
    //   startWith(''),
    //   distinctUntilChanged(),
    //   switchMap(val => this.data.GetMedicineList(val))
    // );
  }

  // search(id) {
  //   this.filteredOptions = this.prescriptionForm
  //     .get(["medicines", id, "name"])
  //     .valueChanges.pipe(
  //       startWith(""),
  //       map(val => (val ? this.filter(val) : this.medicinelist.slice()))
  //     );
  // }

  filter(val: string) {
    console.log(val);
    return this.medicinelist.filter(
      option => option.name.toLowerCase().indexOf(val.toLowerCase()) === 0
    );
  }
  addMedicine() {
    this.medicines.controls.push(this.buildMedicine());
  }
  removeMedicine(index) {
    this.medicines.controls.splice(index);
  }
  buildMedicine(): any {
    return this.fb.group({
      name: [""],
      description: [""]
    });
  }

  save() {
    console.log(this.prescriptionForm.value);
    console.log(this.id + this.role);
    const data: IPrescription = Object.assign({}, this.prescriptionForm.value);
    data.doctorRef = localStorage.getItem("userId");
    this.data
      .savePrescription(this.id, this.role, this.prescriptionForm.value)
      .subscribe(res => {
        console.log(res);
        this.router.navigate(["/doctor/patients/" + this.id]);
        this.snackBar.open("Prescription Added ", "Successfully", {
          duration: 5000
        });
      });
  }
}
