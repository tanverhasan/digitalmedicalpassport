import { Component, OnInit } from "@angular/core";
import {
  AngularFireStorage,
  AngularFireUploadTask
} from "angularfire2/storage";
import { Observable } from "rxjs/Observable";

@Component({
  selector: "app-add-test",
  templateUrl: "./add-test.component.html",
  styleUrls: ["./add-test.component.scss"]
})
export class AddTestComponent implements OnInit {
  downloadUrl: Observable<string>;
  uploadProgress: Observable<number>;
  task: AngularFireUploadTask;
  constructor() {}

  ngOnInit() {}


}
