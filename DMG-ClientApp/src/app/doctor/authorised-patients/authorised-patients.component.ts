import { Component, OnInit, ViewChild } from "@angular/core";
import { DataService } from "../shared/data.service";
import { IDoctorPatient, IUserReq, IRequest } from "../shared/doctor";
import {
  MatTableDataSource,
  MatSort,
  MatPaginator,
  MatSnackBar
} from "@angular/material";
import { DataSource } from "@angular/cdk/collections";
import { Router } from "@angular/router";

@Component({
  selector: "app-authorised-patients",
  templateUrl: "./authorised-patients.component.html",
  styleUrls: ["./authorised-patients.component.scss"]
})
export class AuthorisedPatientsComponent implements OnInit {
  isAuthorised: IRequest[];
  userData: IUserReq;

  patients: IDoctorPatient[];
  dataSource: MatTableDataSource<{}>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns = ["id", "firstName", "lastName", "status", "details"];
  loader = true;
  constructor(
    private data: DataService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.data.getPatients().subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      // this.patients= res as IDoctorPatient[] ;
      console.log(res);
      this.loader = false;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  details(id, role) {
    // this.data.getAuthorisationList().subscribe(res => {
    //   this.userData = res as IUserReq;

    //  const data = this.userData.requests.find(f => f.requestId === id);
    //  console.log(this.userData);
    //  if( data.authorization === true){
    this.data.changeMessage(role);
    this.router.navigate(["/doctor/patients/" + id]);
    //  }
    //  this.openSnackBar();
    // }
  }

  openSnackBar() {
    this.snackBar.open("Access Request", "Denied", {
      duration: 3000
    });
  }
}
