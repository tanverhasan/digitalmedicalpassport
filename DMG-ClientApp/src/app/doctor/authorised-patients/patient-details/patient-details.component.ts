import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, ParamMap, Router } from "@angular/router";
import { DataService } from "../../shared/data.service";
import "rxjs/add/operator/switchMap";
import { IPatient } from "../../shared/doctor";
import { messaging } from "../../../../../functions/node_modules/firebase-admin";
import { MatDialog } from "@angular/material";
import { FileUploadComponent } from "../../file-upload/file-upload.component";

@Component({
  selector: "app-patient-details",
  templateUrl: "./patient-details.component.html",
  styleUrls: ["./patient-details.component.scss"]
})
export class PatientDetailsComponent implements OnInit {
  message: string;
  profile: IPatient;
  loader = true;
  constructor(
    private route: ActivatedRoute,
    private data: DataService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.data.currentMessage.subscribe(message => {
      this.message = message;

      this.route.paramMap
        .switchMap((params: ParamMap) =>
          this.data.getPatientProfile(params.get("id"), this.message)
        )
        .subscribe(res => {
          this.profile = res as IPatient;
          this.loader = false;
          console.log(this.profile);
        });
      console.log(this.message);
    });

    this.data.statusCode.subscribe(res => {
      if (res === 404) {
        this.loader = false;
      }
    });
  }
  uploadDialog() {
    this.dialog.open(FileUploadComponent, {
      width: "600px",
      data: this.profile.id
    });
  }
  addNewPres() {
    this.router.navigate([
      "/doctor/patients/" + this.route.snapshot.paramMap.get("id") + "/pres"
    ]);
  }
  addNewTest() {
    this.router.navigate([
      "/doctor/patients/" + this.route.snapshot.paramMap.get("id") + "/test"
    ]);
  }
}
