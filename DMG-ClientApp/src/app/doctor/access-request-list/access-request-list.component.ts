import { Component, OnInit } from "@angular/core";
import { DataService } from "../shared/data.service";
import { IUserReq, IRequest } from "../shared/doctor";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-access-request-list",
  templateUrl: "./access-request-list.component.html",
  styleUrls: ["./access-request-list.component.scss"]
})
export class AccessRequestListComponent implements OnInit {
  data: IRequest[];
  requests: IUserReq;

  constructor(public http: DataService, private snackBr: MatSnackBar) {}

  ngOnInit() {
    this.http.getAuthorisationList().subscribe((res: IUserReq) => {
      this.requests = res as IUserReq;

      // this.data = this.requests.requests;
      console.log(this.requests);
    });
  }
  authorize(id: number, doctorId, firstName, lastName) {
    const userId = localStorage.getItem("userId");
    this.requests.requests[id].authorization = true;
    this.http.addAccessAuthorisation(this.requests);
    this.http
      .addDoctorToUserProfile(userId, doctorId, firstName, lastName)
      .subscribe(res => {
        console.log("Authorised");
        this.openSnackBar();
      });

    this.http.approveDoctor(doctorId, userId).subscribe(res => {
      console.log(res);
    });
    console.log(this.requests);
  }

  unauthorize(id: number) {
    this.requests.requests[id].authorization = false;
    this.http.addAccessAuthorisation(this.requests);
  }

  openSnackBar() {
    this.snackBr.open("Doctor", "Authorised", {
      duration: 3000
    });
  }
}
