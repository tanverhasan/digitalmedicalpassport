import { Component, OnInit } from "@angular/core";
import { AuthService } from "./auth/auth.service";
import { MessagingService } from "./service/messaging.service";
import { Observable } from "rxjs/Observable";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  email: Observable<string>;
  title = "DMP";
  message;
  constructor(public auth: AuthService) {
    auth.handleAuthentication();
    auth.scheduleRenewal();
    this.email = this.auth.userEmailMessage;
  }

  ngOnInit() {
    // this.auth
    // this.messagingService.getPermission(localStorage.getItem("userId"));
  }
}
