export interface User {
  id: string;
  email: string;
  roles: string;
  requests: IRequest[];
  fcmTokens?: { [token: string]: true };
}

export interface IRequest {
  requestId: string;
  authorization: boolean;
}
