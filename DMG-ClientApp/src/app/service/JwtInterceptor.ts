import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpSentEvent,
  HttpProgressEvent,
  HttpResponse,
  HttpUserEvent,
  HttpHeaderResponse,
  HttpErrorResponse,
  HttpEvent
} from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { AuthService } from "../auth/auth.service";
import "rxjs/add/operator/do";

export class JwtInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).do(
      (event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          //
        }
      },
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.auth.login();
          }
        }
      }
    );
  }
}

// (event: HttpEvent<any>) => {
//   if (event instanceof HttpResponse) {
//     // do stuff with response if you want
//   }
// }, (err: any) => {

//   if (err instanceof HttpErrorResponse {
//     if (err.status === 401) {
//       this.auth.login();
//     }
//   }
// });
// }
