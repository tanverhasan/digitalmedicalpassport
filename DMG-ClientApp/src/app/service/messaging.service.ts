import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase";
import "rxjs/add/operator/take";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { AngularFirestore } from "angularfire2/firestore";
import { Subject } from "rxjs/Subject";

@Injectable()
export class MessagingService {
  messaging = firebase.messaging();
  private messageSource = new Subject();
  currentMessage = this.messageSource.asObservable();
  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth) {}

  getPermission(userId) {
    console.log(userId);
    this.messaging
      .requestPermission()
      .then(() => {
        console.log("Notification permission granted.");
        return this.messaging.getToken();
      })
      .then(token => {
        console.log(token);
        this.saveToken(userId, token);
      })
      .catch(err => {
        console.log("Unable to get permission to notify.", err);
      });
  }
  // Listen for token refresh
  monitorRefresh(userId) {
    this.messaging.onTokenRefresh(() => {
      this.messaging
        .getToken()
        .then(refreshedToken => {
          console.log("Token refreshed.");
          this.saveToken(userId, refreshedToken);
        })
        .catch(err => console.log(err, "Unable to retrieve new token"));
    });
  }
  // save the permission token in firestore
  private saveToken(userId, token): void {
    const currentTokens = token;

    // If token does not exist in firestore, update db
    if (!currentTokens) {
      const userRef = this.afs.collection("users").doc(userId);
      const tokens = { ...currentTokens, [token]: true };
      userRef.update({ fcmTokens: tokens });
    }
  }
  receiveMessage() {
    this.messaging.onMessage(payload => {
      console.log("Message received. ", payload);
      this.messageSource.next(payload);
    });
  }
}
