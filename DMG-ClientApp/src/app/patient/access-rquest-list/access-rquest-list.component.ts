import { Component, OnInit } from "@angular/core";
import { PatientDataService } from "../shared/patient-data.service";
import { IUserReq, IPatient } from "../shared/patient";

@Component({
  selector: "app-access-rquest-list",
  templateUrl: "./access-rquest-list.component.html",
  styleUrls: ["./access-rquest-list.component.scss"]
})
export class AccessRquestListComponent implements OnInit {
  requests: IUserReq;
  constructor(private data: PatientDataService) {}

  ngOnInit() {
    const id = localStorage.getItem("userId");
    this.data.getAccessRequestList(id).subscribe(res => {
      this.requests = res as IUserReq;
    });
  }

  unauthorize(id: number, doctorId: string) {
    this.requests.requests[id].authorization = false;
    this.data.addAccessAuthorisation(this.requests);
    this.data
      .unApproveDoctor(doctorId, localStorage.getItem("userId"))
      .subscribe(res => {
        console.log(res);
      });
  }

  authorize(id: number, doctorId, firstName, lastName) {
    const userId = localStorage.getItem("userId");
    this.requests.requests[id].authorization = true;
    this.data.addAccessAuthorisation(this.requests);

    // Update doctor profile

    const body = {
      patientId: userId,
      status: true
    };
    // tslint:disable-next-line:no-shadowed-variable
    this.data.approveDoctor(doctorId, body).subscribe(res => {
      console.log(res);
    });

    // add doctor to own profile
    this.data
      .addDoctorToUserProfile(userId, doctorId, firstName, lastName)
      .subscribe(res => {
        console.log("Authorised");
        this.openSnackBar();
      });

    console.log(this.requests);
  }

  openSnackBar() {}
}
