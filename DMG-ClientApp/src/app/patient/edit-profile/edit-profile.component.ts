import {
  Component,
  OnInit,
  ElementRef,
  ViewChildren,
  AfterViewInit,
  OnDestroy
} from "@angular/core";
import {
  FormGroup,
  Validators,
  AbstractControl,
  FormBuilder,
  FormArray,
  FormControlName
} from "@angular/forms";

import "rxjs/add/operator/debounceTime";
import { PatientDataService } from "../shared/patient-data.service";
import { Router } from "@angular/router";
import { IPatient } from "../shared/patient";
import { ActivatedRoute } from "@angular/router";
import { startWith, map } from "rxjs/operators";
import { Observable } from "rxjs/Observable";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.component.html",
  styleUrls: ["./edit-profile.component.scss"]
})
export class EditProfileComponent implements OnInit {

  firstFormGroup;
  AddressFormGroup;
  secondFormGroup;
  thirdFormGroup;
  isLinear=true;


  filteredOptions: Observable<string[]>;
  genderFilteredOptions: Observable<string[]>;
  customerForm: FormGroup;
  id: any;

  errorMessage: any;
  userid: any;
  patient: IPatient;

  // customer: Customer = new Customer();
  emailMessage: string;
  Gender = ["Male", "Female"];
  options = ["Mr", "Mrs", "Miss", "Dr", "Ms"];
  genderOptions = ["Heterosexual", "Homosexual", "Lesbian", "Bisexual"];
  get addresses(): FormArray {
    return <FormArray>this.customerForm.get("addresses");
  }
  get phoneNumbers(): FormArray {
    return <FormArray>this.customerForm.get("phoneNumbers");
  }
  get alergys(): FormArray {
    return <FormArray>this.customerForm.get("alergys");
  }

  constructor(
    private fb: FormBuilder,
    private http: PatientDataService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    // this.userid = localStorage.getItem('userId');
  }

  ngOnInit(): void {
    this.customerForm = this.fb.group({
      title: ["", [Validators.required]],
      firstName: ["", [Validators.required, Validators.minLength(3)]],
      lastName: ["", [Validators.required, Validators.minLength(3)]],
      gender: ["", Validators.required],
      dob: ["", [Validators.required]],
      email: ["", [Validators.email]],
      occupation: ["", [Validators.required]],
      sexualOrientation: ["", Validators.required],
      alcohol: "",
      phoneNumbers: this.fb.array([this.buildPhoneNumber()]),
      notification: "email",
      sendCatalog: true,
      addresses: this.fb.array([this.buildAddress()]),
      alergys: this.fb.array([this.buildAlergy()]),
      livingStatus: ["", Validators.required],
      socialHistory: ""
    });
    this.filteredOptions = this.customerForm
      .get("title")
      .valueChanges.pipe(startWith(""), map(val => this.filter(val)));
    this.genderFilteredOptions = this.customerForm
      .get("sexualOrientation")
      .valueChanges.pipe(startWith(""), map(val => this.filterGender(val)));

    this.route.paramMap
      .switchMap(m => this.http.getMedicalProfileById(m.get("id")))
      .subscribe(res => {
        console.log(res);
        const d = new Date(res.dob);
        const setDate = d.toISOString().substring(0, 10);

        this.customerForm.patchValue({
          title: res.title,
          firstName: res.firstName,
          lastName: res.lastName,
          gender: res.gender,
          dob: setDate,
          occupation: res.occupation,
          sexualOrientation: res.sexualOrientation,
          email: res.email,
          alcohol: res.alcohol,
          livingStatus: res.livingStatus,
          socialHistory: res.socialHistory
        });

        this.populateAddresses(res);
        this.populateAlergys(res);
        this.populatePhoneNumber(res);
      });
  }
  private populatePhoneNumber(res: IPatient) {
    for (let index = 0; index < res.phoneNumbers.length; index++) {
      this.phoneNumbers.controls.push(this.buildPhoneNumber());
      this.phoneNumbers.at(index).patchValue({
        type: res.phoneNumbers[index].type,
        number: res.phoneNumbers[index].number
      });
      this.phoneNumbers.controls.pop();
    }
  }

  private populateAlergys(res: IPatient) {
    for (let index = 0; index < res.alergys.length; index++) {
      this.alergys.controls.push(this.buildAlergy());
      this.alergys.at(index).patchValue({
        type: res.alergys[index].type,
        description: res.alergys[index].description
      });
      this.alergys.controls.pop();
    }
  }

  private populateAddresses(res: IPatient) {
    for (let index = 0; index < res.addresses.length; index++) {
      this.addresses.controls.push(this.buildAddress());
      this.addresses.at(index).patchValue({
        addressType: res.addresses[index].postal,
        street1: res.addresses[index].street1,
        street2: res.addresses[index].street2,
        city: res.addresses[index].city,
        country: res.addresses[index].country,
        postal: res.addresses[index].postal
      });
      this.addresses.controls.pop();
    }
  }

  filter(val: string): string[] {
    return this.options.filter(
      option => option.toLowerCase().indexOf(val.toLowerCase()) === 0
    );
  }
  filterGender(val: string): string[] {
    return this.genderOptions.filter(
      option => option.toLowerCase().indexOf(val.toLowerCase()) === 0
    );
  }

  addAddress(): void {
    this.addresses.push(this.buildAddress());
  }
  addPhoneNumber(): void {
    this.phoneNumbers.push(this.buildPhoneNumber());
  }
  addAlergy(): void {
    this.alergys.push(this.buildAlergy());
  }
  buildAddress(): FormGroup {
    return this.fb.group({
      addressType: "home",
      street1: "",
      street2: "",
      state: "",
      postal: "",
      city: "",
      country: ""
    });
  }
  buildPhoneNumber(): FormGroup {
    return this.fb.group({
      type: ["", Validators.required],
      number: ["", Validators.required]
    });
  }
  buildAlergy(): FormGroup {
    return this.fb.group({
      type: "",
      description: ""
    });
  }

  save(): void {
    console.log("clicked");
    const id = this.route.snapshot.paramMap.get("id");

    const p: IPatient = Object.assign({}, this.customerForm.value);
    p.userId = id;
    this.http.updateProfile(id, p).subscribe(res => {
      this.router.navigate(["/patient/details"]);
      this.snackBar.open("Profile updated", "Successfully", {
        duration: 5000
      });
    });
  }
}
