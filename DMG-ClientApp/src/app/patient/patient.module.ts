import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AddProfileComponent } from "./add-profile/add-profile.component";
import { EditProfileComponent } from "./edit-profile/edit-profile.component";
import { ProfileDetailsComponent } from "./profile-details/profile-details.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { AuthHttp, AuthConfig, AUTH_PROVIDERS } from "angular2-jwt";
import { MaterialModule } from "../design/material.module";
import { UiLoadingModule } from "../ui/ui-loading.module";
import { PatientAuthGuard } from "../auth/patient-auth.guard";
import { PatientDataService } from "./shared/patient-data.service";
import { HttpClientModule } from "@angular/common/http";
import { AccessRquestListComponent } from "./access-rquest-list/access-rquest-list.component";
import { AppointmentsComponent } from "./appointments/appointments.component";
import { FileUploadComponent } from "./file-upload/file-upload.component";

// export function authHttpServiceFactory(http: Http, options: RequestOptions) {
//   return new AuthHttp(
//     new AuthConfig({
//       // tokenName: 'access_token',
//       tokenGetter: (): any => localStorage.getItem('access_token'),
//       globalHeaders: [{ 'Content-Type': 'application/json' }]
//     }),
//     http,
//     options
//   );
// }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    UiLoadingModule,
    RouterModule.forChild([
      {
        path: "",
        canActivate: [PatientAuthGuard],
        canActivateChild: [PatientAuthGuard],
        children: [
          { path: "details", component: ProfileDetailsComponent },
          { path: "new", component: AddProfileComponent },
          { path: "accessRequest", component: AccessRquestListComponent },
          { path: "appointments", component: AppointmentsComponent },
          { path: ":id/edit", component: EditProfileComponent }
        ]
      }
    ])
  ],
  providers: [PatientDataService],
  declarations: [
    AddProfileComponent,
    EditProfileComponent,
    ProfileDetailsComponent,
    AccessRquestListComponent,
    AppointmentsComponent,
    FileUploadComponent
  ],
  entryComponents:[FileUploadComponent]
})
export class PatientModule {}
