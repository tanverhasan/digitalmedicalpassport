import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { AuthHttp } from "angular2-jwt";
import { IPatient } from "./patient";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { AuthService } from "../../auth/auth.service";
import { AngularFirestore } from "angularfire2/firestore";
import { IUserReq } from "../../doctor/shared/doctor";
import { ErrorObservable } from "rxjs/observable/ErrorObservable";
import { map, catchError, retry } from "rxjs/operators";
import { environment } from "../../../environments/environment";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Subject } from "rxjs/Subject";

@Injectable()
export class PatientDataService {
  private httpStatusCode = new BehaviorSubject<any>(null);
  statusCode = this.httpStatusCode.asObservable();
  userId = localStorage.getItem("userId");
  // url = "http://localhost:5001/api/patient/";
  url = `${environment.api.baseUrl}patient`;
  constructor(private httpClient: HttpClient, private afs: AngularFirestore) {}

  public getMedicalProfile() {
    return this.httpClient.get(this.url).map(res => res as IPatient[]);
  }

  public getMedicalProfileById(id: string) {
    return this.httpClient
      .get(`${environment.api.baseUrl}Patient/${id}`)
      .pipe(
        retry(3),
        map(res => res as IPatient),
        catchError(c => this.handleError(c))
      );
    // .map(res => res as IPatient)
    // .catch(c => this.handleError(c));
  }

  public addMedicalProfile(body: IPatient) {
    return this.httpClient
      .post(this.url, body)
      .pipe(retry(3), catchError(c => this.handleError(c)));
  }

  updateProfile(id, data: IPatient) {
    return this.httpClient
      .put(`${environment.api.baseUrl}Patient/${id}`, data)
      .pipe(retry(3), catchError(c => this.handleError(c)));
  }
  updateImageUri(id, url) {
    const body = {
      ImageUri: url
    };
    return this.httpClient
      .put(`${environment.api.baseUrl}Patient/image/${id}`, body)
      .pipe(catchError(c => this.handleError(c)));
  }
  public deleteMedicalProfile(id: string) {
    return this.httpClient
      .delete(this.url)
      .pipe(retry(3), catchError(c => this.handleError(c)));
  }
  getAuthorisationList() {
    const ref = this.afs.doc(`users/${this.userId}`);

    return ref.valueChanges();
  }
  addAccessAuthorisation(data: IUserReq) {
    return this.afs.doc(`users/${this.userId}`).update(data);
  }

  getProfileFromFirstore(id: string) {
    return this.afs
      .collection(`users`)
      .doc(id)
      .valueChanges();
  }

  getAccessRequestList(id) {
    return this.afs
      .collection(`users`)
      .doc(id)
      .valueChanges();
  }

  addDoctorToUserProfile(userId, doctorId, doctorFirstName, doctorLastName) {
    const body = {
      userId: userId,
      doctorId: doctorId,
      firstName: doctorFirstName,
      lastName: doctorLastName,
      status: true
    };
    console.log(body);
    return this.httpClient
      .post(`${environment.api.baseUrl}PatientDoctor`, body)
      .pipe(retry(3), catchError(c => this.handleError(c)));
    // }
  }
  approveDoctor(id, data) {
    return this.httpClient
      .put(`${environment.api.baseUrl}DoctorPatients/${id}`, data)
      .pipe(retry(3), catchError(c => this.handleError(c)));
  }
  unApproveDoctor(id, patientId) {
    const body = {
      patientId: patientId,
      status: false
    };
    return this.httpClient
      .put(`${environment.api.baseUrl}DoctorPatients/${id}`, body)
      .pipe(retry(3), catchError(c => this.handleError(c)));
  }

  getAppointments(id) {
    return this.afs
      .collection(`users`)
      .doc(id)
      .collection(`appointments`, ref => ref.orderBy("bookingDate"))
      .valueChanges();
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error("An error occurred:", error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
      console.log(error.status);
      // this.setHttpStatusCode(error.status);
      //  this.setHttpStatusCode(error.status);
      this.httpStatusCode.next(error.status);
    }
    return new ErrorObservable(
      "Something bad happened; please try again later."
    );
  }
}

// , {
//   headers: new HttpHeaders()
//     .set('Authorization', 'Bearer ' + this.auth.getAccessTocken())
//     .set('Content-Type', 'application/json')
// }
