import { Component, OnInit } from "@angular/core";
import { PatientDataService } from "../shared/patient-data.service";
import { IPatient } from "../shared/patient";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material";
import { FileUploadComponent } from "../file-upload/file-upload.component";

@Component({
  selector: "app-profile-details",
  templateUrl: "./profile-details.component.html",
  styleUrls: ["./profile-details.component.scss"]
})
export class ProfileDetailsComponent implements OnInit {
  age: number;
  loader = true;
  profile: IPatient;
  url;
  userId: any = localStorage.getItem("userId");
  constructor(
    private http: PatientDataService,
    private route: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.http.getMedicalProfileById(this.userId).subscribe(res => {
      console.log(res);
      this.profile = res as IPatient;
      this.loader = false;
      console.log(this.profile);
    });
    this.http.statusCode.subscribe(res => {
      console.log(res);
      if (res === 404) {
        this.loader = false;
      }
    });
  }

  public addPatientProfile() {
    this.route.navigate(["/patient/new"]);
  }
  imageUpload() {
    const dialogRef = this.dialog.open(FileUploadComponent);
    dialogRef.afterClosed().subscribe(res => {
      this.profile.imageUri = res;
    });
  }
  editPatientProfile(id: string) {
    this.route.navigate(["/patient/" + id + "/edit"]);
  }
  getAge(dateString) {
    const today = new Date();
    const birthDate = new Date(dateString);
    this.age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      this.age--;
    }
    return this.age;
  }
}
