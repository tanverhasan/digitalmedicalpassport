import { Component, OnInit, Inject } from "@angular/core";
import {
  AngularFireStorage,
  AngularFireUploadTask
} from "angularfire2/storage";
import { Observable } from "rxjs/Observable";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from "@angular/material";
import { map } from "rxjs/operators";
import { PatientDataService } from "../shared/patient-data.service";

@Component({
  selector: "app-file-upload",
  templateUrl: "./file-upload.component.html",
  styleUrls: ["./file-upload.component.scss"]
})
export class FileUploadComponent implements OnInit {
  uploadState: Observable<{}>;
  downloadURL: Observable<string>;
  downloadUrl: Observable<string>;
  uploadProgress: Observable<number>;
  task: AngularFireUploadTask;
  constructor(
    private storage: AngularFireStorage,
    private service: PatientDataService,
    private dialogRef: MatDialogRef<FileUploadComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {}

  ngOnInit() {}

  upload(event) {
    const file = event.target.files[0];
    const fileName = event.target.files[0].name;
    const id = Math.random()
      .toString(36)
      .substring(2);

    const filePath = "uploads/" + id;
    this.task = this.storage.upload(filePath, file);
    this.uploadProgress = this.task.percentageChanges();
    this.downloadURL = this.task.downloadURL();
    this.uploadState = this.task.snapshotChanges().pipe(map(s => s.state));
    this.downloadURL.subscribe(res => {
      if (res != null) {
        const dat = {
          url: res,
          fileName: fileName,
          fileId: id,
          timestamp: new Date().toLocaleString()
        };
        this.service
          .updateImageUri(localStorage.getItem("userId"), res)
          .subscribe(() => {
            this.dialogRef.close(res);
          });
        console.log(dat);
        console.log(this.data);
      }
    });
  }
}
