import { Component, OnInit } from "@angular/core";
import { PatientDataService } from "../shared/patient-data.service";
import { Observable } from "rxjs/Observable";

@Component({
  selector: "app-appointments",
  templateUrl: "./appointments.component.html",
  styleUrls: ["./appointments.component.scss"]
})
export class AppointmentsComponent implements OnInit {
  appointments: Observable<{}[]>;
  constructor(private data: PatientDataService) {}

  ngOnInit() {
   this.appointments = this.data.getAppointments(localStorage.getItem("userId"));
  }
}
