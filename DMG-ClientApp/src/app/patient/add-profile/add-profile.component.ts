import {
  Component,
  OnInit,
  ElementRef,
  ViewChildren,
  AfterViewInit,
  OnDestroy
} from "@angular/core";
import {
  FormGroup,
  Validators,
  AbstractControl,
  FormBuilder,
  FormArray,
  FormControlName
} from "@angular/forms";

import "rxjs/add/operator/debounceTime";
import { PatientDataService } from "../shared/patient-data.service";
import { Router } from "@angular/router";
import { IPatient } from "../shared/patient";
import { startWith, map } from "rxjs/operators";
import { Observable } from "rxjs/Observable";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-add-profile",
  templateUrl: "./add-profile.component.html",
  styleUrls: ["./add-profile.component.scss"]
})
export class AddProfileComponent implements OnInit, AfterViewInit, OnDestroy {
  firstFormGroup;
  AddressFormGroup;
  secondFormGroup;
  thirdFormGroup;
  isLinear = true;
  genderFilteredOptions: Observable<string[]>;
  filteredOptions: Observable<string[]>;
  errorMessage: any;
  userid: any;
  patient: IPatient;
  customerForm: FormGroup;
  // customer: Customer = new Customer();
  emailMessage: string;
  Gender = ["Male", "Female"];
  options = ["Mr", "Mrs", "Miss", "Dr", "Ms"];
  genderOptions = ["Heterosexual", "Homosexual", "Lesbian", "Bisexual"];

  get addresses(): FormArray {
    return <FormArray>this.customerForm.get("addresses");
  }
  get phoneNumbers(): FormArray {
    return <FormArray>this.customerForm.get("phoneNumbers");
  }
  get alergys(): FormArray {
    return <FormArray>this.customerForm.get("alergys");
  }

  constructor(
    private fb: FormBuilder,
    private http: PatientDataService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.userid = localStorage.getItem("userId");
  }

  ngOnInit(): void {
    this.customerForm = this.fb.group({
      title: ["", [Validators.required]],
      firstName: ["", [Validators.required, Validators.minLength(3)]],
      lastName: ["", [Validators.required, Validators.minLength(3)]],
      gender: ["", Validators.required],
      dob: ["", [Validators.required]],
      email: ["", [Validators.email]],
      occupation: ["", [Validators.required]],
      sexualOrientation: ["", Validators.required],
      alcohol: "",
      phoneNumbers: this.fb.array([this.buildPhoneNumber()]),
      notification: "email",
      sendCatalog: true,
      addresses: this.fb.array([this.buildAddress()]),
      alergys: this.fb.array([this.buildAlergy()]),
      livingStatus: ["", Validators.required],
      socialHistory: ""
    });
    this.customerForm.patchValue({
      email: localStorage.getItem("email")
    });
    this.filteredOptions = this.customerForm
      .get("title")
      .valueChanges.pipe(startWith(""), map(val => this.filter(val)));
    this.genderFilteredOptions = this.customerForm
      .get("sexualOrientation")
      .valueChanges.pipe(startWith(""), map(val => this.filterGender(val)));
  }
  filter(val: string): string[] {
    return this.options.filter(
      option => option.toLowerCase().indexOf(val.toLowerCase()) === 0
    );
  }
  filterGender(val: string): string[] {
    return this.genderOptions.filter(
      option => option.toLowerCase().indexOf(val.toLowerCase()) === 0
    );
  }
  ngOnDestroy(): void {
    //  this.http.unsubscribe();
  }
  ngAfterViewInit(): void {}

  addAddress(): void {
    this.addresses.push(this.buildAddress());
  }
  addPhoneNumber(): void {
    this.phoneNumbers.push(this.buildPhoneNumber());
  }
  addAlergy(): void {
    this.alergys.push(this.buildAlergy());
  }
  buildAddress(): FormGroup {
    return this.fb.group({
      addressType: "home",
      street1: "",
      street2: "",
      city: "",
      state: "",
      postal: "",
      country: ""
    });
  }
  removeAddress(index) {
    this.addresses.controls.splice(index);
  }
  buildPhoneNumber(): FormGroup {
    return this.fb.group({
      type: ["", Validators.required],
      number: ["", Validators.required]
    });
  }
  removePhoneNumber(index) {
    this.phoneNumbers.controls.splice(index);
  }
  buildAlergy(): FormGroup {
    return this.fb.group({
      type: "",
      description: ""
    });
  }
  removeAlergy(index) {
    this.alergys.controls.splice(index);
  }

  save(): void {
    // console.log(this.customerForm.value);
    const data: IPatient = Object.assign({}, this.customerForm.value);
    data.userId = localStorage.getItem("userId");
    console.log(data);
    this.http.addMedicalProfile(data).subscribe(res => {
      console.log(res);
      this.router.navigate(["/patient/details"]);
      this.snackBar.open("Profile Added", "Successfully", {
        duration: 500
      });
    });
  }
}
