import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { ServiceWorkerModule } from "@angular/service-worker";
import { AppComponent } from "./app.component";

import { environment } from "../environments/environment";
import { MaterialModule } from "./design/material.module";
import { AuthModule } from "./auth/auth.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HomeComponent } from "./home/home.component";
import { AppRoutingModule } from "./route/route.module";
import { UiLoadingModule } from "./ui/ui-loading.module";
// import { PatientModule } from './patient/patient.module';
import { HttpModule } from "@angular/http";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TockenInterceptor } from "./service/TockenInterceptor";
import { AngularFireModule } from "angularfire2";
import { AngularFireStorageModule } from "angularfire2/storage";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { PatientModule } from "./patient/patient.module";
import { DoctorModule } from "./doctor/doctor.module";
import { ToastModule } from "ng2-toastr";
import { DoctorsListComponent } from "./home/doctors-list/doctors-list.component";
import { AngularFireAuthModule } from "angularfire2/auth";
import { MessagingService } from "./service/messaging.service";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { CoreModule } from "./core/core.module";
import { FooterComponent } from "./footer/footer.component";
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DoctorsListComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    AuthModule.forRoot(),
    ToastModule.forRoot(),
    MaterialModule,
    UiLoadingModule,
    // PatientModule,
    CoreModule,
    Ng2SmartTableModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFirestoreModule.enablePersistence(),
    ServiceWorkerModule.register("/ngsw-worker.js", {
      enabled: environment.production
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TockenInterceptor,
      multi: true
    },
    MessagingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
