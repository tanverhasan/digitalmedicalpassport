import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { InterviewComponent } from "./interview/interview.component";
import { RouterModule } from "@angular/router";
import { SkypeApiService } from "./service/skype-api.service";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: "",
        children: [{ path: "video", component: InterviewComponent }]
      }
    ])
  ],
  declarations: [InterviewComponent],
  providers: [SkypeApiService]
})
export class SkypeModule {}
