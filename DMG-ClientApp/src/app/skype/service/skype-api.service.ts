import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as Guid from "guid";
import * as sha256 from "sha256";
import * as jwt from "jsonwebtoken";
import { environment } from "../../../environments/environment";
import { retry } from "rxjs/operators";

@Injectable()
export class SkypeApiService {
  constructor(private httpClient: HttpClient) {}

  getToken() {
    const payload = {};
    const body = JSON.stringify(payload);
    return this.httpClient
      .post(
        "https://us-central1-digitalmedicalpassport.cloudfunctions.net/skypeSignedToken",
        body,
        { responseType: "text" }
      )
      .pipe(retry(3));
  }

  getInterViewUlr(token) {
    console.log(token);
    const payload = {};
    const body = JSON.stringify(payload);
    return this.httpClient.post(
      "https://interviews.skype.com/api/interviews",
      body,
      {
        headers: new HttpHeaders()
          .set("Content-Type", "application/json")
          .set("Authorization", "Bearer " + token)
      }
    );
  }
}
