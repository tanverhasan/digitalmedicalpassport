import { Component, OnInit } from "@angular/core";
import { SkypeApiService } from "../service/skype-api.service";

@Component({
  selector: "app-interview",
  templateUrl: "./interview.component.html",
  styleUrls: ["./interview.component.scss"]
})
export class InterviewComponent implements OnInit {
  token: Object;
  constructor(private data: SkypeApiService) {}

  ngOnInit() {
    this.data.getToken().subscribe(res => {
      console.log(res);
      this.token = res;
    });
  }

  requestUrl() {
    this.data.getInterViewUlr(this.token).subscribe(res => {
      console.log(res);
    });
  }
}
