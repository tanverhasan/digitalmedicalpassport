interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
  apiUrl: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: "BVsIkCR4T2fURbGPMwjGQbQ024BtkkhF",
  domain: "tanver.eu.auth0.com",
  callbackURL: "http://localhost:5000",
  apiUrl: "https://api.digitalmedicalpassport.com"
};
