import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProfileComponent } from "./profile/profile.component";
import { AuthAdminGuard } from "./auth-admin.guard";
import { AuthDoctorGuard } from "./auth-doctor.guard";
import { PatientAuthGuard } from "./patient-auth.guard";
import { AuthService } from "./auth.service";
import { AuthGuard } from "./auth.guard";
import { AuthDoctorLoadGuard } from "./auth-doctor-load.guard";
import { AuthPatientLoadGuard } from "./auth-patient-load.guard";
import { AuthAdminLoadGuard } from "./auth-admin-load.guard";

@NgModule({
  imports: [CommonModule],
  declarations: [ProfileComponent]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        AuthService,
        AuthGuard,
        AuthAdminGuard,
        AuthDoctorGuard,
        PatientAuthGuard,
        AuthDoctorLoadGuard,
        AuthPatientLoadGuard,
        AuthAdminLoadGuard
      ]
    };
  }
}
