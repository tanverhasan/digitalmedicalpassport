import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Route,
  CanLoad
} from "@angular/router";
import { Observable } from "rxjs/Observable";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthPatientLoadGuard implements CanLoad {
  constructor(private auth: AuthService) {}
  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    if (!this.auth.isAuthenticated() && !this.auth.isInRole("user")) {
      return false;
    }
    return true;
  }
}
