import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanLoad,
  Route,
  Router
} from "@angular/router";
import { Observable } from "rxjs/Observable";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthDoctorLoadGuard implements CanLoad {
  constructor(private auth: AuthService) {}
  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    if (!this.auth.isAuthenticated() && !this.auth.isInRole("doctor")) {
      return false;
    }
    return true;
  }
}
