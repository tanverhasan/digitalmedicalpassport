import { Injectable } from "@angular/core";
import { AUTH_CONFIG } from "./auth0-variables";
import { Router } from "@angular/router";
import "rxjs/add/operator/filter";
import * as auth0 from "auth0-js";
import { JwtHelper } from "angular2-jwt";
import { AngularFirestore } from "angularfire2/firestore";
import { User } from "../service/user";
import { environment } from "./../../environments/environment";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/of";
import "rxjs/add/observable/timer";
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MessagingService } from "../service/messaging.service";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
// import * as admin from "firebase-admin";

@Injectable()
export class AuthService {
  private userEmail = new BehaviorSubject("");
  userEmailMessage = this.userEmail.asObservable();
  // tslint:disable-next-line:no-inferrable-types
  // requestedScopes: string = "openid email profile read:messages write:messages";
  refreshSubscription: any;
  auth0 = new auth0.WebAuth({
    clientID: environment.auth.clientId,
    domain: environment.auth.domain,
    responseType: "token id_token",
    audience: AUTH_CONFIG.apiUrl,
    redirectUri: environment.auth.redirect,
    scope: environment.auth.scope
  });
  // auth0 = new auth0.WebAuth({
  //   clientID: AUTH_CONFIG.clientID,
  //   domain: AUTH_CONFIG.domain,
  //   responseType: "token id_token",
  //   audience: AUTH_CONFIG.apiUrl,
  //   redirectUri: AUTH_CONFIG.callbackURL,
  //   scope: this.requestedScopes
  // });

  userProfile: any;
  // private roles: any=[];

  constructor(
    public router: Router,
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private httpClient: HttpClient,
    private msg: MessagingService
  ) {}

  public login(): void {
    this.auth0.authorize();
  }

  public handleAuthentication(): void {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        console.log(authResult);
        window.location.hash = "";
        this.setSession(authResult);
        this.router.navigate(["/home"]);
      } else if (err) {
        this.router.navigate(["/home"]);
        console.log(err);
        alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  public getProfile(cb: any): void {
    const accessToken = localStorage.getItem("access_token");
    if (!accessToken) {
      throw new Error("Access token must exist to fetch profile");
    }

    const self = this;
    this.auth0.client.userInfo(accessToken, (err, profile) => {
      if (profile) {
        self.userProfile = profile;
      }
      cb(err, profile);
    });
  }

  private setSession(authResult: any): void {
    // set the time that the access token will expire at
    const expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );
    const scopes = authResult.scope || environment.auth.scope || "";

    const jwthelper = new JwtHelper();
    const decodedToken = jwthelper.decodeToken(authResult.idToken);
    console.log("decodeToken" + decodedToken);
    // const roles = decodedToken["http://localhost:5000/roles"];
    const roleUrl = `${environment.auth.roleUrl}/roles`;
    console.log(roleUrl);
    const roles = decodedToken[roleUrl];

    localStorage.setItem("roles", roles);
    console.log(roles);

    localStorage.setItem("access_token", authResult.accessToken);
    localStorage.setItem("id_token", authResult.idToken);
    const email = authResult.idTokenPayload.email;
    localStorage.setItem("email", email);
    this.setUserEmail(email);
    const userid: string = authResult.idTokenPayload.sub;
    const sliceId = userid.slice(6, 30);
    localStorage.setItem("userId", sliceId);
    // const userid: string = authResult.idTokenPayload.sub;
    localStorage.setItem("expires_at", expiresAt);
    localStorage.setItem("scopes", JSON.stringify(scopes));
    this.RequestCustomTockenInFirebase(sliceId);
    // authenticating in firebase using custom jwt issued from auth0
    // this.SignInWithCustomTockenInFirebase(authResult.idToken);
    this.updateUserData(sliceId, email, roles);
    this.scheduleRenewal();
  }
  setUserEmail(email: string) {
    this.userEmail.next(email);
  }
  RequestCustomTockenInFirebase(uid: string) {
    console.log("start" + uid);
    const body = {
      id: uid
    };
    this.httpClient
      .post(
        "https://us-central1-digitalmedicalpassport.cloudfunctions.net/customToken",
        body,
        { responseType: "text" }
      )
      .subscribe(res => {
        console.log("Custom Tocken: " + res);
        this.SignInWithCustomTockenInFirebase(res);
      });
  }
  SignInWithCustomTockenInFirebase(token: string) {
    console.log(token);
    this.afAuth.auth
      .signInWithCustomToken(token)
      .then(credential => {
        console.log("Custom Token Login response: " + credential);
        // this.msg.getPermission(localStorage.getItem("userId"));
      })
      .catch(err => {
        console.log(err);
      });
  }

  private updateUserData(uid: string, email: string, roles: string) {
    const userId = localStorage.getItem("userId");
    const check = userId.substring(0, 6);
    if (check === "auth0|") {
      const slice = uid.slice(6, 30);
      uid = slice;
      localStorage.setItem("userId", slice);
    }
    const data = {
      id: uid,
      email: email,
      roles: roles
    };
    return this.afs
      .collection("users")
      .doc(data.id)
      .set(data, { merge: true });
  }

  public isInRole(roleName: string) {
    const roles: any = localStorage.getItem("roles");
    if (roles != null) {
      return roles.indexOf(roleName) > -1;
    }
  }
  public logout(): void {
    // remove tokens and expiry time from localStorage
    localStorage.removeItem("access_token");
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    localStorage.removeItem("scopes");
    localStorage.removeItem("roles");
    // this.roles = [];
    this.unscheduleRenewal();
    // go back to the home route
    this.router.navigate(["/"]);
  }

  getAccessTocken() {
    // return localStorage.getItem("access_token");
    const idToken = localStorage.getItem("access_token");
    return idToken;
  }
  public renewToken() {
    this.auth0.renewAuth(
      {
        audience: AUTH_CONFIG.apiUrl,
        redirectUri: `${environment.auth.redirect}silent`,
        usePostMessage: true
      },
      (err, result) => {
        if (err) {
          alert(
            `Could not get a new token using silent authentication (${
              err.error
            }).`
          );
        } else {
          alert(`Successfully renewed auth!`);
          this.setSession(result);
        }
      }
    );
  }

  public scheduleRenewal() {
    // tslint:disable-next-line:curly
    if (!this.isAuthenticated()) return;
    this.unscheduleRenewal();

    const expiresAt: any = window.localStorage.getItem("expires_at");

    // tslint:disable-next-line:no-shadowed-variable
    const source = Observable.of(expiresAt).flatMap(expiresAt => {
      const now = Date.now();

      // use the delay in a timer to
      // run the refresh at the proper time
      return Observable.timer(Math.max(1, expiresAt - Date.now()));
    });

    // once the delay time from above is
    // reached, get a new JWT and schedule
    // additional refreshes
    this.refreshSubscription = source.subscribe(() => {
      this.renewToken();
      this.scheduleRenewal();
    });
  }

  public unscheduleRenewal() {
    // tslint:disable-next-line:curly
    if (!this.refreshSubscription) return;
    this.refreshSubscription.unsubscribe();
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt: any = localStorage.getItem("expires_at");
    return new Date().getTime() < expiresAt;
  }
  public userHasScopes(scopes: Array<string>): boolean {
    const grantedScopes: any = localStorage.getItem("scopes");
    console.log("granted scopes", grantedScopes);
    const splitedGrantedScopes = grantedScopes.split(" ");
    return scopes.every(scope => splitedGrantedScopes.includes(scope));
  }

  get tokenValid(): boolean {
    // Check if current time is past access token's expiration
    const expiresAt = JSON.parse(localStorage.getItem("expires_at"));
    const tokenValid = Date.now() < expiresAt;
    return Date.now() < expiresAt;
  }

  // public userHasScopes(scopes: Array<string>): boolean {
  //    const grantedScopes: any = localStorage.getItem('scopes').split(' ');
  //    return scopes.every(scope => grantedScopes.includes(scope));
  // }
}
