import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { retry, catchError, map } from "rxjs/operators";
import { ErrorObservable } from "rxjs/observable/ErrorObservable";
import { environment } from "../../../environments/environment";

@Injectable()
export class AdmindataService {
  constructor(private httpClient: HttpClient) {}

  GetDoctorBySearch(searchTerm: string) {
    // console.log(searchTerm);
    const term = searchTerm.trim();
    const options = term
      ? { params: new HttpParams().set("SearchQuery", term) }
      : {};
    return this.httpClient
      .get(
        `${
          environment.api.baseUrl
        }Doctor?PageNumber=1&PageSize=20&SearchQuery=${searchTerm}`
      )
      .pipe(retry(3), map(res => res), catchError(this.handleError));
  }

  GetPatientBySearch(searchTerm: string) {
    // console.log(searchTerm);
    const term = searchTerm.trim();
    const options = term
      ? { params: new HttpParams().set("SearchQuery", term) }
      : {};
    return this.httpClient
      .get(
        `${
          environment.api.baseUrl
        }Patient?PageNumber=1&PageSize=20&SearchQuery=${searchTerm}`
      )
      .pipe(retry(3), map(res => res), catchError(this.handleError));
  }

  GetGroupByGender() {
    return this.httpClient
      .get(`${environment.api.baseUrl}Admin/Patient/GroupByGender`)
      .pipe(catchError(this.handleError));
  }

  GetGroupBySexualOrientation() {
    return this.httpClient
      .get(`${environment.api.baseUrl}Admin/Patient/GroupBySexualOrientation`)
      .pipe(catchError(this.handleError));
  }

  GetGroupByCity(country) {
    return this.httpClient
      .get(`${environment.api.baseUrl}Admin/Patient/GroupByCity/${country}`)
      .pipe(catchError(this.handleError));
  }

  doctorGetGroupByGender() {
    return this.httpClient
      .get(`${environment.api.baseUrl}Admin/Doctor/GroupByGender`)
      .pipe(catchError(this.handleError));
  }

  doctorGetGroupBySexualOrientation() {
    return this.httpClient
      .get(`${environment.api.baseUrl}Admin/Doctor/GroupBySexualOrientation`)
      .pipe(catchError(this.handleError));
  }

  doctorGetGroupByCity(country) {
    return this.httpClient
      .get(`${environment.api.baseUrl}Admin/Doctor/GroupByCity/${country}`)
      .pipe(catchError(this.handleError));
  }

  getDoctorCount() {
    return this.httpClient
      .get(`${environment.api.baseUrl}Admin/Doctor/Count`)
      .pipe(catchError(this.handleError));
  }
  getPatientCount() {
    return this.httpClient
      .get(`${environment.api.baseUrl}Admin/Patient/Count`)
      .pipe(catchError(this.handleError));
  }

  getPatientRegistrationMonthlyView(year: number) {
    return this.httpClient
      .get(
        `${
          environment.api.baseUrl
        }Admin/Patient/RegistrationMonthlyView/${year}`
      )
      .pipe(catchError(this.handleError));
  }
  getDoctorRegistrationMonthlyView(year: number) {
    return this.httpClient
      .get(
        `${environment.api.baseUrl}Admin/Doctor/RegistrationMonthlyView/${year}`
      )
      .pipe(catchError(this.handleError));
  }
  deleteDoctor(id) {
  return  this.httpClient
      .delete(`${environment.api.baseUrl}Doctor/${id}`)

      .pipe(retry(3), catchError(c => this.handleError(c)));
  }

  deletePatient(id) {
  return  this.httpClient
      .delete(`${environment.api.baseUrl}Patient/${id}`)
      .pipe(retry(3), catchError(c => this.handleError(c)));
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error("An error occurred:", error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return new ErrorObservable(
      "Something bad happened; please try again later."
    );
  }
}
