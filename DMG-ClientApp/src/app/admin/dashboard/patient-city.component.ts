import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { AdmindataService } from "../shared/admindata.service";
declare var Plotly: any;
@Component({
  selector: "app-patient-city",
  template: `
  <app-loading-dot *ngIf="loader"></app-loading-dot>
    <div #chart></div>
  `,
  styles: []
})
export class PatientCityComponent implements OnInit {
  labels: string[] = [];
  values: number[] = [];
  loader = true;
  @ViewChild("chart") el: ElementRef;
  country = "Bangladesh";
  constructor(private data: AdmindataService) {}

  ngOnInit() {
    const divElement = this.el.nativeElement;
    this.data.GetGroupByCity(this.country).subscribe(res => {
      // console.log(res);
      res.forEach(element => {
        this.values.push(element.count);
        element.city.forEach(item => {
          this.labels.push(item);
        });
        // this.labels.push(element[0].city);
      });

      // console.log(this.values);
      // console.log(this.labels);

      const chartData = [
        {
          values: this.values,
          labels: this.labels,
          type: "pie"
        }
      ];
      this.loader = false;
      const layout = {
        title: "Patient comparison from different city",
        height: 400,
        width: 500
      };

      Plotly.newPlot(divElement, chartData, layout);
    });
  }
}
