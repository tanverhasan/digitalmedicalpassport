import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from "@angular/core";
import { AdmindataService } from "../shared/admindata.service";
import { Subscription } from "rxjs/Subscription";
declare var Plotly: any;
@Component({
  selector: "app-patient-registration",
  template: `
  <app-loading-dot *ngIf="loader"></app-loading-dot>
    <div #chart></div>
  `,
  styles: []
})
export class PatientRegistrationComponent implements OnInit, OnDestroy {
  @ViewChild("chart") el: ElementRef;
  loader = true;
  monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  sub: Subscription;
  x: string[] = [];
  y: number[] = [];
  year = 2018;
  constructor(private data: AdmindataService) {}

  ngOnInit() {
    const element = this.el.nativeElement;
    this.sub = this.data
      .getPatientRegistrationMonthlyView(this.year)
      .subscribe(res => {
        console.log(res);
        // tslint:disable-next-line:no-shadowed-variable
        res.forEach(element => {
          // const date = new Date();
          // date.setMonth(element.month);
          // date.setUTCFullYear(element.month.year, element.month.month);
          // date.set
          // console.log(date);
          this.x.push(this.monthNames[element.month]);
          this.y.push(element.total);
        });
        this.loader = false;
        // console.log(this.x);
        // console.log(this.y);
        const chartData = [
          {
            x: this.x,
            y: this.y,
            type: "scatter"
          }
        ];
        // console.log(chartData);
        const style = {
          title: "US Export of Plastic Scrap",
          xaxis: {
            tickangle: -45
          },
          yaxis: {
            title: "Number of patients"
          },
          margin: { t: 0 }
        };
        Plotly.newPlot(element, chartData, style);
      });
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
