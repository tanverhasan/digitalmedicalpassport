import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { AdmindataService } from "../shared/admindata.service";
declare var Plotly: any;
@Component({
  selector: "app-patient-gender",
  template: `
  <app-loading-dot *ngIf="loader"></app-loading-dot>
    <div #chart></div>
  `,
  styles: []
})
export class PatientGenderComponent implements OnInit {
  chartData: { values: number[]; labels: string[]; type: string }[];
  total: number[] = [];
  labels: string[] = [];
  loader = true;
  @ViewChild("chart") el: ElementRef;
  constructor(private data: AdmindataService) {}

  ngOnInit() {
    const element = this.el.nativeElement;
    this.data.GetGroupByGender().subscribe(res => {
      // console.log(res);

      // tslint:disable-next-line:no-shadowed-variable
      res.forEach(element => {
        this.labels.push(element.gender);
        this.total.push(element.count);
      });
      this.chartData = [
        {
          values: this.total,
          labels: this.labels,
          type: "pie"
        }
      ];
      const layout = {
        title: "Patient Gender percentage",
        height: 400,
        width: 500
      };
      this.loader = false;
      Plotly.newPlot(element, this.chartData, layout);
    });
  }
}
