import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  OnDestroy
} from "@angular/core";
import { AdmindataService } from "../shared/admindata.service";
import { Subscription } from "rxjs/Subscription";
declare var Plotly: any;
@Component({
  selector: "app-doctor-registration",
  template: `
  <app-loading-dot *ngIf="loader"></app-loading-dot>
    <div #chart>
    </div>
  `,
  styles: []
})
export class DoctorRegistrationComponent implements OnInit, OnDestroy {
  @ViewChild("chart") el: ElementRef;
  loader = true;
  monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  sub: Subscription;
  x: string[] = [];
  y: number[] = [];
  year = 2018;
  constructor(private data: AdmindataService) {}

  ngOnInit() {
    const element = this.el.nativeElement;
    this.sub = this.data
      .getDoctorRegistrationMonthlyView(this.year)
      .subscribe(res => {
        console.log(res);
        // tslint:disable-next-line:no-shadowed-variable
        res.forEach(element => {
          this.x.push(this.monthNames[element.month]);
          this.y.push(element.total);
        });
        console.log(this.x);
        console.log(this.y);
        const chartData = [
          {
            x: this.x,
            y: this.y,
            type: "scatter"
          }
        ];
        console.log(chartData);
        const style = {
          title: "US Export of Plastic Scrap",
          xaxis: {
            tickangle: -45
          },
          yaxis: {
            title: "Number of Doctors"
          },
          margin: { t: 0 }
        };
        this.loader = false;
        Plotly.newPlot(element, chartData, style);
      });
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
