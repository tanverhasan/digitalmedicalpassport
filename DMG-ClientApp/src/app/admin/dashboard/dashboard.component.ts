import { Component, OnInit } from "@angular/core";
import { AdmindataService } from "../shared/admindata.service";
import { Observable } from "rxjs/Observable";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  totalPatient: Observable<any>;
  totalDoctor: Observable<any>;
  constructor(private data: AdmindataService) {}

  ngOnInit() {
    this.totalDoctor = this.data.getDoctorCount();
    this.totalPatient = this.data.getPatientCount();
  }
}
