import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { AdmindataService } from "../shared/admindata.service";
declare var Plotly: any;
@Component({
  selector: "app-doctor-sexual-orientation",
  template: `
  <app-loading-dot *ngIf="loader"></app-loading-dot>
   <div #chart></div>
  `,
  styles: []
})
export class DoctorSexualOrientationComponent implements OnInit {
  labels: string[] = [];
  values: number[] = [];
  loader = true;
  @ViewChild("chart") el: ElementRef;
  constructor(private data: AdmindataService) {}

  ngOnInit() {
    const element = this.el.nativeElement;
    this.data.doctorGetGroupBySexualOrientation().subscribe(res => {
      // tslint:disable-next-line:no-shadowed-variable
      res.forEach(element => {
        this.values.push(element.count);
        this.labels.push(element.sexualOrientation);
      });

      // console.log(res);
      // console.log(this.labels);
      // console.log(this.values);
      const chartData = [
        {
          values: this.values,
          labels: this.labels,
          type: "pie"
        }
      ];
      this.loader = false;

      const layout = {
        title: "Doctor Sexual Orientation percentage",
        height: 400,
        width: 500
      };

      Plotly.newPlot(element, chartData, layout);
    });
  }
}
