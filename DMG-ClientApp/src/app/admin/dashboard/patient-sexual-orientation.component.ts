import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { AdmindataService } from "../shared/admindata.service";
declare var Plotly: any;
@Component({
  selector: "app-patient-sexual-orientation",
  template: `
  <app-loading-dot *ngIf="loader"></app-loading-dot>
    <div #chart>
    </div>
  `,
  styles: []
})
export class PatientSexualOrientationComponent implements OnInit {
  labels: string[] = [];
  values: number[] = [];
  loader = true;
  @ViewChild("chart") el: ElementRef;
  constructor(private data: AdmindataService) {}

  ngOnInit() {
    const element = this.el.nativeElement;
    this.data.GetGroupBySexualOrientation().subscribe(res => {
      // tslint:disable-next-line:no-shadowed-variable
      res.forEach(element => {
        this.values.push(element.count);
        this.labels.push(element.sexualOrientation);
      });
      this.loader = false;
      // console.log(res);
      // console.log(this.labels);
      // console.log(this.values);
      const chartData = [
        {
          values: this.values,
          labels: this.labels,
          type: "pie"
        }
      ];

      const layout = {
        title: "Patient Sexual Orientation percentage",
        height: 400,
        width: 500
      };

      Plotly.newPlot(element, chartData, layout);
    });
  }
}
