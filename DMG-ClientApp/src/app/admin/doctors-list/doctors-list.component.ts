import { Component, OnInit } from "@angular/core";
import { FormControl, FormBuilder, FormGroup } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import { AdmindataService } from "../shared/admindata.service";
import {
  startWith,
  distinctUntilChanged,
  switchMap,
  debounceTime
} from "rxjs/operators";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-doctors-list",
  templateUrl: "./doctors-list.component.html",
  styleUrls: ["./doctors-list.component.scss"]
})
export class DoctorsListComponent implements OnInit {
  searchForm: FormGroup;
  patients: Observable<any>;
  loader = true;
  p = 1;
  searchControl: FormControl;
  constructor(
    private data: AdmindataService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    this.searchForm = this.fb.group({
      search: [""]
    });
  }

  ngOnInit() {
    this.searchAndLoad();
  }

  private searchAndLoad() {
    this.searchForm
      .get("search")
      .valueChanges.pipe(
        startWith(""),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(m => this.data.GetDoctorBySearch(m))
      )
      .subscribe(res => {
        this.patients = res;
        this.loader = false;
      });
  }

  delete(id) {
    console.log(id);
    this.data.deleteDoctor(id).subscribe(res => {
      this.snackBar.open("User Deleted ", "Successfully", {
        duration: 4000
      });
      this.searchAndLoad();
    });
  }
}
