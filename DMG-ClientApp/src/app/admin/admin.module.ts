import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { DoctorsListComponent } from "./doctors-list/doctors-list.component";
import { PatientListComponent } from "./patient-list/patient-list.component";
import { AuthGuard } from "../auth/auth.guard";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MaterialModule } from "../design/material.module";
import { NgxPaginationModule } from "ngx-pagination";
import { AuthAdminGuard } from "../auth/auth-admin.guard";
import { PatientGenderComponent } from "./dashboard/patient-gender.component";
import { DoctorGenderComponent } from "./dashboard/doctor-gender.component";
import { DoctorCityComponent } from "./dashboard/doctor-city.component";
import { PatientCityComponent } from "./dashboard/patient-city.component";
import { PatientSexualOrientationComponent } from "./dashboard/patient-sexual-orientation.component";
import { DoctorSexualOrientationComponent } from "./dashboard/doctor-sexual-orientation.component";
import { AdmindataService } from "./shared/admindata.service";
import { PatientRegistrationComponent } from "./dashboard/patient-registration.component";
import { DoctorRegistrationComponent } from "./dashboard/doctor-registration.component";
import { UiLoadingModule } from "../ui/ui-loading.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    UiLoadingModule,
    NgxPaginationModule,
    RouterModule.forChild([
      {
        path: "",
        canActivate: [AuthAdminGuard],
        canActivateChild: [AuthAdminGuard],
        children: [
          { path: "dashboard", component: DashboardComponent },
          { path: "doctors-list", component: DoctorsListComponent },
          { path: "patients-list", component: PatientListComponent }
        ]
      }
    ])
  ],
  declarations: [
    PatientListComponent,
    DoctorsListComponent,
    DashboardComponent,
    PatientGenderComponent,
    DoctorGenderComponent,
    DoctorCityComponent,
    PatientCityComponent,
    PatientSexualOrientationComponent,
    DoctorSexualOrientationComponent,
    PatientRegistrationComponent,
    DoctorRegistrationComponent
  ],
  providers: [AdmindataService]
})
export class AdminModule {}
