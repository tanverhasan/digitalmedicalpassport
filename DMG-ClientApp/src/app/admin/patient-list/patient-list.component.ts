import { Component, OnInit } from "@angular/core";
import { FormControl, FormBuilder, FormGroup } from "@angular/forms";
import {
  startWith,
  debounceTime,
  distinctUntilChanged,
  switchMap
} from "rxjs/operators";
import { Observable } from "rxjs/Observable";
import { AdmindataService } from "../shared/admindata.service";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-patient-list",
  templateUrl: "./patient-list.component.html",
  styleUrls: ["./patient-list.component.scss"]
})
export class PatientListComponent implements OnInit {
  searchForm: FormGroup;
  patients: Observable<any>;
  p = 1;
  searchControl: FormControl;
  show = true;
  constructor(
    private data: AdmindataService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    this.searchForm = this.fb.group({
      search: [""]
    });
  }

  ngOnInit() {
    this.searchAndLoad();
  }
  private searchAndLoad() {
    this.patients = this.searchForm
      .get("search")
      .valueChanges.pipe(
        startWith(""),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(m => this.data.GetPatientBySearch(m))
      );
  }

  delete(id) {
    console.log(id);
    this.data.deletePatient(id).subscribe(res => {
      this.snackBar.open("User Deleted ", "Successfully", {
        duration: 4000
      });
      this.searchAndLoad();
    });
  }
}
