import { Component, OnInit, Inject } from "@angular/core";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from "@angular/material";
import { DataService } from "../shared/data.service";
import { IBookingSlot, ISlot } from "../../doctor/shared/booking";
import { debounceTime } from "rxjs/operators";

@Component({
  selector: "app-booking",
  templateUrl: "./booking.component.html",
  styleUrls: ["./booking.component.scss"]
})
export class BookingComponent implements OnInit {
  slots: ISlot[];
  slotId: string;
  filteredOptions: ISlot[];
  bookingSlots: IBookingSlot[];

  bookingForm: FormGroup;

  get firstName() {
    return this.bookingForm.get("firstName") as FormControl;
  }
  get lastName() {
    return this.bookingForm.get("lastName") as FormControl;
  }
  get email() {
    return this.bookingForm.get("email") as FormControl;
  }
  get bookingDate() {
    return this.bookingForm.get("bookingDate") as FormControl;
  }
  get bookingTime() {
    return this.bookingForm.get("bookinTime") as FormControl;
  }
  get reason() {
    return this.bookingForm.get("reason") as FormControl;
  }
  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<BookingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private snackBar: MatSnackBar,
    private http: DataService
  ) {}

  ngOnInit() {
    this.bookingForm = this.fb.group({
      userId: ["", [Validators.required]],
      firstName: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      email: ["", [Validators.email]],
      bookingDate: ["", [Validators.required]],
      bookingTime: ["", [Validators.required]],
      reason: [""]
    });
    this.bookingForm.patchValue({
      email: localStorage.getItem("email")
    });
    this.bookingForm.get("bookingDate").patchValue(new Date());
    this.bookingForm.get("userId").patchValue(localStorage.getItem("userId"));
    this.bookingForm.get("bookingDate").valueChanges.subscribe(res => {
      this.http.getSlotsByDate(this.data.id, res).subscribe(r => {
        this.bookingSlots = r as IBookingSlot[];
        console.log(this.bookingSlots);
        // this.filteredOptions = this.bookingSlots.filter(f => f.slot.filter(s => s.booked === false));

        this.bookingSlots.forEach(element => {
          this.slotId = element.slotId;
          this.slots = element.slot;
          this.filteredOptions = element.slot.filter(f => f.booked === false);
          console.log(this.filteredOptions);
        });
      });
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
    this.openSnackBar("Booking !", "Unsuccessful");
  }
  save() {
    // this.bookingSlots.forEach(element=>{

    // })

    this.slots.filter(f => {
      if (f.time === this.bookingForm.get("bookingTime").value) {
        f.booked = true;
        f.userId = localStorage.getItem("userId");
        f.firstName = this.firstName.value;
        f.lastName = this.lastName.value;
        f.email = this.email.value;
        f.reason = this.reason.value;
      }
    });
    console.log(this.slots);

    this.http
      .updateSlot(this.data.id, this.slotId, this.bookingDate.value, this.slots)
      .then(() => {
        console.log("Updated");
        this.openSnackBar("Appointment", "Successfull");
      });
    this.http
      .addAppointmentToMyProfile(
        localStorage.getItem("userId"),
        this.data.id,
        this.data.firstName,
        this.data.lastName,
        this.bookingForm.get("bookingDate").value,
        this.bookingForm.get("bookingTime").value
      )
      .then(() => {
        console.log("confirmed");
      });

    // this.http.AddAppointment(this.data.id, this.bookingForm.value).then(() => {
    //   this.openSnackBar("Booking", "Successful");
    //   this.dialogRef.close();
    // });
  }
  openSnackBar(part1, part2) {
    this.snackBar.open(part1, part2, { duration: 3000 });
  }
}
