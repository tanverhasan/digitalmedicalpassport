import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { IDoctor } from "./doctor";
import { HttpHeaders } from "@angular/common/http";
import { HttpParams } from "@angular/common/http";
import { ErrorObservable } from "rxjs/observable/ErrorObservable";
import { catchError, retry, map } from "rxjs/operators";
import { AngularFirestore } from "angularfire2/firestore";
import { IBooking } from "./booking";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class DataService {
  private httpStatusCode = new BehaviorSubject<any>(null);
  statusCode = this.httpStatusCode.asObservable();
  constructor(private http: HttpClient, private afs: AngularFirestore) {}

  GetDoctor() {
    return this.http
      .get(`${environment.api.baseUrl}Doctor`)
      .map(res => {
        return res as IDoctor[];
      })
      .catch(c => this.handleError(c));
  }

  GetDoctorBySearch(searchTerm: string) {
    // console.log(searchTerm);
    const term = searchTerm.trim();
    const options = term
      ? { params: new HttpParams().set("SearchQuery", term) }
      : {};
    return this.http
      .get(
        `${
          environment.api.baseUrl
        }Doctor?PageNumber=1&PageSize=20&SearchQuery=${searchTerm}`
      )
      .pipe(
        retry(3),
        map(res => res as IDoctor[]),
        catchError(c => this.handleError(c))
      );
    // .map(res => res as IDoctor[]);
    // .pipe(retry(3), catchError(this.handleError));
  }

  AddAppointment(userId: string, data: any) {
    const booking: IBooking = Object.assign({}, data);
    const id = this.afs.createId();
    booking.id = id;
    return this.afs
      .collection("users")
      .doc(userId)
      .collection("bookings")
      .doc(id)
      .set(booking);
  }

  updateSlot(userId, slotId, date, slots) {
    return this.afs
      .collection("users")
      .doc(userId)
      .collection("slots", ref => ref.where("date", "==", date))
      .doc(slotId)
      .update({ slot: slots });
  }

  getSlotsByDate(id, date) {
    console.log(id, date);
    return this.afs
      .collection("users")
      .doc(id)
      .collection("slots", ref => ref.where("date", "==", date))
      .valueChanges();
  }

  addAppointmentToMyProfile(
    id: string,
    doctorid: string,
    firstName: string,
    lastName: string,
    bookingDate: Date,
    bookingTime: string
  ) {
    const docid = this.afs.createId();

    const data = {
      id: docid,
      doctorId: doctorid,
      firstName: firstName,
      lastName: lastName,
      bookingDate: bookingDate,
      bookingTime: bookingTime
    };
    return this.afs
      .collection(`users`)
      .doc(id)
      .collection(`appointments`)
      .doc(docid)
      .set(data);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error("An error occurred:", error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
      this.httpStatusCode.next(error.status);
    }
    return new ErrorObservable(
      "Something bad happened; please try again later."
    );
  }
}
