

export class IBooking {
  id: string;
  userId: string;
  firstName: string;
  lastName: string;
  email: string;
  bookingDate: Date;
  bookingTime: string;
}

