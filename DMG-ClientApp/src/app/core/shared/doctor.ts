export interface IDoctor {
  id: string;
  userId: string;
  firstName: string;
  lastName: string;
  gender: string;
  dob: Date;
  email: string;
  confirmEmail: string;
  phoneNumbers: IPhoneNumber[];
  timeStamp: string;
  addresses: IAddress[];
  occupation: string;
  sexualOrientation: string;
  livingStatus: string;
  alcohol: string;
  socialHistory: string;
  alergys: IAlergy[];
  prescriptions: IPrescription[];
  qualifications: IQualification[];
  doctors: any[];
  patients: IDoctorPatient[];
  links: any[];
}
export interface IPatient {
  id: string;
  title: string;
  userId: string;
  firstName: string;
  lastName: string;
  gender: string;
  dob: Date;
  email: string;
  phoneNumbers: IPhoneNumber[];
  timeStamp: string;
  addresses: IAddress[];
  occupation: string;
  sexualOrientation: string;
  livingStatus: string;
  alcohol: string;
  socialHistory: string;
  alergys: IAlergy[];
  prescriptions: IPrescription[];
  doctors: any[];
  links: any[];
}
export interface IDoctorPatient {
  patientId: string;
  patientFirstName: string;
  patientLastName: string;
  role: string;
  status: boolean;
}
export interface IQualification {
  university: string;
  subject: string;
  degree: string;
}
export interface IPhoneNumber {
  type: string;
  number: string;
}

export interface IAddress {
  addressType: string;
  street1: string;
  street2: string;
  city: string;
  state: string;
  postal: string;
}

export interface IAlergy {
  type: string;
  description: string;
}

export interface IMedicine {
  name: string;
  description: string;
}

export interface IPrescription {
  userId: string;
  medicines: IMedicine[];
  comments: string;
  currentDateAndTime: DateTimeFormat;
}

export interface IRequest {
  requestId: string;
  authorization: boolean;
  firstName: string;
  lastName: string;
}

export interface IUserReq {
  id: string;
  email: string;
  requests: IRequest[];
}
