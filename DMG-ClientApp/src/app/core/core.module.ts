import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DoctorListComponent } from "./doctor-list/doctor-list.component";
import { DataService } from "./shared/data.service";
import { Router } from "@angular/router";
import { RouterModule } from "@angular/router";
import { MaterialModule } from "../design/material.module";
import { UiLoadingModule } from "../ui/ui-loading.module";
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthModule } from "../auth/auth.module";
import { BookingComponent } from "./booking/booking.component";
import { AuthGuard } from "../auth/auth.guard";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    UiLoadingModule,
    NgxPaginationModule,
    AuthModule,
    MaterialModule,
    RouterModule.forChild([
      {
        path: "",
        children: [
          { path: "doctor-list", component: DoctorListComponent },
          {
            path: "appointment",
            component: BookingComponent,
            canActivate: [AuthGuard]
          }
        ]
      }
    ])
  ],
  declarations: [DoctorListComponent, BookingComponent],
  entryComponents: [BookingComponent],
  providers: [DataService]
})
export class CoreModule {}
