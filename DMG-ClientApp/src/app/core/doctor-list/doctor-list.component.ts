import { Component, OnInit } from "@angular/core";
import { DataService } from "../shared/data.service";
import { IDoctor } from "../shared/doctor";
import { Subject } from "rxjs/Subject";
import { debounceTime } from "rxjs/operators/debounceTime";
import { distinctUntilChanged } from "rxjs/operators/distinctUntilChanged";
import { Observable } from "rxjs/Observable";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { startWith, switchMap } from "rxjs/operators";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from "@angular/material";
import { BookingComponent } from "../booking/booking.component";
import { AuthService } from "../../auth/auth.service";

@Component({
  selector: "app-doctor-list",
  templateUrl: "./doctor-list.component.html",
  styleUrls: ["./doctor-list.component.scss"]
})
export class DoctorListComponent implements OnInit {
  doctors: Observable<IDoctor[]>;
  searchForm: FormGroup;
  loader = true;
  p = 1;

  // tslint:disable-next-line:max-line-length
  constructor(
    private data: DataService,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private auth: AuthService,
    private snackbar: MatSnackBar
  ) {
    this.searchForm = this.fb.group({
      search: [""]
    });
  }

  ngOnInit() {
    this.searchForm
      .get("search")
      .valueChanges.pipe(
        startWith(""),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(m => this.data.GetDoctorBySearch(m))
      )
      .subscribe(res => {
        this.doctors = res;
        this.loader = false;
      });

    this.data.statusCode.subscribe(res => {
      if (res === 404) {
        this.loader = false;
      }
    });
  }
  openDialog(
    id: string,
    firstName: string,
    lastName: string,
    areaOfWork: string
  ) {
    if (this.auth.isAuthenticated()) {
      this.dialog.open(BookingComponent, {
        width: "250px",
        data: { id, firstName, lastName, areaOfWork }
      });
    } else {
      this.snackbar.open("Sign up", "Required", {
        duration: 3000
      });
    }
  }
}
