import { RouterModule, Routes } from "@angular/router";
import { ProfileComponent } from "../auth/profile/profile.component";
import { AuthGuard } from "../auth/auth.guard";
import { HomeComponent } from "../home/home.component";
// import { DoctorModule } from "../doctor/doctor.module";
// import { PatientModule } from "../patient/patient.module";
import { NgModule } from "@angular/core";
import { AuthModule } from "../auth/auth.module";
import { AuthDoctorLoadGuard } from "../auth/auth-doctor-load.guard";
import { AuthPatientLoadGuard } from "../auth/auth-patient-load.guard";
import { AuthAdminLoadGuard } from "../auth/auth-admin-load.guard";
// import { SkypeModule } from "../skype/skype.module";
// import { CoreModule } from "../core/core.module";
// import { AdminModule } from "../admin/admin.module";

const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "home", component: HomeComponent },
  { path: "profile", component: ProfileComponent, canActivate: [AuthGuard] },

  {
    path: "doctor",
    loadChildren: "../doctor/doctor.module#DoctorModule",
    canLoad: [AuthDoctorLoadGuard]
  },
  {
    path: "patient",
    loadChildren: "../patient/patient.module#PatientModule",
    canLoad: [AuthPatientLoadGuard]
  },
  { path: "core", loadChildren: "../core/core.module#CoreModule" },
  {
    path: "admin",
    loadChildren: "../admin/admin.module#AdminModule",
    canLoad: [AuthAdminLoadGuard]
  },
  {
    path: "skype",
    loadChildren: "../skype/skype.module#SkypeModule"
  },
  // { path: 'admin', component: AdminComponent, canActivate: [AuthAdminGuard] },
  // { path: 'fetch-data', component: FetchDataComponent, canActivate: [AuthGuard] },
  { path: "**", redirectTo: "home" }
  // uploads are lazy loaded
  // { path: 'uploads', loadChildren: './uploads/shared/upload.module#UploadModule', canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), AuthModule.forRoot()],
  exports: [RouterModule]
})
export class AppRoutingModule {}
