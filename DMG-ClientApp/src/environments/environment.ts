export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBSZCcF4T_StFQHJMyfCPrk-hkYYp5nH7I",
    authDomain: "digitalmedicalpassport.firebaseapp.com",
    databaseURL: "https://digitalmedicalpassport.firebaseio.com",
    projectId: "digitalmedicalpassport",
    storageBucket: "digitalmedicalpassport.appspot.com",
    messagingSenderId: "16224129375"
  },
  auth: {
    clientId: "BVsIkCR4T2fURbGPMwjGQbQ024BtkkhF",
    domain: "tanver.eu.auth0.com", // e.g., you.auth0.com
    audience: "http://localhost:5000", // e.g., http://localhost:1337/
    redirect: "http://localhost:5000/",
    apiUrl: "https://api.digitalmedicalpassport.com",
    scope: "openid email profile read:messages write:messages",
    roleUrl: "http://localhost:5000"
  },
  api: {
    baseUrl: "http://localhost:5001/api/",
    mail: "http://localhost:5003/api/"
  },
  skype: {
    api_key: "1b129ef8-2d46-b59b-9346-16d1896b8d20",
    api_secrete: "5c71f501-1d05-447e-f13c-4ca97934807e"
  }
};
