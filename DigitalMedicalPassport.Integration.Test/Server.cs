using DigitalMedicalPassport.API;
using DigitalMedicalPassport.API.DTO;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace DigitalMedicalPassport.Integration.Test
{
    public class Server : IDisposable
    {
        private readonly TestServer testServer;
        public HttpClient client { get; private set; }
        public Server()
        {
            var server = new TestServer(new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>());
            client = server.CreateClient();
            client.BaseAddress = new Uri("http://localhost:5001");
        }

        public virtual void Dispose()
        {
            client.Dispose();
            testServer.Dispose();
        }
    }
}
