﻿using ApplicationCore.Entities;
using DigitalMedicalPassport.API.DTO;
using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DigitalMedicalPassport.Integration.Test
{
    public class PatientControllerTest
    {
        private HttpClient client;
        public PatientControllerTest()
        {
            this.client = new HttpClient();
            this.client.BaseAddress = new Uri("http://localhost:5001");
            this.client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1rVTFNVE5GUVVGRU5EWXlNRFE1TWpnd05UQXdNMEk1UVVFeU5EWkJRalUwTnpRMVFqaEJSUSJ9.eyJodHRwOi8vbG9jYWxob3N0OjUwMDAvcm9sZXMiOlsidXNlciJdLCJpc3MiOiJodHRwczovL3RhbnZlci5ldS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTllYjU5NzMyM2MyZTUyNGZmNzU4Nzg4IiwiYXVkIjpbImh0dHBzOi8vYXBpLmRpZ2l0YWxtZWRpY2FscGFzc3BvcnQuY29tIiwiaHR0cHM6Ly90YW52ZXIuZXUuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyMDE3OTU1NywiZXhwIjoxNTIwMTg2NzU3LCJhenAiOiJCVnNJa0NSNFQyZlVSYkdQTXdqR1FiUTAyNEJ0a2toRiIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwifQ.R029RCYEicnZ-1vzsBAV3bnP9BmBAkBu3FXZMVeUIP5igjJt2UX140VP-R4Xysosg-T25mKvZ9t79-30gFlyuoDxV0oqQOt9bE1-gFgCUOpDQBg48-qcTwOijH3BacREPNZWogH1X-GuVy8aWVsfmyYFQMYMT0lY4BhbsrS3WYEAgf17v39Hec8o8xuS5sFWuLcQjbySy22V_cZOHWU0MO9Owyd7lqqRyTQIliiU2R2cdwHqM1kQt8VSa9cBXXkcWQIZfNdQTByVvEUJ7ZsF8OpAvrBVUgWW9TWeI1m8O2csSDIpmqalP16MLFzNDGbyV2-uTe80CFXimLV6RtSHSQ");

        }
        [Fact]
        public async Task Get_GetAll()
        {

            var response = await client.GetAsync("/api/Patient");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());
            var responsestring = await response.Content.ReadAsStringAsync();
            var doctors = JsonConvert.DeserializeObject<IEnumerable<PatientDto>>(responsestring);
            doctors.Count().Should().Be(4);
        }

        [Fact]
        public async Task Get_Pagination()
        {
            var response = await client.GetAsync("api/Patient?PageNumber=1&PageSize=20");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());
            Assert.NotNull(response.Headers.GetValues("x-pagination"));
            var responsestring = await response.Content.ReadAsStringAsync();
            var doctors = JsonConvert.DeserializeObject<IEnumerable<DoctorDto>>(responsestring);
            doctors.Count().Should().Be(4);
        }

        [Fact]
        public async Task Get_Filter()
        {
            var response = await client.GetAsync("api/Patient?PageNumber=1&PageSize=20&FirstName=Tanver");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());

            var responsestring = await response.Content.ReadAsStringAsync();
            var doctors = JsonConvert.DeserializeObject<IEnumerable<DoctorDto>>(responsestring);
            doctors.Count().Should().Be(2);
            foreach (var item in doctors)
            {
                item.FirstName.Should().Be("Tanver");
            };
        }

        [Fact]
        public async Task Get_Search()
        {
            var response = await client.GetAsync("api/Patient?PageNumber=1&PageSize=10&SearchQuery=dhaka");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());
            Assert.True(response.IsSuccessStatusCode);
            var responsestring = await response.Content.ReadAsStringAsync();
            var doctors = JsonConvert.DeserializeObject<IEnumerable<DoctorDto>>(responsestring);
            doctors.Count().Should().Be(1);
            foreach (var item in doctors)
            {
                item.FirstName.Should().Be("Tanver");
            };
        }
        [Fact]
        public async Task Get_NullTest()
        {
            var response = await client.GetAsync("api/Patient?PageNumber=1&PageSize=10&SearchQuery=ABCK");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());
            Assert.True(response.IsSuccessStatusCode);
            var responsestring = await response.Content.ReadAsStringAsync();
            var doctors = JsonConvert.DeserializeObject<IEnumerable<DoctorDto>>(responsestring);
            doctors.Count().Should().Be(0);

        }
        [Fact]
        public async Task GeyById()
        {
            var response = await client.GetAsync("api/Patient/59eb597323c2e524ff758789");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());
            Assert.True(response.IsSuccessStatusCode);
            var responsestring = await response.Content.ReadAsStringAsync();
            var doctor = JsonConvert.DeserializeObject<DoctorDto>(responsestring);
            doctor.FirstName.Should().Be("Tanver");
            doctor.UserId.Should().Be("59eb597323c2e524ff758789");
        }

        [Fact]
        public async Task Post()
        {
            var doctor = new PatientCreationDto
            {
                Title = "Mr",
                UserId = "123456789123456789",
                FirstName = "Aminul",
                LastName = "Rifat",
                Gender = "Male",
                Dob = DateTime.Now,
                Email = "aminul@outlook.com",



            };
            var content = JsonConvert.SerializeObject(doctor);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");


            var response = await client.PostAsync("/api/Patient", stringContent);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();

            Assert.Equal("OK", response.StatusCode.ToString());


        }
        [Fact]
        public async Task Delete()
        {
            var id = "123456789123456789";
            var response = await client.DeleteAsync($"api/Patient/{id}");
            response.EnsureSuccessStatusCode();

        }

       
    }
}