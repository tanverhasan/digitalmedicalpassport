﻿using ApplicationCore.Entities;
using DigitalMedicalPassport.API.DTO;
using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static DigitalMedicalPassport.API.Controllers.DoctorPatientsController;

namespace DigitalMedicalPassport.Integration.Test
{
    public class DoctorPatientsControllerTest
    {
        private HttpClient client;
        public DoctorPatientsControllerTest()
        {
            this.client = new HttpClient();
            this.client.BaseAddress = new Uri("http://localhost:5001");
            this.client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1rVTFNVE5GUVVGRU5EWXlNRFE1TWpnd05UQXdNMEk1UVVFeU5EWkJRalUwTnpRMVFqaEJSUSJ9.eyJodHRwOi8vbG9jYWxob3N0OjUwMDAvcm9sZXMiOlsiZG9jdG9yIl0sImlzcyI6Imh0dHBzOi8vdGFudmVyLmV1LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1OWViNGNmNzY2ZDNhNjFhYTIyMTZlZTkiLCJhdWQiOlsiaHR0cHM6Ly9hcGkuZGlnaXRhbG1lZGljYWxwYXNzcG9ydC5jb20iLCJodHRwczovL3RhbnZlci5ldS5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNTIwMTkxMzI5LCJleHAiOjE1MjAxOTg1MjksImF6cCI6IkJWc0lrQ1I0VDJmVVJiR1BNd2pHUWJRMDI0QnRra2hGIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCJ9.JtSHEj89WOSjuYAkZTRMhDaHlDI4bN4i89wtnRAjD7OhPXqcU3oldON5FGSFIbgEngZRGxUQxJmqpHY0U0li9xkTx48JLxogyuDb5b5gXbhV1dhhd2C6-1VV5ipp-M3ppXdvzmY58qfo5FPDk9ezB9L4pB1jaiQudInVpwwPpqoBETvWRAAAhLdIepJJEJZFZgBlJSVuEqDY6CHz4lJaQ549yywqAMf4WoxQWR6hYZlCPkLUjb5hjvlVF0ciNQF1_4b8K3QfX3rHcXJ3ej9JKEujSoeOtUwNH6172SVFD2CwupT_tESkT0SP2K8WTnY80p0TERa6f0OfBqY_LsztUw");

        }
        [Fact]
        public async Task Test_Get()
        {
            var response= await this.client.GetAsync("/api/DoctorPatients/59eb4cf766d3a61aa2216ee9");
            response.EnsureSuccessStatusCode();

            var data =await response.Content.ReadAsStringAsync();
            var patients = JsonConvert.DeserializeObject<IEnumerable<PatientRef>>(data);
            patients.Count().Should().Be(2);
        }

        [Fact]
        public async Task Test_Post()
        {
            var model = new DoctorPatientRef
            {
                DoctorId = "59eb4cf766d3a61aa2216ee9",
                PatientId = "59eb597323c2e524ff758789",
                PatientFirstNmae = "test1",
                PatientLastName = "test1",
                Role = "user",
                status = true,
            };

            var content = JsonConvert.SerializeObject(model);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

            var response = await this.client.PostAsync("/api/DoctorPatients", stringContent);

            response.EnsureSuccessStatusCode();
            Assert.Equal("NoContent", response.StatusCode.ToString());
        }

        [Fact]
        public async Task Test_Put()
        {
            var model = new DoctorPatientParam
            {
                PatientId = "59eb597323c2e524ff758789",
                status = true
            };
            var content = JsonConvert.SerializeObject(model);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");
            var response =await this.client.PutAsync("/api/DoctorPatients/59eb597323c2e524ff758789", stringContent);

            response.EnsureSuccessStatusCode();

        }
    }
}
