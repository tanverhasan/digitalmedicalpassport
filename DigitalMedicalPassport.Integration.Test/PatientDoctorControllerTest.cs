﻿using DigitalMedicalPassport.API.DTO;
using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DigitalMedicalPassport.Integration.Test
{
   public class PatientDoctorControllerTest
    {
        private HttpClient client;
        public PatientDoctorControllerTest()
        {
            this.client = new HttpClient();
            this.client.BaseAddress = new Uri("http://localhost:5001");
            this.client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1rVTFNVE5GUVVGRU5EWXlNRFE1TWpnd05UQXdNMEk1UVVFeU5EWkJRalUwTnpRMVFqaEJSUSJ9.eyJodHRwOi8vbG9jYWxob3N0OjUwMDAvcm9sZXMiOlsidXNlciJdLCJpc3MiOiJodHRwczovL3RhbnZlci5ldS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTllYjU5NzMyM2MyZTUyNGZmNzU4Nzg4IiwiYXVkIjpbImh0dHBzOi8vYXBpLmRpZ2l0YWxtZWRpY2FscGFzc3BvcnQuY29tIiwiaHR0cHM6Ly90YW52ZXIuZXUuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyMDE4NzQwOSwiZXhwIjoxNTIwMTk0NjA5LCJhenAiOiJCVnNJa0NSNFQyZlVSYkdQTXdqR1FiUTAyNEJ0a2toRiIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwifQ.WRX3YAIS9XDxg9eU5uDxYtA3M784CwB2mvfv6MTg5PQ9R_o8PcxecUzwQA-_JGUqlNwQZRbgVzBWgfYQoz2XsLHEYqateelE7e_3gpkctRo-Syd6whkA_siTa20Wrw774Esl5Pka2RucbMwVoFgwzXeRmTs_mGUCl1cBOM6E57sBKsQLfd04p5hchUZV23L-Rnyw1SZQZnmnJl-fcUr2XhZob8jnwn63muBKdkqvR3eI5X1TUlZLovj-s7qev37YR32G_xZCDKcXI8qdIlGLOf1DEkodIdOzeK5P7ENAsa0dM42hLXe78iiZwq7rrm3EP65qEXzAeSpa0I6OMwktrg");

        }
        [Fact]
        public async Task Test_Get()
        {

            var response = await this.client.GetAsync($"/api/PatientDoctor/59eb597323c2e524ff758789");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());
            var result = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject< IEnumerable<DoctorRefDto>>(result);
            data.Count().Should().Be(1);
        }
        [Fact]
        public async Task Test_Post()
        {
            var model = new DoctorRefCreationDto
            {
                UserId= "59eb597323c2e524ff758789",
                DoctorId= "59eb4cf766d3a61aa2216ee9",
                FirstName="Sadik",
                LastName="Mahmud",
                Status=true
            };

            var content = JsonConvert.SerializeObject(model);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");


            var response = await client.PostAsync("/api/PatientDoctor", stringContent);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();

            Assert.Equal("NoContent", response.StatusCode.ToString());
        }
    }
}
