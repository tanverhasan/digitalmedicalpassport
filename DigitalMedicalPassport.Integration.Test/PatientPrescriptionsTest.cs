﻿using DigitalMedicalPassport.API.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DigitalMedicalPassport.Integration.Test
{
    public class PatientPrescriptionsTest
    {
        private HttpClient client;
        public PatientPrescriptionsTest()
        {
            this.client = new HttpClient();
            this.client.BaseAddress = new Uri("http://localhost:5001");
            this.client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1rVTFNVE5GUVVGRU5EWXlNRFE1TWpnd05UQXdNMEk1UVVFeU5EWkJRalUwTnpRMVFqaEJSUSJ9.eyJodHRwOi8vbG9jYWxob3N0OjUwMDAvcm9sZXMiOlsidXNlciJdLCJpc3MiOiJodHRwczovL3RhbnZlci5ldS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTllYjU5NzMyM2MyZTUyNGZmNzU4Nzg4IiwiYXVkIjpbImh0dHBzOi8vYXBpLmRpZ2l0YWxtZWRpY2FscGFzc3BvcnQuY29tIiwiaHR0cHM6Ly90YW52ZXIuZXUuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyMDE4NzQwOSwiZXhwIjoxNTIwMTk0NjA5LCJhenAiOiJCVnNJa0NSNFQyZlVSYkdQTXdqR1FiUTAyNEJ0a2toRiIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwifQ.WRX3YAIS9XDxg9eU5uDxYtA3M784CwB2mvfv6MTg5PQ9R_o8PcxecUzwQA-_JGUqlNwQZRbgVzBWgfYQoz2XsLHEYqateelE7e_3gpkctRo-Syd6whkA_siTa20Wrw774Esl5Pka2RucbMwVoFgwzXeRmTs_mGUCl1cBOM6E57sBKsQLfd04p5hchUZV23L-Rnyw1SZQZnmnJl-fcUr2XhZob8jnwn63muBKdkqvR3eI5X1TUlZLovj-s7qev37YR32G_xZCDKcXI8qdIlGLOf1DEkodIdOzeK5P7ENAsa0dM42hLXe78iiZwq7rrm3EP65qEXzAeSpa0I6OMwktrg");

        }
        [Fact]
        public async Task Test_Post()
        {
            var model = new PrescriptionCreationDto
            {
                UserId = "59eb597323c2e524ff758789",
                CurrentDate = DateTime.Now,
                Medicines = new List<MedicineCreationDto>
                {
                    new MedicineCreationDto
                {
                    Name="Abcd",
                    Description="aldkf"
                }
                },
                Comments = "lsdjflaskdjf"
            };

            var content = JsonConvert.SerializeObject(model);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");


            var response = await this.client.PostAsync("/api/PatientPrescriptions",stringContent);
            response.EnsureSuccessStatusCode();

        }
    }
}
