﻿using ApplicationCore.Entities;
using DigitalMedicalPassport.API;
using DigitalMedicalPassport.API.DTO;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DigitalMedicalPassport.Integration.Test
{
    public class DoctorControllerTest
    {
        private HttpClient client;
        public DoctorControllerTest()
        {
            this.client = new HttpClient();
            this.client.BaseAddress = new Uri("http://localhost:5001");
            this.client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1rVTFNVE5GUVVGRU5EWXlNRFE1TWpnd05UQXdNMEk1UVVFeU5EWkJRalUwTnpRMVFqaEJSUSJ9.eyJodHRwOi8vbG9jYWxob3N0OjUwMDAvcm9sZXMiOlsidXNlciJdLCJpc3MiOiJodHRwczovL3RhbnZlci5ldS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTllYjU5NzMyM2MyZTUyNGZmNzU4Nzg4IiwiYXVkIjpbImh0dHBzOi8vYXBpLmRpZ2l0YWxtZWRpY2FscGFzc3BvcnQuY29tIiwiaHR0cHM6Ly90YW52ZXIuZXUuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyMDA4Mzg0OCwiZXhwIjoxNTIwMDkxMDQ4LCJhenAiOiJCVnNJa0NSNFQyZlVSYkdQTXdqR1FiUTAyNEJ0a2toRiIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwifQ.BZo4_qyO34DZjZ5Jky2Kk6YPML3RjF3IAFJHQQ3gUnsT-U0ggSmFK5Kr6_9gjYanecoSpT8fE86ZvuOoMK85zdaAvkF7ya-vlvfE9n2duMtEBCJ0MnxMfNZg3SkZfZJcxGerHswsoXgOREqPmNFnIqVBbg5gG6JhOiIG1Ft7AWepMczrBzazSlXnWslRWcm1o8YA9tAhICYAzPoiWA_nPBXoxnZ5k0fkXkjw9deeoEJdirBKC4TA9f-eGE0ryakSRzLW6aMAUG1zrc-DF460WnG12B45iCCqGQI9rv-MhQB_82S8gN6OOloGJApFAO_L01nEhQ6_hgqfSOxiJgl_-w");

        }
        [Fact]
        public async Task Get_GetAll()
        {

            var response = await client.GetAsync("/api/Doctor");

            response.EnsureSuccessStatusCode();
            Assert.Equal("200", response.StatusCode.ToString());
            var responsestring = await response.Content.ReadAsStringAsync();
            var doctors = JsonConvert.DeserializeObject<IEnumerable<DoctorDto>>(responsestring);
            doctors.Count().Should().Be(2);
        }

        [Fact]
        public async Task Get_Pagination()
        {
            var response = await client.GetAsync("api/Doctor?PageNumber=1&PageSize=20");

            response.EnsureSuccessStatusCode();
            Assert.Equal("200", response.StatusCode.ToString());
            Assert.NotNull(response.Headers.GetValues("x-pagination"));
            var responsestring = await response.Content.ReadAsStringAsync();
            var doctors = JsonConvert.DeserializeObject<IEnumerable<DoctorDto>>(responsestring);
            doctors.Count().Should().Be(2);
        }

        [Fact]
        public async Task Get_Filter()
        {
            var response = await client.GetAsync("api/Doctor?PageNumber=1&PageSize=20&FirstName=Tanver");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());

            var responsestring = await response.Content.ReadAsStringAsync();
            var doctors = JsonConvert.DeserializeObject<IEnumerable<DoctorDto>>(responsestring);
            doctors.Count().Should().Be(2);
            foreach (var item in doctors)
            {
                item.FirstName.Should().Be("Tanver");
            };
        }

        [Fact]
        public async Task Get_Search()
        {
            var response = await client.GetAsync("api/Doctor?PageNumber=1&PageSize=10&SearchQuery=dhaka");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());
            Assert.True(response.IsSuccessStatusCode);
            var responsestring = await response.Content.ReadAsStringAsync();
            var doctors = JsonConvert.DeserializeObject<IEnumerable<DoctorDto>>(responsestring);
            doctors.Count().Should().Be(1);
            foreach (var item in doctors)
            {
                item.FirstName.Should().Be("Tanver");
            };
        }
        [Fact]
        public async Task Get_NullTest()
        {
            var response = await client.GetAsync("api/Doctor?PageNumber=1&PageSize=10&SearchQuery=ABCK");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());
            Assert.True(response.IsSuccessStatusCode);
            var responsestring = await response.Content.ReadAsStringAsync();
            var doctors = JsonConvert.DeserializeObject<IEnumerable<DoctorDto>>(responsestring);
            doctors.Count().Should().Be(0);

        }
        [Fact]
        public async Task GeyById()
        {
            var response = await client.GetAsync("api/Doctor/59eb4cf766d3a61aa2216ee9");

            response.EnsureSuccessStatusCode();
            Assert.Equal("OK", response.StatusCode.ToString());
            Assert.True(response.IsSuccessStatusCode);
            var responsestring = await response.Content.ReadAsStringAsync();
            var doctor = JsonConvert.DeserializeObject<DoctorDto>(responsestring);
            doctor.FirstName.Should().Be("Tanver");
            doctor.UserId.Should().Be("59eb4cf766d3a61aa2216ee9");
        }

        [Fact]
        public async Task Post()
        {
            var doctor = new Doctor
            {
                Title = "Mr",
                UserId = "123456789123456789",
                FirstName = "Aminul",
                LastName = "Rifat",
                Gender="Male",
                Dob = DateTime.Now,
                Email = "aminul@outlook.com",
                
                

            };
            var content = JsonConvert.SerializeObject(doctor);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");


            var response = await client.PostAsync("/api/Doctor", stringContent);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();

            Assert.Equal("OK", response.StatusCode.ToString());


       }
        [Fact]
        public async Task Delete()
        {
            var id = "123456789123456789";
           var response= await client.DeleteAsync($"api/Doctor/{id}");
            response.EnsureSuccessStatusCode();
        
        }
    
    }
}
