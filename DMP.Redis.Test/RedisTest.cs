
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StackExchange.Redis;
using System.Net;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using NFluent;
using System;
using System.Threading;

namespace DMP.Redis.Test
{

    [TestClass]
    public class RedisTest
    {
        private static string connectionString="DigitalMP.redis.cache.windows.net:6380,password=bfecW6t+hPArlPmzU1uOIfvTV4BZ5mu5CqdhxYdkVBE=,ssl=True,abortConnect=False";

        
        public RedisTest()
        {
            
            
        }
        [TestMethod]
        public void TestMethod1()
        {
           
            var cs =  ConnectionMultiplexer.Connect(connectionString);
            Check.That(cs).IsNotNull();
            var database = cs.GetDatabase();

            database.StringSet("mykey", "myvalue", TimeSpan.FromSeconds(5));
            var fetch = database.StringGet("mykey");
            Check.That(fetch.HasValue);

            Thread.Sleep(6000);
            var fetch2 = database.StringGet("mykey");
            Check.That(fetch2.HasValue).IsFalse();
        }
    }
}
