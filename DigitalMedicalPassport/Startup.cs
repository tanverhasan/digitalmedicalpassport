using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Diagnostics;

namespace DigitalMedicalPassport
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            //Configuration = configuration;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);



            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            //services.AddMvc(action =>
            //{
            //    action.ReturnHttpNotAcceptable = true;
            //    action.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
            //    action.InputFormatters.Add(new XmlDataContractSerializerInputFormatter());
            //});
            //services.Configure<MvcOptions>(options =>
            //{
            //    options.Filters.Add(new CorsAuthorizationFilterFactory("AllowSpecificOrigin"));
            //});
            //services.AddCors();
            // Register the Swagger generator, defining one or more Swagger documents
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            //});
            //// 1. Add Authentication Services
            //services.AddAuthentication(options =>
            //{
            //    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            //}).AddJwtBearer(options =>
            //{
            //    options.Authority = "https://tanver.eu.auth0.com/";
            //    options.Audience = "https://api.digitalmedicalpassport.com";
            //});
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy(AppPolicy.RequireAdminRole, policy => policy.RequireClaim("http://localhost:5000/roles", "admin"));
            //    options.AddPolicy(AppPolicy.RequireDoctorRole, policy => policy.RequireClaim("http://localhost:5000/roles", "doctor"));
            //    options.AddPolicy(AppPolicy.RequireUserRole, policy => policy.RequireClaim("http://localhost:5000/roles", "user"));
            //});

            //DI
            // services.AddScoped<typeof(DbContext<>), typeof(DbContext<>));

            //services.AddTransient<IPatientRepository, PatientRepository>();
            //services.AddTransient<IDoctorRepository, DoctorRepository>();
            //services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            //services.AddScoped<IUrlHelper, UrlHelper>(implementationFactory =>
            //{
            //    var actionContext = implementationFactory.GetService<IActionContextAccessor>().ActionContext;
            //    return new UrlHelper(actionContext);
            //});

            

          
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            loggerFactory.AddConsole();
            loggerFactory.AddDebug(LogLevel.Information);
            // loggerFactory.AddProvider(new NLog.Extensions.Logging.NLogLoggerProvider());
            //  loggerFactory.AddNLog();
            //app.UseIpRateLimiting();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        var logger = loggerFactory.CreateLogger("Global Exception Logger");

                        var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                        if (exceptionHandlerFeature != null)
                        {
                            logger.LogError(500, exceptionHandlerFeature.Error, exceptionHandlerFeature.Error.Message);
                        }
                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("An unexpected fault happend. Try again later");
                    });
                });
            }
            // Shows UseCors with named policy.
            // app.UseCors("AllowSpecificOrigin");
            //AutoMapper.Mapper.Initialize(conf =>
            //{
            //    conf.CreateMap(typeof(Task<>), typeof(Task<>));
            //    //Patient maping configuration
            //    conf.CreateMap<Patient, PatientDto>()
            //    .ForMember(dest => dest.Id, ob => ob.MapFrom(src => src._id.ToString()));
            //    //.ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
            //    //   $"{src.FirstName} {src.LastName}"));

            //    conf.CreateMap<PhoneNumber, PhoneNumberDto>();
            //    conf.CreateMap<Address, AddressDto>();

            //    conf.CreateMap<Alergy, AlergyDto>();
            //    conf.CreateMap<Prescription, PrescriptionDto>();
            //    conf.CreateMap<Medicine, MedicineDto>();
            //    conf.CreateMap<DoctorRef, DoctorRefDto>();

            //    //Patient creation mapping configuration
            //    conf.CreateMap<PatientCreationDto, Patient>();
            //    conf.CreateMap<PhoneNumberCreationDto, PhoneNumber>();
            //    conf.CreateMap<AddressCreationDto, Address>();
            //    conf.CreateMap<AlergyCreationDto, Alergy>();
            //    conf.CreateMap<PrescriptionCreationDto, Prescription>();
            //    conf.CreateMap<MedicineCreationDto, Medicine>();
            //    conf.CreateMap<DoctorCreationDto, DoctorRef>();

            //    //doctor view return mappting
            //    conf.CreateMap<Doctor, DoctorDto>()
            //    .ForMember(dest => dest.Id, ob => ob.MapFrom(src => src._id));
            //    conf.CreateMap<Qualification, QualificationDto>();
            //    conf.CreateMap<PatientRef, PatientRefDto>();

            //    //doctor creation mapping
            //    conf.CreateMap<DoctorCreationDto, Doctor>();
            //    conf.CreateMap<QualificationCreationDto, Qualification>();
            //    conf.CreateMap<PatientCreationDto, PatientRef>();



            //});
            app.UseStaticFiles();
            // 2. Enable authentication middleware
            app.UseAuthentication();
           // app.UseSwagger();

            //// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            //});
            //app.UseHttpCacheHeaders();
            //app.UseResponseCaching();


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });

        }


    }
}
