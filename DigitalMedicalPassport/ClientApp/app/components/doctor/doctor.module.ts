import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProfileComponent } from "./profile/profile.component";
import { AddProfileComponent } from "./add-profile/add-profile.component";
import { EditProfileComponent } from "./edit-profile/edit-profile.component";
import { PatientsComponent } from "./patients/patients.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { MaterialModule } from "../../material/material.module";
import { UiLoadingModule } from "../shared/ui/ui-loading.module";
import { RouterModule } from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule,
    UiLoadingModule,
    RouterModule.forRoot([
      {
        path: "doctor",
        // canActivate: [PatientAuthGuard],
        children: [
          { path: "", component: ProfileComponent },
          { path: "new", component: AddProfileComponent },
          { path: "id/edit", component: EditProfileComponent }
        ]
      }
    ])
  ],
  declarations: [
    ProfileComponent,
    AddProfileComponent,
    EditProfileComponent,
    PatientsComponent
  ]
})
export class DoctorModule {}
