import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  AbstractControl
} from "@angular/forms";

function emailMatcher(c: AbstractControl): { [key: string]: boolean } | null {
  let emailControl: any = c.get("email");
  let confirmControl: any = c.get("confirmEmail");

  if (emailControl.pristine || confirmControl.pristine) {
    return null;
  }

  if (emailControl.value === confirmControl.value) {
    return null;
  }
  return { match: true };
}
@Component({
  selector: "app-add-profile",
  templateUrl: "./add-profile.component.html",
  styleUrls: ["./add-profile.component.css"]
})
export class AddProfileComponent implements OnInit {
  doctorForm: FormGroup;

  get addresses(): FormArray {
    return <FormArray>this.doctorForm.get("addresses");
  }
  get phoneNumber(): FormArray {
    return <FormArray>this.doctorForm.get("phoneNumber");
  }
  get alergy(): FormArray {
    return <FormArray>this.doctorForm.get("alergy");
  }

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.doctorForm = this.fb.group({
      title: ["", [Validators.required]],
      firstName: ["", [Validators.required, Validators.minLength(3)]],
      lastName: ["", [Validators.required, Validators.minLength(3)]],
      gender: ["", Validators.required],
      dob: ["", [Validators.required]],
      emailGroup: this.fb.group(
        {
          email: [
            "",
            [
              Validators.required,
              Validators.pattern("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+")
            ]
          ],
          confirmEmail: ["", Validators.required]
        },
        { validator: emailMatcher }
      ),
      occupation: ["", [Validators.required]],
      sexualOrientation: ["", Validators.required],
      alcohol: "",
      phoneNumber: this.fb.array([this.buildPhoneNumber()]),
      notification: "email",
      sendCatalog: true,
      addresses: this.fb.array([this.buildAddress()]),
      alergy: this.fb.array([this.buildAlergy()]),
      livingStatus: ["", Validators.required],
      socialHistory: ""
    });
  }
  addAddress(): void {
    this.addresses.push(this.buildAddress());
  }
  addPhoneNumber(): void {
    this.phoneNumber.push(this.buildPhoneNumber());
  }
  addAlergy(): void {
    this.alergy.push(this.buildAlergy());
  }
  buildAddress(): FormGroup {
    return this.fb.group({
      addressType: "home",
      street1: "",
      street2: "",
      city: "",
      state: "",
      postal: ""
    });
  }
  buildPhoneNumber(): FormGroup {
    return this.fb.group({
      type: ["", Validators.required],
      number: ["", Validators.required]
    });
  }
  buildAlergy(): FormGroup {
    return this.fb.group({
      type: "",
      description: ""
    });
  }
}
