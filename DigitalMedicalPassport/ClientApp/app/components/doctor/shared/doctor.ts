
export interface IDoctor {
    id: string;
    userId: string;
    firstName: string;
    lastName: string;
    dob: Date;
    email:string;
    confirmEmail: string;
    phoneNumber: IPhoneNumber[];
    timeStamp: string;
    addresses: IAddress[];
    occupation: string;
    sexualOrientation: string;
    livingStatus: string;
    alcohol: string;
    socialHistory: string;
    alergy: IAlergy[];
    prescription: IPrescription[];
    qualification: IQualification[];
    doctors: any[];
    links: any[];
}

export interface IQualification{
    title: string;
    university:string;
    subject:string;
    degree:string;
}
export interface IPhoneNumber {
    type: string;
    number: string;
}

export interface IAddress {
    addressType: string;
    street1: string;
    street2: string;
    city: string;
    state: string;
    postal: string;
}

export interface IAlergy {
    type: string;
    description: string;
}

export interface IMedicine {
    name: string;
    description: string;
}

export interface IPrescription {
    userId: string;
    medicines: IMedicine[];
    comments: string;
}

