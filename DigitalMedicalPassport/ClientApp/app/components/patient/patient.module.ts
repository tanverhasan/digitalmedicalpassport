import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AddProfileComponent } from "./add-profile/add-profile.component";
import { EditProfileComponent } from "./edit-profile/edit-profile.component";
import { ProfileDetailsComponent } from "./profile-details/profile-details.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { HttpModule, RequestOptions, Http } from "@angular/http";
import { AuthHttp, AuthConfig, AUTH_PROVIDERS } from "angular2-jwt";
import { PatientDataService } from "./shared/patient-data.service";
import { PatientAuthGuard } from "../../auth/patient-auth.guard";
import { MaterialModule } from "../../material/material.module";
import { UiLoadingModule } from "../shared/ui/ui-loading.module";

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(
    new AuthConfig({
      // tokenName: 'access_token',
      tokenGetter: (): any => localStorage.getItem("access_token"),
      globalHeaders: [{ "Content-Type": "application/json" }]
    }),
    http,
    options
  );
}

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule,UiLoadingModule,
    RouterModule.forRoot([
      {
        path: "patient",
        canActivate: [PatientAuthGuard],
        children: [
          { path: "", component: ProfileDetailsComponent },
          { path: "new", component: AddProfileComponent },
          { path: "id/edit", component: EditProfileComponent }
        ]
      }
    ])
  ],
  providers: [
    AUTH_PROVIDERS,
    PatientDataService,
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    }
  ],
  declarations: [
    AddProfileComponent,
    EditProfileComponent,
    ProfileDetailsComponent
  ]
})
export class PatientModule {}
