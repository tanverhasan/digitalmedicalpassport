import {
  Component,
  OnInit,
  ElementRef,
  ViewChildren,
  AfterViewInit,
  OnDestroy
} from "@angular/core";
import {
  FormGroup,
  Validators,
  AbstractControl,
  FormBuilder,
  FormArray,
  FormControlName
} from "@angular/forms";

import "rxjs/add/operator/debounceTime";
import { GenericValidator } from "../../shared/generic-validator";
import { Observable } from "rxjs/Rx";
import { PatientDataService } from "../shared/patient-data.service";
import { Router } from "@angular/router";
import { IPatient } from "../shared/patient";


function emailMatcher(c: AbstractControl): { [key: string]: boolean } | null {
  let emailControl: any = c.get("email");
  let confirmControl: any = c.get("confirmEmail");

  if (emailControl.pristine || confirmControl.pristine) {
    return null;
  }

  if (emailControl.value === confirmControl.value) {
    return null;
  }
  return { match: true };
}

@Component({
  selector: "app-add-profile",
  templateUrl: "./add-profile.component.html",
  styleUrls: ["./add-profile.component.css"]
})
export class AddProfileComponent implements OnInit, AfterViewInit, OnDestroy {
  errorMessage: any;
  isLinear = true;
  userid: any;
  patient: IPatient;
  customerForm: any;
  // customer: Customer = new Customer();
  emailMessage: string;
  Gender = [
    "Male",
    "Female",
  ];

  get addresses(): FormArray {
    return <FormArray>this.customerForm.get("addresses");
  }
  get phoneNumber(): FormArray {
    return <FormArray>this.customerForm.get("phoneNumber");
  }
  get alergy(): FormArray {
    return <FormArray>this.customerForm.get("alergy");
  }


  constructor(
    private fb: FormBuilder,
    private http: PatientDataService,
    private router: Router
  ) {

    this.userid = localStorage.getItem("userId");
 
  }

  ngOnInit(): void {
    this.customerForm = this.fb.group({
      title: ["", [Validators.required]],
      firstName: ["", [Validators.required, Validators.minLength(3)]],
      lastName: ["", [Validators.required, Validators.minLength(3)]],
      gender: ["", Validators.required],
      dob: ["", [Validators.required]],
      emailGroup: this.fb.group(
        {
          email: [
            "",
            [
              Validators.required,
              Validators.pattern("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+")
            ]
          ],
          confirmEmail: ["", Validators.required]
        },
        { validator: emailMatcher }
      ),
      occupation: ["", [Validators.required]],
      sexualOrientation: ["", Validators.required],
      alcohol: "",
      phoneNumber: this.fb.array([this.buildPhoneNumber()]),
      notification: "email",
      sendCatalog: true,
      addresses: this.fb.array([this.buildAddress()]),
      alergy: this.fb.array([this.buildAlergy()]),
      livingStatus: ["", Validators.required],
      socialHistory: ""
    });


  }

  ngOnDestroy(): void {
    //  this.http.unsubscribe();
  }
  ngAfterViewInit(): void {
 
  }

  addAddress(): void {
    this.addresses.push(this.buildAddress());
  }
  addPhoneNumber(): void {
    this.phoneNumber.push(this.buildPhoneNumber());
  }
  addAlergy(): void {
    this.alergy.push(this.buildAlergy());
  }
  buildAddress(): FormGroup {
    return this.fb.group({
      addressType: "home",
      street1: "",
      street2: "",
      city: "",
      state: "",
      postal: ""
    });
  }
  buildPhoneNumber(): FormGroup {
    return this.fb.group({
      type: ["", Validators.required],
      number: ["", Validators.required]
    });
  }
  buildAlergy(): FormGroup {
    return this.fb.group({
      type: "",
      description: ""
    });
  }

  save(): void {
      console.log('clicked');
    if (this.customerForm.dirty && this.customerForm.valid) {
      // copy the form values over the product object values
      this.patient.userId = this.userid;
      this.patient.email = this.customerForm.get("emailGroup.email").value;
      console.log(this.patient.email);
      let p = Object.assign({}, this.patient, this.customerForm.value);
      //   this.patient.userId = JSON.stringify(localStorage.getItem("userId"));
      //   console.log(this.patient.email);
      this.http
        .addMedicalProfile(p)
        .subscribe(
          () => this.onSaveComplete(),
          (error: any) => (this.errorMessage = <any>error)
        );
    } else if (!this.customerForm.dirty) {
      this.onSaveComplete();
    }
  }

  onSaveComplete(): void {
    // Reset the form to clear the flags
    this.customerForm.reset();
    this.router.navigate(["/patient"]);
  }
}
  // setMessage(c: AbstractControl): void {
  //   this.emailMessage = "";
  //   if ((c.touched || c.dirty) && c.errors) {
  //     this.emailMessage = Object.keys(c.errors)
  //       .map((key: any) => this.emailValidationMessages[key])
  //       .join(" ");
  //   }
    // setNotification(notifyVia: string): void {
  //    const phoneControl = this.customerForm.get('phoneNumber.number');
  //    if (notifyVia === 'text') {
  //        phoneControl.setValidators(Validators.required);

  //    } else {
  //        phoneControl.clearValidators();
  //    }
  //    phoneControl.updateValueAndValidity();
  //}


