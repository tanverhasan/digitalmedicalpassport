import { Component, OnInit } from "@angular/core";
import { PatientDataService } from "../shared/patient-data.service";
import {} from "rxjs";
import { IPatient } from "../shared/patient";
import { Router } from "@angular/router";

@Component({
  selector: "app-profile-details",
  templateUrl: "./profile-details.component.html",
  styleUrls: ["./profile-details.component.css"]
})
export class ProfileDetailsComponent implements OnInit {
  loader = true;
  profile: IPatient;
  userId: any = localStorage.getItem("userId");
  constructor(private http: PatientDataService, private route: Router) {}

  ngOnInit() {
    //this.http.getMedicalProfile()mo
    //    .subscribe(res => {
    //        this.profile = res as Patient[];
    //        console.log(res);
    //    });
    this.http.getMedicalProfileById(this.userId).subscribe(res => {
      this.profile = res as IPatient;
      this.loader = false;
      console.log(this.profile);
    });
  }

  public onClickAdd() {
    this.route.navigate(["/patient/new"]);
  }
}
