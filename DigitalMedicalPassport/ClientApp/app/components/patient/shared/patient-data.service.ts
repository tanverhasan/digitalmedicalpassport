import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { AuthHttp } from "angular2-jwt";
import { IPatient } from "./patient";
import { Observable } from "rxjs";

@Injectable()
export class PatientDataService  {
    url: string = "http://localhost:5001/api/patient/";
    constructor(private http: AuthHttp) { }


    public getMedicalProfile(): Observable<IPatient[]> {
     return   this.http.get(this.url).map(res=> res.json().data as IPatient[]);
    }

    public getMedicalProfileById(id: string): Observable<IPatient> {
        return this.http.get(this.url +id).map(res => res.json() as IPatient);
    }

    public addMedicalProfile(body: IPatient) {
        var object = JSON.stringify(body);

      return  this.http.post(this.url, body).map(res => {
            return res.json();
        });
    }

    public deleteMedicalProfile(id:string) {
       return this.http.delete(this.url).map(res => res.json());
    }
}


