export interface IPatient {
    id: string;
    userId: string;
    firstName: string;
    lastName: string;
    gender:string;
    dob: Date;
    email:string;
    confirmEmail: string;
    phoneNumber: IPhoneNumber[];
    timeStamp: string;
    addresses: IAddress[];
    occupation: string;
    sexualOrientation: string;
    livingStatus: string;
    alcohol: string;
    socialHistory: string;
    alergy: IAlergy[];
    prescription: IPrescription[];
    doctors: any[];
    links: any[];
}


export interface IPhoneNumber {
    type: string;
    number: string;
}

export interface IAddress {
    addressType: string;
    street1: string;
    street2: string;
    city: string;
    state: string;
    postal: string;
}

export interface IAlergy {
    type: string;
    description: string;
}

export interface IMedicine {
    name: string;
    description: string;
}

export interface IPrescription {
    userId: string;
    medicines: IMedicine[];
    comments: string;
}

