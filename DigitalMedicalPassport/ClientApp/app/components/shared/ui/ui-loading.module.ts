import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LoadingSpinnerRectComponent } from "./loading-spinner-rect/loading-spinner-rect.component";
import { LoadingSpinnerComponent } from "./loading-spinner/loading-spinner.component";
import { LoadingFoldingCubeComponent } from "./loading-folding-cube/loading-folding-cube.component";
import { LoadingFadingCircleComponent } from "./loading-fading-circle/loading-fading-circle.component";
import { LoadingDoubleBounceComponent } from "./loading-double-bounce/loading-double-bounce.component";
import { LoadingDotComponent } from "./loading-dot/loading-dot.component";
import { LoadingCubeGridComponent } from "./loading-cube-grid/loading-cube-grid.component";
import { LoadingCubeComponent } from "./loading-cube/loading-cube.component";
import { LoadingCircleComponent } from "./loading-circle/loading-circle.component";
import { LoadingBounceComponent } from "./loading-bounce/loading-bounce.component";

@NgModule({
  imports: [CommonModule],
  exports: [
    LoadingSpinnerRectComponent,
    LoadingSpinnerComponent,
    LoadingFoldingCubeComponent,
    LoadingFadingCircleComponent,
    LoadingDoubleBounceComponent,
    LoadingDotComponent,
    LoadingCubeGridComponent,
    LoadingCubeComponent,
    LoadingCircleComponent,
    LoadingBounceComponent
  ],
  declarations: [
    LoadingSpinnerRectComponent,
    LoadingSpinnerComponent,
    LoadingFoldingCubeComponent,
    LoadingFadingCircleComponent,
    LoadingDoubleBounceComponent,
    LoadingDotComponent,
    LoadingCubeGridComponent,
    LoadingCubeComponent,
    LoadingCircleComponent,
    LoadingBounceComponent
  ]
})
export class UiLoadingModule {}
