import { Injectable, Inject } from '@angular/core';
import { AuthHttp } from "angular2-jwt";
import { Http } from "@angular/http";

@Injectable()
export class PatientData {

    private readonly endPoint = '/api/Patients';
    private headers = new Headers({ 'Content-Type': 'application/json' });
    constructor( public http: AuthHttp) { }

    getPatient() {
      
        return this.http.get("http://localhost:5001/api/Patient").map(res=> res.json())
           
    }

}
