import { Component, OnInit } from '@angular/core';
import { PatientData } from "../service/data.service";
import { AuthService } from "../../auth/auth.service";
import { Http } from "@angular/http";

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

    constructor(private service: PatientData) { }

    ngOnInit() {
    }
    public adminPing(): void {
        //this.message = '';
        //this.authHttp.post(`${this.API_URL}/admin`, {
        //    headers: new HttpHeaders().set('Authorization', 'Bearer' + localStorage.getItem('access_token')).set('Content-Type', 'application/json')
        //})
        //    .map(res => res)
        //    .subscribe(data => {
        //        this.message = data
        //    },
        //    (err: HttpErrorResponse) => {
        //        if (err.error instanceof Error) {

        //            console.log('An error occurred:', err.error.message);
        //        } else {
        //            console.log(`Backend returned code ${err.status}, body was: ${err.error.error_description}`);
        //            this.message = err.error.error_description;
        //        }
        //    }
       
            
        //    );
        this.service.getPatient().subscribe(res => {
            console.log(res);
        }, error => console.error(error));
        console.log('clicked');
    }
}
