import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';
import { AuthService } from './auth/auth.service';
import { ProfileComponent } from './auth/profile/profile.component';
import { AuthGuard } from "./auth/auth.guard";
import { AdminComponent } from "./components/admin/admin.component";
import { ScopeGuard } from "./auth/scope.guard";
import { AuthAdminGuard } from "./auth/auth-admin.guard";
import { AUTH_PROVIDERS, AuthHttp, AuthConfig } from "angular2-jwt";
import {  PatientData } from "./components/service/data.service";

import { AuthDoctorGuard } from "./auth/auth-doctor.guard";
import { PatientModule } from "./components/patient/patient.module";
import { DoctorModule } from "./components/doctor/doctor.module";
import { BrowserModule } from "@angular/platform-browser";
import { PatientAuthGuard } from "./auth/patient-auth.guard";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./material/material.module";
import { UiLoadingModule } from './components/shared/ui/ui-loading.module';

// Import HttpClientModule from @angular/common/http
//import { HttpClientModule } from '@angular/common/http';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
    return new AuthHttp(new AuthConfig({
        //tokenName: 'access_token',
        tokenGetter: (():any => localStorage.getItem('access_token')),
        globalHeaders: [{ 'Content-Type': 'application/json' }],
    }), http, options);
}

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent,
        ProfileComponent,
        AdminComponent,

    ],
    imports: [
        BrowserModule,
        CommonModule,
        BrowserAnimationsModule,
        HttpModule,
        FormsModule,
        MaterialModule,UiLoadingModule,
        //HttpClientModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent, canActivate: [AuthGuard] },
            { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
            { path: 'admin', component: AdminComponent, canActivate: [AuthAdminGuard] },
            { path: 'fetch-data', component: FetchDataComponent, canActivate: [AuthGuard] },

            { path: '**', redirectTo: 'home' }
        ]),
        PatientModule, DoctorModule
    ],
    providers: [AuthService, AuthGuard, ScopeGuard, AuthAdminGuard, AuthDoctorGuard, PatientAuthGuard, AUTH_PROVIDERS, PatientData, {
        provide: AuthHttp,
        useFactory: authHttpServiceFactory,
        deps: [Http, RequestOptions]
    }]
})
export class AppModuleShared {
}
