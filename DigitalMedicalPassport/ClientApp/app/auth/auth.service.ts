import { Injectable } from "@angular/core";
import { AUTH_CONFIG } from "./auth0-variables";
import { Router } from "@angular/router";
import "rxjs/add/operator/filter";
import * as auth0 from "auth0-js";
import { Observable } from "rxjs";
import { JwtHelper } from "angular2-jwt";

@Injectable()
export class AuthService {
  requestedScopes: string = "openid email profile read:messages write:messages";
  refreshSubscription: any;
  auth0 = new auth0.WebAuth({
    clientID: AUTH_CONFIG.clientID,
    domain: AUTH_CONFIG.domain,
    responseType: "token id_token",
    audience: AUTH_CONFIG.apiUrl,
    redirectUri: AUTH_CONFIG.callbackURL,
    scope: this.requestedScopes
  });

  userProfile: any;
  // private roles: any=[];
  constructor(public router: Router) {}

  public login(): void {
    this.auth0.authorize();
  }

  public handleAuthentication(): void {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        console.log(authResult);
        window.location.hash = "";
        this.setSession(authResult);
        this.router.navigate(["/home"]);
      } else if (err) {
        this.router.navigate(["/home"]);
        console.log(err);
        alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  public getProfile(cb: any): void {
    const accessToken = localStorage.getItem("access_token");
    if (!accessToken) { 
      throw new Error("Access token must exist to fetch profile");
    }

    const self = this;
    this.auth0.client.userInfo(accessToken, (err, profile) => {
      if (profile) {
        self.userProfile = profile;
      }
      cb(err, profile);
    });
  }

  private setSession(authResult: any): void {
    // set the time that the access token will expire at
    const expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );
    const scopes = authResult.scope || this.requestedScopes || "";

    var jwthelper = new JwtHelper();
    var decodedToken = jwthelper.decodeToken(authResult.accessToken);
    // console.log(decodedToken);
    var roles = decodedToken["http://localhost:5000/roles"];
    localStorage.setItem("roles", roles);
    // console.log(this.roles);

    localStorage.setItem("access_token", authResult.accessToken);
    localStorage.setItem("id_token", authResult.idToken);
    localStorage.setItem("email", authResult.idTokenPayload.email);
    localStorage.setItem("expires_at", expiresAt);
    localStorage.setItem("scopes", JSON.stringify(scopes));
    this.scheduleRenewal();
  }

  public isInRole(roleName: string) {
    const roles: any = localStorage.getItem("roles");
    if (roles != null) {
      return roles.indexOf(roleName) > -1;
    }
  }
  public logout(): void {
    // remove tokens and expiry time from localStorage
    localStorage.removeItem("access_token");
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    localStorage.removeItem("scopes");
    localStorage.removeItem("roles");
    // this.roles = [];
    this.unscheduleRenewal();
    // go back to the home route
    this.router.navigate(["/"]);
  }
  public renewToken() {
    this.auth0.renewAuth(
      {
        audience: AUTH_CONFIG.apiUrl,
        redirectUri: "http://localhost:3001/silent",
        usePostMessage: true
      },
      (err, result) => {
        if (err) {
          alert(
            `Could not get a new token using silent authentication (${
              err.error
            }).`
          );
        } else {
          alert(`Successfully renewed auth!`);
          this.setSession(result);
        }
      }
    );
  }

  public scheduleRenewal() {
    if (!this.isAuthenticated()) return;
    this.unscheduleRenewal();

    const expiresAt: any = window.localStorage.getItem("expires_at");

    const source = Observable.of(expiresAt).flatMap(expiresAt => {
      const now = Date.now();

      // use the delay in a timer to
      // run the refresh at the proper time
      return Observable.timer(Math.max(1, expiresAt - Date.now()));
    });

    // once the delay time from above is
    // reached, get a new JWT and schedule
    // additional refreshes
    this.refreshSubscription = source.subscribe(() => {
      this.renewToken();
      this.scheduleRenewal();
    });
  }

  public unscheduleRenewal() {
    if (!this.refreshSubscription) return;
    this.refreshSubscription.unsubscribe();
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt: any = localStorage.getItem("expires_at");
    return new Date().getTime() < expiresAt;
  }
  public userHasScopes(scopes: Array<string>): boolean {
    const grantedScopes: any = localStorage.getItem("scopes");
    console.log("granted scopes", grantedScopes);
    const splitedGrantedScopes = grantedScopes.split(" ");
    return scopes.every(scope => splitedGrantedScopes.includes(scope));
  }
  //public userHasScopes(scopes: Array<string>): boolean {
  //    const grantedScopes: any = localStorage.getItem('scopes').split(' ');
  //    return scopes.every(scope => grantedScopes.includes(scope));
  //}
}
