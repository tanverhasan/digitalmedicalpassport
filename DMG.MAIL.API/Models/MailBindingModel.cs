﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMG.MAIL.API.Models
{
    public class MailBindingModel
    {
        public string from { get; set; }

        public string to { get; set; }
        public string subject { get; set; }
        public string content { get; set; }
        public string htmlContent { get; set; }
    }
}
