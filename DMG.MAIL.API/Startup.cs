﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using NServiceBus;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace DMG.MAIL.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "My API",
                    Version = "v1",
                    Description = "The Doctor and Patient HTTP MAIL / SMS Microservice",
                });
            });

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "https://tanver.eu.auth0.com/";
                options.Audience = "https://api.digitalmedicalpassport.com";

            });
            services.AddAuthorization(options =>
            {

              //  options.AddPolicy(AppPolicy.RequireAdminRole, policy => policy.RequireClaim("http://localhost:5000/roles", "admin"));
               // options.AddPolicy(AppPolicy.RequireDoctorRole, policy => policy.RequireClaim("http://localhost:5000/roles", "doctor"));
               // options.AddPolicy(AppPolicy.RequireUserRole, policy => policy.RequireClaim("http://localhost:5000/roles", "user"));


            });

            //services.AddSingleton(Configuration);
            //var containerBuilder = new ContainerBuilder();
            //containerBuilder.Populate(services);
            //var container = RegisterEventBus(containerBuilder);
            //return new AutofacServiceProvider(container);
        }

        //private IContainer RegisterEventBus(ContainerBuilder containerBuilder)
        //{
        //    IEndpointInstance endpoint = null;
        //    containerBuilder.Register(c => endpoint).As<IEndpointInstance>().SingleInstance();
        //    var container = containerBuilder.Build();

        //    var endpointConfiguration = new EndpointConfiguration("endpoint2");
        //    var transport = endpointConfiguration.UseTransport<LearningTransport>();
        //    endpointConfiguration.UsePersistence<LearningPersistence>();
        //    endpointConfiguration.UseSerialization<JsonSerializer>();
        //    endpoint = Endpoint.Start(endpointConfiguration).GetAwaiter().GetResult();
        //    return container;
        //}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            //if (env.IsProduction() || env.IsStaging() || env.IsEnvironment("Staging_2"))
            //{
            //    app.UseExceptionHandler("/Error");
            //}
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseMvcWithDefaultRoute();
        }

    }
}
