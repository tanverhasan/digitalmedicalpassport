﻿using DMG.MAIL.API.Models;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMG.MAIL.API.Services
{
    public class ServiceBusReceive
    {

        private readonly IConfiguration _configuration;
        private readonly IQueueClient _queueClient;
        private readonly ILogger<ServiceBusReceive> logger;

        public ServiceBusReceive(IConfiguration configuration, ILogger<ServiceBusReceive> _logger)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _queueClient = new QueueClient(_configuration["AzureServiceBus:connectionString"], _configuration["AzureServiceBus:queueName"]);
            logger = _logger ?? throw new ArgumentNullException(nameof(_logger));
        }

        public async Task ProcessReceiveMessage(Message data, CancellationToken token)
        {
            try
            {
                var mail = JsonConvert.DeserializeObject<MailBindingModel>(Encoding.UTF8.GetString(data.Body));
                var key = this._configuration.GetValue<string>("SendGrid_API_KEY");
                var client = new SendGridClient(key);

                var from = new EmailAddress(mail.from, "Example User");
                var subject = "Hello World from the SendGrid CSharp Library Helper!";
                var to = new EmailAddress(mail.to, "Example User");
                var plainTextContent = mail.content;
                var htmlContent = "<strong>Hello, Email from the helper! [SendSingleEmailAsync]</strong>";
                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);

                var response = await client.SendEmailAsync(msg);
                await _queueClient.CompleteAsync(data.SystemProperties.LockToken);
        
            }
            catch (Exception ex)
            {

                logger.LogError(ex.Message);
            }

        }
    }
}
