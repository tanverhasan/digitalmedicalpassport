﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DMG.MAIL.API.Controllers
{
    [Produces("application/json")]
    [Route("api/SMS")]
    public class SMSController : Controller
    {
        // GET: api/SMS
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/SMS/5
        //[HttpGet("{id}", Name = "GetById")]
        //public string Get(int id)
        //{
        //    return "value";
        //}
        
        // POST: api/SMS
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/SMS/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
