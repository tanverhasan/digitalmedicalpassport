﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DMG.MAIL.API.Models;
using SendGrid.Helpers.Mail;
using SendGrid;
using Microsoft.Extensions.Configuration;

namespace DMG.MAIL.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Mail")]
    public class MailController : Controller
    {
       private  readonly IConfiguration configuration;

        public MailController(IConfiguration _configuration)
        {
            this.configuration = _configuration;
        }
        // GET: api/Mail
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Mail/5
        //[HttpGet("{id}", Name = "GetById")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/Mail
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]MailBindingModel mail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var key = this.configuration.GetValue<string>("SendGrid_API_KEY");
            var client = new SendGridClient(key);

            var from = new EmailAddress(mail.from, "Example User");
            var subject = "Hello World from the SendGrid CSharp Library Helper!";
            var to = new EmailAddress(mail.to, "Example User");
            var plainTextContent = mail.content;
            var htmlContent = "<strong>Hello, Email from the helper! [SendSingleEmailAsync]</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);

            var response = await client.SendEmailAsync(msg);
            //Console.WriteLine(msg.Serialize());
            //Console.WriteLine(response.StatusCode);
           

            //var client = new SendGridClient(key);
            //var response = await client.SendEmailAsync(msg);
            return Ok(response.StatusCode);
            
        }
        
        // PUT: api/Mail/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
           
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
