﻿using DigitalMedicalPassport.API.Middleware;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.Extensions
{
    public static class NotImplementedExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseNotImplementedExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<NotImplementedExceptionMiddleware>();
        }
    }
}
