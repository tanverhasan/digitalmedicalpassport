﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using DigitalMedicalPassport.API.Policy;
using Microsoft.AspNetCore.Mvc.Formatters;
using Swashbuckle.AspNetCore.Swagger;
using ApplicationCore.Interface;
using ApplicationCore.Implementation;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using ApplicationCore.Entities;
using DigitalMedicalPassport.API.DTO;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DigitalMedicalPassport.API.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using DigitalMedicalPassport.API.Extensions;

namespace DigitalMedicalPassport.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowSpecificOrigin"));
            });
            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin",
                    builder => builder.WithOrigins(Configuration["Client:url"]).AllowAnyHeader()
                    .AllowAnyMethod());
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "Digital Medical Passport",
                    Version = "v1",
                    Description = "Degital Medical Passport project API endpoint Documentation",
                    Contact = new Contact { Name = "Tanver Hasan", Email = "B00316640@studentmail.uws.ac.uk" }
                });
                var basePath = AppContext.BaseDirectory;
                var xmlPath = System.IO.Path.Combine(basePath, "DigitalMedicalPassport.API.xml");
                c.IncludeXmlComments(xmlPath);
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "https://tanver.eu.auth0.com/";
                options.Audience = "https://api.digitalmedicalpassport.com";

            });
            services.AddAuthorization(options =>
            {

                options.AddPolicy(AppPolicy.RequireAdminRole, policy => policy.RequireClaim(Configuration["Client:roleUrl"], "admin")); 
                options.AddPolicy(AppPolicy.RequireDoctorRole, policy => policy.RequireClaim(Configuration["Client:roleUrl"], "doctor"));
                options.AddPolicy(AppPolicy.RequireUserRole, policy => policy.RequireClaim(Configuration["Client:roleUrl"], "user"));
                
                
            });
            services.AddTransient<IPatientRepository, PatientRepository>();
            services.AddTransient<IDoctorRepository, DoctorRepository>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton(Configuration);
            services.AddSingleton<IServiceBusSend, ServiceBusSend>();
            services.AddScoped<IUrlHelper, UrlHelper>(implementationFactory =>
            {
                var actionContext = implementationFactory.GetService<IActionContextAccessor>().ActionContext;
                return new UrlHelper(actionContext);
            });
            services.AddResponseCaching();
            services.AddMvc(action =>
            {
                action.ReturnHttpNotAcceptable = true;
                action.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
                action.InputFormatters.Add(new XmlDataContractSerializerInputFormatter());
                // action.Filters.Add(typeof(HttpGlobalExceptionFilter));
            }).AddControllersAsServices();


            services.AddDistributedRedisCache(options =>
            {
                options.Configuration = Configuration["Redis:connectionString"];
                options.InstanceName = "RedisCahceServer";
            });
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
            });
            // var containerBuilder = new ContainerBuilder();
            //containerBuilder.Populate(services);
            //var container = RegisterEventBus(containerBuilder);
            //// return new AutofacServiceProvider(container);
        }

        //private IContainer RegisterEventBus(ContainerBuilder containerBuilder)
        //{
        //    IEndpointInstance endpoint = null;
        //    containerBuilder.Register(c => endpoint).As<IEndpointInstance>().SingleInstance();
        //    var container = containerBuilder.Build();

        //    var endpointConfiguration = new EndpointConfiguration("endpoint1");
        //    var transport = endpointConfiguration.UseTransport<LearningTransport>();
        //    endpointConfiguration.UsePersistence<LearningPersistence>();
        //    endpointConfiguration.UseSerialization<JsonSerializer>();
        //    endpoint = Endpoint.Start(endpointConfiguration).GetAwaiter().GetResult();
        //    return container;
        //}



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            loggerFactory.AddConsole();
            loggerFactory.AddDebug(LogLevel.Information);
            app.UseResponseCaching();
            app.Use(async (context, next) =>
            {
                context.Response.GetTypedHeaders().CacheControl = new CacheControlHeaderValue()
                {
                    Public = true,
                    MaxAge = TimeSpan.FromSeconds(10)
                };
                context.Response.Headers[HeaderNames.Vary] = new string[] { "Accept-Encoding" };

                await next();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            // Shows UseCors with named policy.
            // app.UseCors("AllowSpecificOrigin");
            AutoMapper.Mapper.Initialize(conf =>
            {
                conf.CreateMap(typeof(Task<>), typeof(Task<>));
                //Patient maping configuration
                conf.CreateMap<Patient, PatientDto>()
                .ForMember(dest => dest.Id, ob => ob.MapFrom(src => src._id));
                //.ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
                //   $"{src.FirstName} {src.LastName}"));

                conf.CreateMap<PhoneNumber, PhoneNumberDto>();
                conf.CreateMap<ApplicationCore.Entities.Address, AddressDto>();

                conf.CreateMap<Alergy, AlergyDto>();
                conf.CreateMap<Prescription, PrescriptionDto>();
                conf.CreateMap<Qualification, QualificationDto>();
                conf.CreateMap<Medicine, MedicineDto>();
                conf.CreateMap<DoctorRef, DoctorRefDto>();

                //Patient return view

                conf.CreateMap<Patient, PatientMinInfoDto>().ForMember(des => des.Id, src => src.MapFrom(p => p._id));
                //Patient creation mapping configuration
                conf.CreateMap<PatientCreationDto, Patient>();
                conf.CreateMap<PhoneNumberCreationDto, PhoneNumber>();
                conf.CreateMap<AddressCreationDto, ApplicationCore.Entities.Address>();
                conf.CreateMap<AlergyCreationDto, Alergy>();
                conf.CreateMap<PrescriptionCreationDto, Prescription>();
                conf.CreateMap<MedicineCreationDto, Medicine>();
                //conf.CreateMap<DoctorCreationDto, DoctorRef>();
                conf.CreateMap<DoctorRefCreationDto, DoctorRef>();
                conf.CreateMap<DoctorRefDto, DoctorRef>();


                //doctor view return mappting
                conf.CreateMap<Doctor, DoctorDto>()
                .ForMember(dest => dest.Id, ob => ob.MapFrom(src => src._id));
                conf.CreateMap<Doctor, DoctorMinInfoDto>()
            .ForMember(dest => dest.Id, ob => ob.MapFrom(src => src._id));
                conf.CreateMap<PatientRef, PatientRefDto>();

                conf.CreateMap<DoctorDoctorRef, DoctorDoctorRefDto>();

                //doctor creation mapping
                conf.CreateMap<DoctorCreationDto, Doctor>();
                conf.CreateMap<QualificationCreationDto, Qualification>();
                conf.CreateMap<PatientCreationDto, PatientRef>();
                conf.CreateMap<DoctorPatientRef, PatientRef>();
            });
            // Shows UseCors with named policy.
            app.UseCors("AllowSpecificOrigin");

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseSession();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            //HTTP Response Caching
            app.UseETagger();

            app.UseNotImplementedExceptionMiddleware();
            app.UseMvcWithDefaultRoute();
        }
    }
}
