﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApplicationCore.Entities;
using DigitalMedicalPassport.API.DTO;
using DigitalMedicalPassport.API.Helper;
using AutoMapper;
using ApplicationCore.Interface;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using DigitalMedicalPassport.API.Policy;

namespace DigitalMedicalPassport.API.Controllers
{
    
    [Authorize]
    [Produces("application/json")]
    [Route("api/DoctorDoctor")]
   // [Authorize(AppPolicy.RequireDoctorRole)]
    public class DoctorDoctorController : Controller
    {
        IDoctorRepository repository;
        private readonly ILogger<DoctorController> logger;
        private readonly IUrlHelper urlHlper;
        public DoctorDoctorController(IDoctorRepository _repository, ILogger<DoctorController> _logger, IUrlHelper _urlHelper)
        {
            this.repository = _repository ?? throw new ArgumentNullException(nameof(_repository));
            this.logger = _logger ?? throw new ArgumentNullException(nameof(_logger));
            this.urlHlper = _urlHelper ?? throw new ArgumentNullException(nameof(_logger));
        }
        // GET: api/DoctorDoctor
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/DoctorDoctor/5
        //[HttpGet("{id}", Name = "GetById")]
        //public async Task<IActionResult> Get(string id)
        //{
        //    if (id != null)
        //    {
        //        var total = await this.repository.getTotalDoctors(id);
        //        return Ok(total);
        //    }
        //    return BadRequest();

        //}
        /// <summary>
        /// This API Endpoint is used to persis doctor to doctor profile. Because each doctor can see other doctor as patient
        /// </summary>
        /// <param name="model"> Doctor creation model</param>
        /// <returns></returns>

        // POST: api/DoctorDoctor
        [HttpPost]
        public IActionResult Post([FromBody]DoctorDoctorCreationDto model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }
            try
            {
                var map = Mapper.Map<DoctorDoctorRef>(model);
                repository.AddDoctorToDoctorProfile(map, model.UserId);
                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }
        }
        /// <summary>
        /// This API Endpoint is responsible for update doctor reference in doctor profile
        /// </summary>
        /// <param name="id"> Doctor Id</param>
        /// <param name="value"> Doctor reference creation model</param>
        // PUT: api/DoctorDoctor/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]DoctorDoctorCreationDto value)
        {
        }
        /// <summary>
        /// This API Endpoint is responsible to delete doctor reference from doctor profile
        /// </summary>
        /// <param name="id"></param>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
