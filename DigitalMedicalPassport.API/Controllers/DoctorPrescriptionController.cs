﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DigitalMedicalPassport.API.DTO;
using DigitalMedicalPassport.API.Helper;
using AutoMapper;
using ApplicationCore.Entities;
using ApplicationCore.Interface;
using Microsoft.Extensions.Logging;
using DigitalMedicalPassport.API.Service;
using DigitalMedicalPassport.API.Models;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using System.Text;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.AspNetCore.Authorization;
using DigitalMedicalPassport.API.Policy;

namespace DigitalMedicalPassport.API.Controllers
{
    
    [Authorize]
    [Produces("application/json")]
    [Route("api/DoctorPrescription")]
    //[Authorize(AppPolicy.RequireDoctorRole)]
    public class DoctorPrescriptionController : Controller
    {
        private readonly IDoctorRepository _repo;
        private readonly ILogger<DoctorPrescriptionController> logger;
        private readonly IUrlHelper urlHlper;
        private readonly IServiceBusSend serviceBus;
        private readonly IDistributedCache cache;
        public DoctorPrescriptionController(IDoctorRepository repo, ILogger<DoctorPrescriptionController> logger, IUrlHelper urlHelper, IServiceBusSend serviceBus, IDistributedCache cache)
        {
            this._repo = repo ?? throw new ArgumentNullException(nameof(repo));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.urlHlper = urlHelper ?? throw new ArgumentNullException(nameof(urlHelper));
            this.serviceBus = serviceBus ?? throw new ArgumentException(nameof(serviceBus));
            this.cache = cache ?? throw new ArgumentException(nameof(cache));
        }
        // GET: api/DoctorPrescription
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/DoctorPrescription/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        /// <summary>
        /// This API Endpoint is responsible for prescription creation 
        /// </summary>
        /// <param name="prescriptionCreationDto"> Prescription creation model</param>
        /// <returns></returns>

        // POST: api/DoctorPrescription
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PrescriptionCreationDto prescriptionCreationDto)
        {
            if (prescriptionCreationDto == null)
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }
            try
            {
                var model = Mapper.Map<Prescription>(prescriptionCreationDto);
                var result = await this._repo.AddPrescription(model, prescriptionCreationDto.UserId);
                if (result)
                {
                    var user = this._repo.GetById(prescriptionCreationDto.UserId).Result;
                    var mail = new MailBindingModel
                    {
                        from = "t.h.aia@outlook.com",
                        to = user.Email,
                        subject = "New Prescription",
                        content = "A prescription is added to your profile."
                    };
                    var message = new Message(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(mail)));
                    await this.serviceBus.sendMessage(message);
                    var key = $"dmp:{prescriptionCreationDto.UserId}:doctorProfile";
                    var cacheValue = this.cache.Get(key);

                    if (cacheValue != null)
                    {
                        // var cacheResult = JsonConvert.DeserializeObject<DoctorDto>(Encoding.UTF8.GetString(value));
                        // return Ok(cacheResult);
                        await this.cache.RemoveAsync(key);
                    }
                }
                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// This API Endpoint is responsible for Update  prescription in patient profile 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        // PUT: api/DoctorPrescription/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        /// <summary>
        /// This API Endpoint is responsible to delete prescription from patient profile
        /// </summary>
        /// <param name="id"></param>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
