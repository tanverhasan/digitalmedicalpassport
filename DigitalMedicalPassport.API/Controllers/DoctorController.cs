﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApplicationCore.Interface;
using DigitalMedicalPassport.API.Helper;
using DigitalMedicalPassport.API.DTO;
using ApplicationCore.Helper;
using AutoMapper;
using Microsoft.Extensions.Logging;
using ApplicationCore.Entities;
using DigitalMedicalPassport.API.Service;
using System.Text;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using DigitalMedicalPassport.API.Policy;

namespace DigitalMedicalPassport.API.Controllers
{
      
    [Authorize]
    [Produces("application/json")]
    [Route("api/Doctor")]
   // [Authorize(AppPolicy.RequireDoctorRole)]
    public class DoctorController : Controller
    {

        IDoctorRepository repository;
        private readonly ILogger<DoctorController> logger;
        private readonly IUrlHelper urlHlper;
        private readonly IServiceBusSend serviceBusSend;
        private readonly IDistributedCache cache;
        public DoctorController(IDoctorRepository _repository, ILogger<DoctorController> _logger, IUrlHelper _urlHelper, IServiceBusSend _serviceBusSend, IDistributedCache cache)
        {
            this.repository = _repository ?? throw new ArgumentNullException(nameof(_repository));
            this.logger = _logger ?? throw new ArgumentNullException(nameof(_logger));
            this.urlHlper = _urlHelper ?? throw new ArgumentNullException(nameof(_urlHelper));
            this.serviceBusSend = _serviceBusSend ?? throw new ArgumentNullException(nameof(_serviceBusSend));
            this.cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }

        /// <summary>
        ///  This Api Endpoint can used to find all doctors from the databse. It support filter by first name. For the purpose of pagination it take PageNumber and page size. By default results or orderd by first name
        /// . Search query param used to  establish search functionality. It searchs in all common field. Forexample, first name , last name, email, date of birth, area of work , city and country
        /// </summary>
        /// 
        /// <returns>List of Doctors</returns>
        // GET: api/Doctors
        [AllowAnonymous]
        [HttpGet(Name = "GetDoctors")]
        public async Task<IActionResult> Get(DoctorResourceParameter resourcePrameter)
        {
            try
            {
                var result = await repository.GetAll(resourcePrameter);

                if (result == null)
                {
                    return NotFound();
                }
                var previousPageLink = result.HasPrevious ? CreateResourceUri(resourcePrameter, ResourceUriType.PreviousPage) : null;

                var nextPageLink = result.HasNext ? CreateResourceUri(resourcePrameter, ResourceUriType.NextPage) : null;

                var paginationMetaData = new
                {
                    totalCount = result.TotalCount,
                    pageSize = result.PageSize,
                    currentPage = result.CurrentPage,
                    totalPages = result.TotalPage,
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink
                };
                Response.Headers.Add("X-Pagination", Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetaData));

                var doctors = Mapper.Map<IEnumerable<DoctorMinInfoDto>>(result);
                return Ok(doctors);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }

        }
        private string CreateResourceUri(DoctorResourceParameter resourceParameter, ResourceUriType resourceUriType)
        {
            switch (resourceUriType)
            {
                case ResourceUriType.PreviousPage:
                    return urlHlper.Link("GetDoctors", new
                    {
                        orderBy = resourceParameter.OrderByFirstName,
                        searchQuery = resourceParameter.SearchQuery,
                        firstName = resourceParameter.FirstName,
                        pageNumber = resourceParameter.PageNumber - 1,
                        pageSize = resourceParameter.PageSize
                    });
                case ResourceUriType.NextPage:
                    return urlHlper.Link("GetDoctors", new
                    {
                        orderBy = resourceParameter.OrderByFirstName,
                        searchQuery = resourceParameter.SearchQuery,
                        firstName = resourceParameter.FirstName,
                        pageNumber = resourceParameter.PageNumber + 1,
                        pageSize = resourceParameter.PageSize
                    });
                default:
                    return urlHlper.Link("GetDoctors", new
                    {
                        orderBy = resourceParameter.OrderByFirstName,
                        searchQuery = resourceParameter.SearchQuery,
                        firstName = resourceParameter.FirstName,
                        pageNumber = resourceParameter.PageNumber,
                        pageSize = resourceParameter.PageSize
                    });
            }
        }
        /// <summary>
        /// This API endpoint returns single doctor from the database. It take doctor id as argument
        /// 
        /// </summary>
        /// <param name="id"> Doctor Id </param>
        /// <returns> Returns Doctor profile</returns>
        // GET: api/Doctors/5
        [HttpGet("{id}", Name = "GetDoctorById")]
        public async Task<IActionResult> Get(string id)
        {
            var user = HttpContext.User.IsInRole("doctor");
            if (id == null)
            {
                return BadRequest();
            }

            try
            {
                var key = $"dmp:{id}:doctorProfile";
                var value = this.cache.Get(key);
                if (value != null)
                {
                    var result = JsonConvert.DeserializeObject<DoctorDto>(Encoding.UTF8.GetString(value));
                    return Ok(result);
                }



                if (repository.IsExist(id).Result)
                {
                    var result = await repository.GetById(id);

                    if (result == null)
                    {
                        return NotFound();
                    }
                    var doctor = Mapper.Map<DoctorDto>(result);

                    await this.cache.SetAsync(key, Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(doctor)));
                    // var message = new Message(Encoding.UTF8.GetBytes("Hello"));
                    // await serviceBusSend.sendMessage(message);
                    return Ok(doctor);
                }
                return NotFound();

                // return Ok(value);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }

        }
        /// <summary>
        /// This API Endpoint persist doctor 
        /// </summary>
        /// <param name="model"> Doctor Creation Model</param>
        /// <returns> Returns HTTP Status code 200</returns>
        // POST: api/Doctors
        [HttpPost]
        public IActionResult Post([FromBody]DoctorCreationDto model)
        {
            try
            {
                if (model == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }
                if (!repository.IsExist(model.UserId).Result)
                {
                    var doctor = Mapper.Map<Doctor>(model);
                    repository.Insert(doctor);
                    return Ok();
                }


                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }

        }




        /// <summary>
        /// This API Endpoint is used to update doctor profile information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// 
        // PUT: api/Doctors/5
        [HttpPut("{id}", Name = "UpdateProfile")]
        [ActionName("UpdateProfile")]
        public async Task<IActionResult> UpdateProfile(string id, [FromBody]DoctorCreationDto model)
        {
            try
            {
                if (model == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }
                if (repository.IsExist(model.UserId).Result)
                {
                    var doctor = Mapper.Map<Doctor>(model);
                    var insert = await repository.Update(id, doctor);
                    if (insert)
                    {
                        var key = $"dmp:{id}:doctorProfile";
                        var value = this.cache.RemoveAsync(key);

                        return Ok();
                    }

                }


                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }
        }
        [HttpPut("image/{id}", Name = "SaveProfileImage")]
        [ActionName("SaveProfileImage")]
        public async Task<IActionResult> SaveProfileImage(string id, [FromBody]PImage model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            if (this.repository.IsExist(id).Result)
            {
                try
                {
                    var result = await this.repository.UpdateImageUri(id, model.ImageUri);
                    if (result)
                    {
                        var key = $"dmp:{id}:doctorProfile";
                        var value = this.cache.RemoveAsync(key);
                    }
                    return Ok();

                }
                catch (Exception ex)
                {
                    logger.LogError(ex.Message);
                    return BadRequest();
                }
            }
            return NotFound();
        }

        /// <summary>
        /// This API Endpoint is responsible for deleting doctor profile
        /// </summary>
        /// <param name="id">Doctor Id</param>
        /// <returns></returns>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            try
            {
                if (repository.IsExist(id).Result)
                {

                    var result = repository.Delete(id);
                    if (result.Result)
                    {
                        var key = $"dmp:{id}:doctorProfile";
                        var value = this.cache.RemoveAsync(key);
                        return Ok();
                    }

                }
                return NotFound();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }
        }

        public class PImage
        {
            public string ImageUri { get; set; }
        }
    }
}