﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DigitalMedicalPassport.API.DTO;
using ApplicationCore.Interface;
using Microsoft.Extensions.Logging;
using AutoMapper;
using ApplicationCore.Entities;
using DigitalMedicalPassport.API.Helper;
using System.Net.Http;
using AutoMapper.Configuration;
using DigitalMedicalPassport.API.Service;
using DigitalMedicalPassport.API.Models;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Azure.ServiceBus;
using Microsoft.AspNetCore.Cors;

namespace DigitalMedicalPassport.API.Controllers
{
    [Produces("application/json")]
    [Route("api/PatientPrescription")]
    public class PatientPrescriptionController : Controller
    {
        private readonly IPatientRepository _repo;
        private readonly ILogger<PatientController> logger;
        private readonly IUrlHelper urlHlper;
       // private readonly IConfiguration configuration;
        private readonly IServiceBusSend serviceBus;
        public PatientPrescriptionController(IPatientRepository repo, ILogger<PatientController> _logger, IUrlHelper _urlHelper, IConfiguration _configuration, IServiceBusSend serviceBus)
        {
            this._repo = repo ?? throw new ArgumentNullException(nameof(repo));
            this.logger = _logger ?? throw new ArgumentNullException(nameof(_logger));
            this.urlHlper = _urlHelper ?? throw new ArgumentNullException(nameof(_urlHelper));
           // this.configuration = _configuration ?? throw new ArgumentNullException(nameof(_configuration));
            this.serviceBus = serviceBus ?? throw new ArgumentNullException(nameof(serviceBus));
        }
        // GET: api/PatientPrescription
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/PatientPrescription/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}
        
        // POST: api/PatientPrescription
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PrescriptionCreationDto value)
        {
            if (value ==null)
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }
            try
            {
                var model = Mapper.Map<Prescription>(value);
               var result= await _repo.InsertPrescription(model, value.UserId);
                if (result)
                {
                    var user = this._repo.GetById(value.UserId).Result;
                    var mail = new MailBindingModel
                    {
                        from = "t.h.aia@outlook.com",
                        to = user.Email,
                        subject = "New Prescription",
                        content = "A prescription is added to your profile."
                    };
                    var message= new Message(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(mail)));
                    await this.serviceBus.sendMessage(message);
                   
                }
                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
                
            }
        }
        
        // PUT: api/PatientPrescription/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
