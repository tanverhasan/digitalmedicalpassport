﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Interface;
using DigitalMedicalPassport.API.Policy;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DigitalMedicalPassport.API.Controllers
{
    
    
    [Authorize]
    [Produces("application/json")]
    [Route("api/Admin")]
  //  [Authorize(AppPolicy.RequireAdminRole)]
    public class AdminController : Controller
    {
        public readonly IPatientRepository patientRepository;
        public readonly IDoctorRepository doctorRepository;
        private readonly ILogger<AdminController> logger;
        public AdminController(IPatientRepository patientRepository, IDoctorRepository doctorRepository, ILogger<AdminController> logger)
        {
            this.doctorRepository = doctorRepository ?? throw new ArgumentException(nameof(doctorRepository));
            this.patientRepository = patientRepository ?? throw new ArgumentException(nameof(patientRepository));
            this.logger = logger ?? throw new ArgumentException(nameof(logger));
        }
        // GET: api/Admin
        [HttpGet("Patient/GroupByGender", Name = "Patient_GroupByGender")]
        [ActionName("Patient_GroupByGender")]
        public async Task<IActionResult> Patient_GroupByGender()
        {
            try
            {
                var query = await this.patientRepository.groupByGender();
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);
            }
            return NoContent();
        }

        // GET: api/Admin
        [HttpGet("Patient/GroupBySexualOrientation", Name = "Patient_GroupBySexualOrientation")]
        [ActionName("Patient_GroupBySexualOrientation")]
        public async Task<IActionResult> Patient_GroupBySexualOrientation()
        {
            try
            {
                var query = await this.patientRepository.groupBySexualOrientation();
                return Ok(query);
            }
            catch (Exception ex)
            {

                logger.LogError(ex.Message);
            }
            return NoContent();

        }

        // GET: api/Admin
        [HttpGet("Patient/GroupByCity/{country}", Name = "Patient_GroupByCity")]
        [ActionName("Patient_GroupByCity")]
        public async Task<IActionResult> Patient_GroupByCity(string country)
        {
            try
            {
                var query = await this.patientRepository.groupByCity("Bangladesh");
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);

            }
            return NoContent();
        }

        [HttpGet("Patient/GroupByCountry", Name = "Patient_GroupByCountry")]
        [ActionName("Patient_GroupByCountry")]
        public async Task<IActionResult> Patient_GroupByCountry()
        {
            try
            {
                var query = await this.patientRepository.groupByCountry();
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);
            }
            return NoContent();
        }

        [HttpGet("Patient/RegistrationMonthlyView/{year}", Name = "Patient_RegistrationMonthlyView")]
        [ActionName("Patient_RegistrationMonthlyView")]
        public async Task<IActionResult> Patient_RegistrationMonthlyView(int year)
        {
            try
            {
                var query = await this.patientRepository.getByMonth(year);
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);
            }
            return NoContent();
        }





        // GET: api/Admin
        [HttpGet("Doctor/GroupByGender", Name = "Doctor_GroupByGender")]
        [ActionName("Doctor_GroupByGender")]
        public async Task<IActionResult> Doctor_GroupByGender()
        {
            try
            {
                var query = await this.doctorRepository.groupByGender();
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);
            }
            return NoContent();
        }

        // GET: api/Admin
        [HttpGet("Doctor/GroupBySexualOrientation", Name = "Doctor_GroupBySexualOrientation")]
        [ActionName("Doctor_GroupBySexualOrientation")]
        public async Task<IActionResult> Doctor_GroupBySexualOrientation()
        {
            try
            {
                var query = await this.doctorRepository.groupBySexualOrientation();
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);
            }
            return NoContent();

        }

        // GET: api/Admin
        [HttpGet("Doctor/GroupByCity/{country}", Name = "Doctor_GroupByCity")]
        [ActionName("Doctor_GroupByCity")]
        public async Task<IActionResult> Doctor_GroupByCity(string country)
        {
            try
            {
                var query = await this.doctorRepository.groupByCity("Bangladesh");
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);
            }
            return NoContent();
        }

        [HttpGet("Doctor/GroupByCountry", Name = "Doctor_GroupByCountry")]
        [ActionName("Doctor_GroupByCountry")]
        public async Task<IActionResult> Doctor_GroupByCountry()
        {
            try
            {
                var query = await this.doctorRepository.groupByCountry();
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);
            }
            return NoContent();
        }

        [HttpGet("Patient/Count", Name = "Patient_Count")]
        [ActionName("Patient_Count")]
        public async Task<IActionResult> Patient_Count()
        {
            try
            {
                var query = await this.patientRepository.CountPatient();
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);
            }
            return NoContent();
        }
        [HttpGet("Doctor/Count", Name = "Doctor_Count")]
        [ActionName("Doctor_Count")]
        public async Task<IActionResult> Doctor_Count()
        {
            try
            {
                var query = await this.doctorRepository.CountDoctor();
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);
            }
            return NoContent();
        }

        [HttpGet("Doctor/RegistrationMonthlyView/{year}", Name = "Doctor_RegistrationMonthlyView")]
        [ActionName("Doctor_RegistrationMonthlyView")]
        public async Task<IActionResult> Doctor_RegistrationMonthlyView(int year)
        {
            try
            {
                var query = await this.doctorRepository.getByMonth(year);
                return Ok(query);
            }
            catch (Exception ex)
            {

                this.logger.LogError(ex.Message);
            }
            return NoContent();
        }
        // GET: api/Admin/5
        //[HttpGet("{id}", Name = "GetById")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/Admin
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Admin/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
