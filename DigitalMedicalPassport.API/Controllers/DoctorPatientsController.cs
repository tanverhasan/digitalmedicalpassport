﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApplicationCore.Interface;
using Microsoft.Extensions.Logging;
using ApplicationCore.Entities;
using DigitalMedicalPassport.API.DTO;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Distributed;
using DigitalMedicalPassport.API.Policy;

namespace DigitalMedicalPassport.API.Controllers
{
    
    [Authorize]
    [Produces("application/json")]
    [Route("api/DoctorPatients")]
    //[Authorize(AppPolicy.RequireDoctorRole)]
    public class DoctorPatientsController : Controller
    {
        IDoctorRepository repository;
        private readonly ILogger<DoctorPatientsController> logger;
        private readonly IUrlHelper urlHlper;
        private readonly IDistributedCache cache;
        public DoctorPatientsController(IDoctorRepository _repository, ILogger<DoctorPatientsController> _logger, IUrlHelper _urlHelper, IDistributedCache cache)
        {
            this.repository = _repository ?? throw new ArgumentNullException(nameof(_repository));
            this.logger = _logger ?? throw new ArgumentNullException(nameof(_logger));
            this.urlHlper = _urlHelper ?? throw new ArgumentNullException(nameof(_urlHelper));
            this.cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }
        // GET: api/DoctorPatients
        //[HttpGet]
        //public async Task<IActionResult> Get(string id)
        //{
        //    var result = await repository.getTotalPatients(id);
        //    return Ok(result);
        // }
        /// <summary>
        /// This Endpoint is used to find each patient profile exist in doctor supervision
        /// </summary>
        /// <param name="id"> Patient Id </param>
        /// <returns></returns>

        // GET: api/DoctorPatients/5
        [HttpGet("{id}", Name = "GetById")]
        public IActionResult Get(string id)
        {
            try
            {
                if (id == null)
                {
                    return BadRequest();
                }
                var result = this.repository.getPatients(id);
                return Ok(result.Result);

            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }

        }

        /// <summary>
        /// 
        /// This API Endpoint is responsible to add patient in doctor profile
        /// </summary>
        /// <param name="pref"> Patient creation model</param>
        /// <returns></returns>

        // POST: api/DoctorPatients
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] DoctorPatientRef pref)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }
                var refModel = Mapper.Map<PatientRef>(pref);
                var result = await repository.addPatient(refModel, pref.DoctorId);
                if (result)
                {
                    var key = $"dmp:{pref.DoctorId}:doctorProfile";
                    var value = this.cache.RemoveAsync(key);
                    return Ok();
                }
                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// This API Endpoint is responsible to update patint profile in doctors database
        /// </summary>
        /// <param name="id"> Doctor Id</param>
        /// <param name="model">Patient cration model</param>
        /// <returns></returns>

        // PUT: api/DoctorPatients/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody]DoctorPatientParam model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                var result = await repository.AddAuthorisation(id, model.PatientId, model.status);
                if (result)
                {

                    var key = $"dmp:{id}:doctorProfile";
                    var value = this.cache.RemoveAsync(key);

                    return Ok();
                }
                return Ok();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();

            }
        }

        /// <summary>
        /// This Api Endpoint is used to delete patient from doctor profile 
        /// </summary>
        /// <param name="id"> Patient Id</param>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        public class DoctorPatientParam
        {
            public string PatientId { get; set; }
            //public string patientFirstNmae { get; set; }
            //public string patientLastName { get; set; }
            //public string Role { get; set; }
            public bool status { get; set; }
        }
    }
}

