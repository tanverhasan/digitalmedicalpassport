﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApplicationCore.Interface;
using Microsoft.Extensions.Logging;
using DigitalMedicalPassport.API.Helper;
using DigitalMedicalPassport.API.DTO;
using AutoMapper;
using ApplicationCore.Entities;
using DigitalMedicalPassport.API.Models;
using Newtonsoft.Json;
using Microsoft.Azure.ServiceBus;
using System.Text;
using DigitalMedicalPassport.API.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Distributed;
using DigitalMedicalPassport.API.Policy;

namespace DigitalMedicalPassport.API.Controllers
{
    [Produces("application/json")]
    [Route("api/PatientPrescriptions")]
   // [Authorize(AppPolicy.RequireDoctorRole)]
    [Authorize]
    public class PatientPrescriptionsController : Controller
    {
        private readonly IPatientRepository _repo;
        private readonly ILogger<PatientPrescriptionsController> logger;
        private readonly IUrlHelper urlHlper;
        private readonly IServiceBusSend serviceBus;
        private readonly IDistributedCache cache;
        public PatientPrescriptionsController(IPatientRepository repo, ILogger<PatientPrescriptionsController> _logger, IUrlHelper _urlHelper, IServiceBusSend serviceBus, IDistributedCache distributedCache)
        {
            this._repo = repo ?? throw new ArgumentNullException(nameof(repo));
            this.logger = _logger ?? throw new ArgumentNullException(nameof(_logger));
            urlHlper = _urlHelper ?? throw new ArgumentNullException(nameof(_urlHelper));
            this.serviceBus = serviceBus ?? throw new ArgumentException(nameof(serviceBus));
            this.cache = distributedCache ?? throw new ArgumentException(nameof(distributedCache));
        }
        //// GET: api/PatientPrescriptions
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/PatientPrescriptions/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}
        /// <summary>
        /// This API Endpoint is responsible for creation of patient prescription
        /// </summary>
        /// <param name="value"> Prescription creation model</param>
        /// <returns></returns>
        // POST: api/PatientPrescriptions
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PrescriptionCreationDto value)
        {
            if (value == null)
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }
            try
            {
                var model = Mapper.Map<Prescription>(value);
                var result = await _repo.InsertPrescription(model, value.UserId);
                if (result)
                {
                    var key = $"dmp:{value.UserId}:patientProfile";
                    await this.cache.RemoveAsync(key);

                    var user = this._repo.GetById(value.UserId).Result;
                    var mail = new MailBindingModel
                    {
                        from = "t.h.aia@outlook.com",
                        to = user.Email,
                        subject = "New Prescription",
                        content = "A prescription is added to your profile."
                    };
                    var message = new Message(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(mail)));
                    await this.serviceBus.sendMessage(message);

                }
                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();

            }
        }
        /// <summary>
        /// This API endpoint is responsible to update patient prescription
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        // PUT: api/PatientPrescriptions/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        /// <summary>
        /// This API Endpoint is responsible to delete patient prescription
        /// </summary>
        /// <param name="id"></param>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
