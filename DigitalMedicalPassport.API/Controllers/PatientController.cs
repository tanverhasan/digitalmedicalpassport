﻿using ApplicationCore.Entities;
using ApplicationCore.Helper;
using ApplicationCore.Interface;
using AutoMapper;
using DigitalMedicalPassport.API.DTO;
using DigitalMedicalPassport.API.Helper;
using DigitalMedicalPassport.API.Policy;
using DigitalMedicalPassport.API.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Patient")]
   // [Authorize]
    [Authorize(AppPolicy.RequireUserRole)]
    public class PatientController : Controller
    {
        private readonly IPatientRepository _repo;
        private readonly ILogger<PatientController> logger;
        private readonly IUrlHelper urlHlper;
        private readonly IServiceBusSend serviceBusSend;
        private readonly IDistributedCache cache;

        public PatientController(IPatientRepository repo, ILogger<PatientController> _logger, IUrlHelper _urlHelper, IServiceBusSend serviceBusSend,IDistributedCache cache)
        {
            this._repo = repo ?? throw new ArgumentNullException(nameof(repo));
            this.logger = _logger ?? throw new ArgumentNullException(nameof(_logger));
            this.urlHlper = _urlHelper ?? throw new ArgumentNullException(nameof(_urlHelper));
            this.serviceBusSend = serviceBusSend ?? throw new ArgumentNullException(nameof(serviceBusSend));
            this.cache = cache ?? throw new ArgumentNullException(nameof(cache));

        }

        /// <summary>
        /// This API Endpoint is responsible for getting all patient from system. It can ne used to search. The search query searches in all common fileds in patient profile.
        /// It also take page size and page number for pagination.
        /// </summary>
        /// <param name="resourceParameter"> Patient Resource parameter model</param>
        /// <returns></returns>
        // GET: api/Patient
       // [Authorize(AppPolicy.RequireAdminRole)]
        [HttpGet(Name = "GetPatients")]

        public async Task<IActionResult> Get(PatientResourceParameter resourceParameter)
        {

            try
            {
                var result = await _repo.GetAll(resourceParameter);

                var previousPageLink = result.HasPrevious ? CreateResourceUri(resourceParameter, ResourceUriType.PreviousPage) : null;

                var nextPageLink = result.HasNext ? CreateResourceUri(resourceParameter, ResourceUriType.NextPage) : null;

                var paginationMetaData = new
                {
                    totalCount = result.TotalCount,
                    pageSize = result.PageSize,
                    currentPage = result.CurrentPage,
                    totalPages = result.TotalPage,
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink
                };

                Response.Headers.Add("X-Pagination", Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetaData));
                var patient = Mapper.Map<IEnumerable<PatientMinInfoDto>>(result);
                return Ok(patient);
            }catch(Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }


        }

        private string CreateResourceUri(PatientResourceParameter resourceParameter, ResourceUriType resourceUriType)
        {
            switch (resourceUriType)
            {
                case ResourceUriType.PreviousPage:
                    return urlHlper.Link("GetPatients", new
                    {
                        orderBy = resourceParameter.OrderByFirstName,
                        searchQuery = resourceParameter.SearchQuery,
                        firstName = resourceParameter.FirstName,
                        pageNumber = resourceParameter.PageNumber - 1,
                        pageSize = resourceParameter.PageSize
                    });
                case ResourceUriType.NextPage:
                    return urlHlper.Link("GetPatients", new
                    {
                        orderBy = resourceParameter.OrderByFirstName,
                        searchQuery = resourceParameter.SearchQuery,
                        firstName = resourceParameter.FirstName,
                        pageNumber = resourceParameter.PageNumber + 1,
                        pageSize = resourceParameter.PageSize
                    });
                default:
                    return urlHlper.Link("GetPatients", new
                    {
                        orderBy = resourceParameter.OrderByFirstName,
                        searchQuery = resourceParameter.SearchQuery,
                        firstName = resourceParameter.FirstName,
                        pageNumber = resourceParameter.PageNumber,
                        pageSize = resourceParameter.PageSize
                    });
            }
        }

        /// <summary>
        /// This API Endpoint is responsible to find one patient form database.
        /// </summary>
        /// <param name="id">Patient ID</param>
        /// <returns></returns>
        // GET: api/Test/5
        [ResponseCache(VaryByHeader = "User-Agent", Duration = 30)]
        [HttpGet("{id}", Name = "GetPatientId")]
        public async Task<IActionResult> Get(string id)
        {
            var user = HttpContext.User;
            
            try
            {
                var key = $"dmp:{id}:patientProfile";
                var value = this.cache.Get(key);
                if (value != null)
                {
                    var result = JsonConvert.DeserializeObject<PatientDto>(Encoding.UTF8.GetString(value));
                    return Ok(result);
                }
                if (this._repo.IsExist(id).Result)
                {
                    var query = await _repo.GetById(id);

                    if (query == null)
                    {
                        return NotFound();
                    }

                    var patient = Mapper.Map<PatientDto>(query);
                    await this.cache.SetAsync(key, Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(patient)));
                    return Ok(patient);
                }

                return NotFound();

            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }


        }

      

            /// <summary>
            /// This API Endpoint is responsible to create patient profile.
            /// </summary>
            /// <param name="patient">Patient creation model </param>
            /// <returns></returns>
        // POST: api/Patient
        [HttpPost]
        public IActionResult Post([FromBody]PatientCreationDto patient)
        {
            try
            {
                if (patient == null)
                {
                    return BadRequest();
                }

                
                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }
                if (!_repo.IsExist(patient.UserId).Result)
                {
                    var patientEntity = Mapper.Map<Patient>(patient);

                    _repo.Insert(patientEntity);

                    return Ok();
                }
                else
                {
                    return NoContent();
                }
                
                
            }catch( Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }

        }
        /// <summary>
        /// This API Endpoint is responsible to update patient profile
        /// </summary>
        /// <param name="id">Patient Id</param>
        /// <param name="model">Patient Creation Model </param>
        /// <returns></returns>
        // PUT: api/Patient/5
        [HttpPut("{id}", Name = "UpdatePatient")]
        [ActionName("UpdatePatient")]
        public async Task< IActionResult> UpdatePatient(string id, [FromBody]PatientCreationDto model)
        {
            try
            {
                if (model == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }
                if (this._repo.IsExist(model.UserId).Result)
                {
                    var patient = Mapper.Map<Patient>(model);
                    var insert = await _repo.Update(id, patient);
                    if (insert)
                    {
                        var key = $"dmp:{id}:patientProfile";
                        var value = this.cache.RemoveAsync(key);

                        return Ok();
                    }

                }


                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }

        }
        [HttpPut("image/{id}",Name = "UpdateImage")]
        [ActionName("UpdateImage")]
        public async Task<IActionResult> UpdateImage(string id ,[FromBody]ProfileImage model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            if (this._repo.IsExist(id).Result)
            {
                try
                {
                   var result= await this._repo.UpdateImageUri(id, model.ImageUri);
                    if (result)
                    {
                        var key = $"dmp:{id}:patientProfile";
                        var value = this.cache.RemoveAsync(key);
                        return Ok();
                    }
                    
                }
                catch (Exception ex)
                {
                    logger.LogError(ex.Message);
                    return BadRequest();
                }
            }
            return NotFound();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}", Name = "DeletePatient")]
        public IActionResult Delete(string id)
        {
            try
            {
                var exist = _repo.GetById(id);
                if (exist == null)
                {
                    return NotFound();
                }
                _repo.Delete(id);
                var key = $"dmp:{id}:patientProfile";
                var value = this.cache.RemoveAsync(key);
                logger.LogInformation(100, $" Patient record is deleted for id {id}");
                return NoContent();
            }catch(Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }
        }

        private PatientDto CreateLinksForPatient(PatientDto patient)
        {
            
            patient.Links.Add(new LinkDto(urlHlper.Link("GetById", new { id = patient.Id }), "self", "Get"));
            patient.Links.Add(new LinkDto(urlHlper.Link("DeletePatient", new { Id = patient.Id }), "delete_patient", "Delete"));
            patient.Links.Add(new LinkDto(urlHlper.Link("UpdatePatient", new { Id = patient.Id }), "update_patient", "Update"));
            return patient;
        }

        private LinkedCollectionResourceWrapperDto<PatientDto> CreateLinksForBooks(
           LinkedCollectionResourceWrapperDto<PatientDto> booksWrapper)
        {
            // link to self
            booksWrapper.Links.Add(
                new LinkDto(urlHlper.Link("GetPatient", new { }),
                "self",
                "GET"));

            return booksWrapper;
        }

        public class ProfileImage
        {
            public string ImageUri { get; set; }
        }
    }
}