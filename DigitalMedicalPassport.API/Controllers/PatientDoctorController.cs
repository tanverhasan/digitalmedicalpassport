﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApplicationCore.Interface;
using Microsoft.Extensions.Logging;
using DigitalMedicalPassport.API.DTO;
using AutoMapper;
using DigitalMedicalPassport.API.Helper;
using ApplicationCore.Entities;
using DigitalMedicalPassport.API.Service;
using DigitalMedicalPassport.API.Models;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Azure.ServiceBus;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Distributed;
using DigitalMedicalPassport.API.Policy;

namespace DigitalMedicalPassport.API.Controllers
{
    [Produces("application/json")]
    [Route("api/PatientDoctor")]
    //[Authorize(AppPolicy.RequireUserRole)]
    [Authorize]
    public class PatientDoctorController : Controller
    {
        private readonly IPatientRepository _repo;
        private readonly ILogger<PatientController> logger;
        private readonly IUrlHelper urlHlper;
        private readonly IServiceBusSend serviceBusSend;
        private readonly IDistributedCache cache;
        public PatientDoctorController(IDistributedCache cache, IPatientRepository repo, IServiceBusSend serviceBusSend, ILogger<PatientController> _logger, IUrlHelper _urlHelper)
        {
            this._repo = repo ?? throw new ArgumentNullException(nameof(repo));
            this.logger = _logger ?? throw new ArgumentNullException(nameof(_logger));
            urlHlper = _urlHelper ?? throw new ArgumentNullException(nameof(_urlHelper));
            this.serviceBusSend = serviceBusSend ?? throw new ArgumentNullException(nameof(serviceBusSend));
            this.cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }
        // GET: api/PatientDoctor
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        /// <summary>
        /// This API Endpoint is responsible to find all doctors in patient profile
        /// </summary>
        /// <param name="id">Doctor Id</param>
        /// <returns></returns>
        // GET: api/PatientDoctor/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            try
            {
                var query = await _repo.getDoctors(id);

                if (query == null)
                {
                    return NotFound();
                }

                var patient = Mapper.Map<IEnumerable<DoctorRefDto>>(query);
                return Ok(patient);


            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// This API Endpoint is responsible to create doctor in patient profile
        /// </summary>
        /// <param name="model">Doctor Reference Creation Model</param>
        /// <returns></returns>

        // POST: api/PatientDoctor
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]DoctorRefCreationDto model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                if (!this._repo.IsExist(model.UserId).Result)
                {
                    return NotFound();
                }
                var map = Mapper.Map<DoctorRef>(model);
                await _repo.InsertDoctorToPatientProfile(map, model.UserId);

                var key = $"dmp:{model.UserId}:patientProfile";
                await this.cache.RemoveAsync(key);

                var result = await _repo.GetById(model.UserId);
                var mail = new MailBindingModel
                {
                    from = "t.h.aia@outlook.com",
                    to = result.Email,
                    subject = "Doctor authorisation",
                    content = $" A doctor has been added to your profile with name {model.FirstName} {model.LastName} ",
                };
                var message = new Message(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(mail)));
                await serviceBusSend.sendMessage(message);
                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return BadRequest();
            }

        }
        /// <summary>
        /// This API Endpoint is responsible to update doctor reference in Pateint  profile
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        // PUT: api/PatientDoctor/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]DoctorRefCreationDto value)
        {
        }
        /// <summary>
        /// This API Endpoint is responsible for deleting doctor reference form patient profile
        /// </summary>
        /// <param name="id"></param>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
