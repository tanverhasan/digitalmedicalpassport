﻿namespace DigitalMedicalPassport.API.Helper
{
    public enum ResourceUriType
    {
        PreviousPage,
        NextPage
    }
}