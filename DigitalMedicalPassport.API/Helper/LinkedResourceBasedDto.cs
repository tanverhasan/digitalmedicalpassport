﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.Helper
{
    public abstract class LinkedResourceBasedDto
    {
        public List<LinkDto> Links { get; set; }
        = new List<LinkDto>();
    }
}
