﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class DoctorDto
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime Dob { get; set; }
        public string ImageUri { get; set; }
       
        public string AreaOfWork { get; set; }
        
        public string Summary { get; set; }
        public string Email { get; set; }
        public string SexualOrientation { get; set; }
        public string LivingStatus { get; set; }
        public string Alcohol { get; set; }
        public string SocialHistory { get; set; }

        public IList<AddressDto> Addresses { get; set; }

        public IList<PhoneNumberDto> PhoneNumbers { get; set; }

        public IList<QualificationDto> Qualifications { get; set; }
        public IList<AlergyDto> Alergys { get; set; }
        public IList<PrescriptionDto> Prescriptions { get; set; }
        public IList<PatientRefDto> Patients { get; set; }

        public IList<DoctorDoctorRefDto> Doctors { get; set; }
    }

    public  class DoctorMinInfoDto
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime Dob { get; set; }
        public string Email { get; set; }
        public string ImageUri { get; set; }
        
        public string AreaOfWork { get; set; }
        
        public string Summary { get; set; }
        public IList<AddressDto> Addresses { get; set; }
    }
    public class DoctorDoctorRefDto
    {
        public string DoctorId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Status { get; set; }
    }
}
