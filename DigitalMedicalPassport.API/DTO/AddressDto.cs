﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class AddressDto
    {
        public string AddressType { get; set; }
        public string Street1 { get; set; }
        
        public string Street2 { get; set; }
        
        
        public string City { get; set; }
        
        public string District { get; set; }
       
        public string Postal { get; set; }
        public string Country { get; set; }
    }
    public class AddressCreationDto
    {
        public string AddressType { get; set; }
        public string Street1 { get; set; }

        public string Street2 { get; set; }

        public string City { get; set; }

        public string District { get; set; }

        public string Postal { get; set; }

        public string Contry { get; set; }
    }

}
