﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class QualificationDto
    {

         
            public string University { get; set; }
          
            public string Subject { get; set; }
           
            public string Degree { get; set; }
        
    }
}
