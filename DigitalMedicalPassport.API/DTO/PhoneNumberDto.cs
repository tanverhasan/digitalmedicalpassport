﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class PhoneNumberDto
    {
        public string Type { get; set; }

        public string Number { get; set; }
    }
    public class PhoneNumberCreationDto
    {
        public string Type { get; set; }

        public string Number { get; set; }
    }

}
