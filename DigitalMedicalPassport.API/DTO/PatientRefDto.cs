﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class PatientRefDto
    {

        public string PatientId { get; set; }
        public string PatientFirstNmae { get; set; }
        public string PatientLastName { get; set; }
        public string Role { get; set; }
        public bool status { get; set; }
    }
}
