﻿using DigitalMedicalPassport.API.Helper;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class PatientDto:LinkedResourceBasedDto
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string UserId { get; set; }
        // public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Dob { get; set; }
        public string ImageUri { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public IList<PhoneNumberDto> PhoneNumbers { get; set; }
        public Byte[] TimeStamp { get; set; }
        public IList<AddressDto> Addresses { get; set; }
        public string Occupation { get; set; }
        public string SexualOrientation { get; set; }
        public string LivingStatus { get; set; }
        public string Alcohol { get; set; }
        public IList<AlergyDto> Alergys { get; set; }
        public IList<PrescriptionDto> Prescriptions { get; set; }
        public  IList<DoctorRefDto> Doctors { get; set; }

    }

    public class PatientMinInfoDto : LinkedResourceBasedDto
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string UserId { get; set; }
        // public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Dob { get; set; }
        public IList<PhoneNumberDto> PhoneNumber { get; set; }
       
    }

}
