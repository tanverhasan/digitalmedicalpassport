﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class DoctorCreationDto
    {
        public string Title { get; set; }
        [Required(ErrorMessage ="UserId fild is required")]
        public string UserId { get; set; }

        [Required(ErrorMessage = "FirstName is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "LastName is required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "DateOfBirth is required")]
        public string Gender { get; set; }
        public DateTime Dob { get; set; }
        public string ImageUri { get; set; }
        public string AreaOfWork { get; set; }
        public string Summary { get; set; }
        [Required(ErrorMessage ="Address is required")]
        public string Email { get; set; }
        public string  SexualOrientation { get; set; }
        public string LivingStatus { get; set; }
        public string Alcohol { get; set; }
        public string SocialHistory { get; set; }
        public IList<AddressCreationDto> Addresses { get; set; }
        public IList<PhoneNumberCreationDto> PhoneNumbers { get; set; }
        public IList<QualificationCreationDto> Qualifications { get; set; }
        public IList<AlergyCreationDto> Alergys { get; set; }

        public IList<PatientRefCreationDto> Patients { get; set; }

        public IList<DoctorDoctorCreationDto> Doctors { get; set; }
    } 
    public class DoctorDoctorCreationDto
    {
        public string UserId { get; set; }
        public string DoctorId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Boolean Status { get; set; }
    }
    public class PatientRefCreationDto
    {
        public string PatientId { get; set; }
        public string PatientFirstNmae { get; set; }
        public string PatientLastName { get; set; }
        public string Role { get; set; }
        public bool status { get; set; }
    }
}
