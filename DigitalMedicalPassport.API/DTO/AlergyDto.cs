﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class AlergyDto
    {

        public string Type { get; set; }

        public string Description { get; set; }
    }
    public class AlergyCreationDto
    {

        public string Type { get; set; }

        public string Description { get; set; }
    }

}
