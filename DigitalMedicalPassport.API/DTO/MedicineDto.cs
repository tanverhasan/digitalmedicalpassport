﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class MedicineDto
    {
        public string Name { get; set; }
        
        public string Description { get; set; }
    }
    public class MedicineCreationDto
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }


}
