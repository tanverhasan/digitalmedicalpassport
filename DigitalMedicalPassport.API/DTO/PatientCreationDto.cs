﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class PatientCreationDto
    {
        public string Title { get; set; }
        [Required(ErrorMessage ="User Id field is required")]
        public string UserId { get; set; }
        
        [Required(ErrorMessage = "First Name field is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last  Name field is required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Date of birth field is required")]
        public DateTime Dob { get; set; }
        public string Gender { get; set; }
        [Required(ErrorMessage ="Email field is required")]
        public string Email { get; set; }
        public IList<PhoneNumberCreationDto> PhoneNumbers { get; set; }
        public IList<AddressCreationDto> Addresses { get; set; }
        public string Occupation { get; set; }
        public string SexualOrientation { get; set; }
        public string LivingStatus { get; set; }
        public string Alcohol { get; set; }
        public string SocialHistory { get; set; }
        public IList<AlergyCreationDto> Alergys { get; set; }
        public IList<PrescriptionCreationDto> Prescriptions { get; set; }

        public IList<DoctorRefCreationDto> Doctors { get; set; }
    }

    public class DoctorRefCreationDto
    {
        public string UserId { get; set; }
        public string DoctorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Status { get; set; }
    }
}
