﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.DTO
{
    public class PrescriptionDto
    {

        public DateTime CurrentDate { get; set; }

        public IList<MedicineDto> Medicines { get; set; }

      
        public string Comments { get; set; }
    }
    public class PrescriptionCreationDto
    {

        public string UserId { get; set; }
       // public string DoctorRef { get; set; }
        public DateTime CurrentDate { get; set; }
        public IList<MedicineCreationDto> Medicines { get; set; }
        public string DoctorRef { get; set; }

        public string Comments { get; set; }
    }

}
