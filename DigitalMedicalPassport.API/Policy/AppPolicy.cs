﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.Policy
{
    public static class AppPolicy
    {
        public const string RequireAdminRole = "RequireAdminRole";
        public const string RequireDoctorRole = "RequireDoctorRole";
        public const string RequireUserRole = "RequireUserRole";
    }
}
