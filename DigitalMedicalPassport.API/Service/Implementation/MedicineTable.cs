﻿
//using DigitalMedicalPassport.API.Models;
//using Microsoft.Azure.CosmosDB.Table;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;
//using Microsoft.WindowsAzure.Storage;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace DigitalMedicalPassport.API.Service
//{
//    public class MedicineTable : IMedicineTable
//    {
//        private readonly  ILogger<MedicineTable> logger;
//        private  string connectionstring=null;
//        IConfiguration configuration;
//        CloudStorageAccount storageAccount = null;
//        public MedicineTable(ILogger<MedicineTable> _logger, IConfiguration configuration)
//        {
//            this.logger = _logger ?? throw new ArgumentException(nameof(_logger));
//            this.configuration = configuration ?? throw new ArgumentException(nameof(configuration));
//            Connect();
//        }
//        public  CloudStorageAccount Connect()
//        {

//            connectionstring = configuration["Cosmos:connectionString"];
//            try
//            {
//                storageAccount = CloudStorageAccount.Parse(connectionstring);
//            }
//            catch (FormatException ex)
//            {

//                logger.LogError(ex.Message);
//                throw;
//            }
//            catch (ArgumentException ex)
//            {
//                logger.LogError(ex.Message);
//                throw;
//            }

//            return storageAccount;
//        }

//        public  async Task<Medicine> RetrieveEntityUsingPointQueryAsync(CloudTable table, string partitionKey, string rowKey)
//        {
//            try
//            {
                
//                TableOperation retrieveOperation = TableOperation.Retrieve<Medicine>(partitionKey, rowKey);
//                TableResult result = await table.ExecuteAsync(retrieveOperation);
//                Medicine medicine = result.Result as Medicine;
//                return medicine;
//            }
//            catch (StorageException ex)
//            {
//                logger.LogError(ex.Message);
//                throw;
//            }
//        }
//    }
//}
