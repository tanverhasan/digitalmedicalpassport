﻿using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DigitalMedicalPassport.API.Service
{
    public class ServiceBusSend : IServiceBusSend
    {
        private readonly IConfiguration _configuration;
        private readonly IQueueClient _queueClient;
        private readonly ILogger<ServiceBusSend> logger;

        public ServiceBusSend(IConfiguration configuration ,  ILogger<ServiceBusSend> _logger )
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _queueClient = new QueueClient(_configuration["AzureServiceBus:connectionString"],_configuration["AzureServiceBus:queueName"]);
            logger= _logger ?? throw new ArgumentNullException(nameof(_logger));
        }

        public async Task sendMessage(Message data)
        {
            try
            {
               
               await _queueClient.SendAsync(data);
               await _queueClient.CloseAsync();
            }
            catch (Exception ex)
            {

                logger.LogError(ex.Message);
            }
            
        }
    }
}
