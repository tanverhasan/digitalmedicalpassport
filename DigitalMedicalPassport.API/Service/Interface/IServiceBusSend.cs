﻿using Microsoft.Azure.ServiceBus;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.Service
{
    public interface IServiceBusSend
    {
        Task sendMessage(Message data);
    }
}