﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DigitalMedicalPassport.API.Middleware
{
    public class NotImplementedExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<NotImplementedExceptionMiddleware> _logger;

        public NotImplementedExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = loggerFactory?.CreateLogger<NotImplementedExceptionMiddleware>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (NotImplementedException ex)
            {
                if (!context.Response.HasStarted)
                {
                    int errorCode = (int)HttpStatusCode.NotImplemented;

                    context.Response.Clear();
                    context.Response.StatusCode = errorCode;
                    context.Response.ContentType = "application/json";

                    // this is a sample error result object
                    ErrorResult error = new ErrorResult("Not implemented", ex.Message);
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(error));
                }
            }
        }

        private class ErrorResult
        {
            private string subject;
            private string message;

            public ErrorResult(string subject, string message)
            {
                this.subject = subject;
                this.message = message;
            }
        }
    }
}
