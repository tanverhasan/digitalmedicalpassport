﻿//using Microsoft.AspNetCore.Mvc.Filters;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace DigitalMedicalPassport.API.Filter
//{
//    public class WebApiValidateAntiForgeryTokenAttribute : ActionFilterAttribute
//    {
//        public override void OnActionExecuting(
//        System.Web.Http.Controllers.HttpActionContext actionContext)
//    {
//        if (actionContext == null)
//        {
//            throw new ArgumentNullException("actionContext");
//        }

//        if (actionContext.Request.Method.Method != "GET")
//        {
//            var headers = actionContext.Request.Headers;
//            var tokenCookie = headers
//                .GetCookies()
//                .Select(c => c[AntiForgeryConfig.CookieName])
//                .FirstOrDefault();

//            var tokenHeader = string.Empty;
//            if (headers.Contains("X-XSRF-Token"))
//            {
//                tokenHeader = headers.GetValues("X-XSRF-Token").FirstOrDefault();
//            }

//            AntiForgery.Validate(
//                tokenCookie != null ? tokenCookie.Value : null, tokenHeader);
//        }

//        base.OnActionExecuting(actionContext);
//    }
//    }
//}
